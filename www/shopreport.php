<?php

//
// ShopADV
// API module (for widget and external data getting)
// Copyright (c) 2014 Alyoshin Vitaliy <master@ap-soft.com>
//
//
// // Req. INPUT (other input depends on "action" param):
// xapi - идентификатор клиента (*)
// action - идентификатор действия (*)
// OUTPUT on SUCCESS: depends on "action" param

define('IN_SITE', 'ok');

set_time_limit(600); // 10 min

require_once('../api/config.php');
require_once('../api/error_codes.php');
require_once('../api/enum.php');
require_once('../api/classes/json.php');
require_once('../api/classes/dbshopadv.php');
require_once('../api/classes/utils.php');
require_once('../api/classes/loger.php');
require_once('../api/classes/input.php');
require_once('../api/classes/mail.php');

$res = array();

$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';

// start input data
$in = $_GET;

$in['login'] = 'SHOP';
$in['password'] = 'shop';

if (!isset($in['xid'])) {
    $in['xid'] = 1; // default
}

if (!isset($in['key'])) {
    $in['key'] = '1af322b2b69ec230ff0a323d8c9e6cb5';
}

$in['action'] = '_shopreport';
if (isset($_SESSION['sid:' . $ip])) {
    $in['sid'] = $_SESSION['sid:' . $ip];
}

$Mail = new SMTPMailer();
$Mail->Host = SMTP_HOST;
$Mail->Port = SMTP_PORT;
$Mail->User = SMTP_USER;
$Mail->Password = SMTP_PASS;
$Mail->SMTPType = SMTP_TYPE;
$Mail->SMTPFrom = SMTP_FROM;
$Mail->From = SYSTEM_EMAIL_FROM;
$Mail->FromName = SYSTEM_EMAIL;
$Mail->Mailer = SMTP_MAILER;

$Loger = new Loger(LOG_FILE, 'api');
$Loger->Email = SYSTEM_EMAIL;
$Loger->Mailer = $Mail;

$Input = new Input($in);

$Loger->Session = $Input->get();

try {

    $action = $Input->getParam('action', true);
    if (!$action) {
        throw new Exception('<action> is required', ERR_PARAM_MISSING);
    }

    $DB = new DatabaseShopADV(DB_NAME, DB_HOST, DB_USER, DB_PASSWORD, DB_PORT);
    $DB->Loger = $Loger;

    $actionFileName = '../api/actions/' . $action . '.php';

    if (file_exists($actionFileName)) {

        include('../api/actions/connect.php');
        $_SESSION['sid:' . $ip] = $res['sid'];
        $sid = $res['sid'];

        if (!isset($_SESSION[$sid][$ip])) {

            //$Loger->log(Loger::WARN, "Session is not set: $sid $ip");

            $sessionInfo = $DB->sessionTest($sid, $ip);

            if (!$sessionInfo) {
                throw new Exception('Not Auth.', ERR_AUTH_REQUIRED);
            }

            $xid = $sessionInfo['xid'];
            if (!$xid) {
                throw new Exception('Not Auth.', ERR_AUTH_REQUIRED);
            }

            $country = $sessionInfo['country'];
            if (!$country) {
                throw new Exception('Not Auth.', ERR_AUTH_REQUIRED);
            }

            $config = array(
                'title' => $sessionInfo['title'],
                'country' => $country,
                'period_start' => date('Y-m')
            );

            $DB->XID = $xid;
            $DB->Country = $country;

            $user = $DB->getUser($sessionInfo['login']);
            if (!$user) {
                throw new Exception('Not Auth.', ERR_AUTH_REQUIRED);
            }

            $_SESSION[$sid][$ip] = array(
                'session' => $sessionInfo['session'],
                'config' => $config,
                'xid' => $xid,
                'user' => $user
            );
        } else {
            $DB->XID = $_SESSION[$sid][$ip]['xid'];
            $DB->Country = $_SESSION[$sid][$ip]['config']['country'];
        }

        $DB->sessionDo($_SESSION[$sid][$ip]['session']);

        function testRole($allowedRoles) {
            global $sid;
            global $ip;
            return in_array($_SESSION[$sid][$ip]['user']['role'], $allowedRoles);
        }

        include($actionFileName);
    } else {
        throw new Exception('wrong action', ERR_ACTION_WRONG);
    }
} catch (Exception $e) {
    $Loger->log(Loger::WARN, 'Error: ' . $e->getCode() . ': ' . $e->getMessage() . "\n\n" . print_r($Input->get(), true) . print_r($_REQUEST, true));
    header("HTTP/1.0 404 Not Found");
}

exit();
