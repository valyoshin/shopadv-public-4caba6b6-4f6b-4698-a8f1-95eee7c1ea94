<?php

//
// ShopADV
// Document module (for widget and external data getting)
// Copyright (c) 2014 Alyoshin Vitaliy <master@ap-soft.com>
//

define('IN_SITE', 'ok');

set_time_limit(600); // 10 min

require_once('../api/config.php');
require_once('../api/error_codes.php');
require_once('../api/enum.php');
require_once('../api/classes/json.php');
require_once('../api/classes/dbshopadv.php');
require_once('../api/classes/utils.php');
require_once('../api/classes/loger.php');
require_once('../api/classes/input.php');
require_once('../api/classes/mail.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");

function getContentType($ext) {
    $ext = strtolower($ext);
    switch ($ext) {
        case 'xls':
            return 'application/vnd.ms-excel';
        case 'doc':
            return 'application/msword';
        case 'xlsx':
            return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        case 'docx':
            return 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        case 'jpg':
        case 'jpeg':
            return 'image/jpeg';
        case 'png':
            return 'image/png';
        case 'gif':
            return 'image/gif';
    }

    return 'octet-stream';
}

$Mail = new SMTPMailer();
$Mail->Host = SMTP_HOST;
$Mail->Port = SMTP_PORT;
$Mail->User = SMTP_USER;
$Mail->Password = SMTP_PASS;
$Mail->SMTPType = SMTP_TYPE;
$Mail->SMTPFrom = SMTP_FROM;
$Mail->From = SYSTEM_EMAIL_FROM;
$Mail->FromName = SYSTEM_EMAIL;
$Mail->Mailer = SMTP_MAILER;

$Loger = new Loger(LOG_FILE, 'doc');
$Loger->Email = SYSTEM_EMAIL;
$Loger->Mailer = $Mail;

$Input = new Input($_GET);

$Loger->Session = $Input->get();

try {

    $doc = $Input->getParam('doc', true);
    if (!$doc) {
        throw new Exception('<doc> is required', ERR_PARAM_MISSING);
    }

    $DB = new DatabaseShopADV(DB_NAME, DB_HOST, DB_USER, DB_PASSWORD, DB_PORT);
    $DB->Loger = $Loger;

    $info = $DB->getDocInfo($doc);
    if (!$info) {
        throw new Exception('Document "'.$doc.'" is missing', ERR_DOC_MISSING);
    }

    $title = $info['title'];
    $ext = $info['ext'];

    $filename = DOC_DIR . $doc;
    if ($ext) {
        $filename .= '.' . $ext;
    }

    if (!file_exists( $filename )) {
        throw new Exception('Document "'.$doc.'" is missing', ERR_DOC_MISSING);
    }

    // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
    // если этого не сделать файл будет читаться в память полностью!
    if (ob_get_level()) {
      ob_end_clean();
    }
    // заставляем браузер показать окно сохранения файла
    header('Content-Description: File Transfer');
    // XLS files

    header('Content-Type: ' . getContentType($ext));
    header('Content-Disposition: attachment; filename=' . Utils::asFilename( $title ));
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: ' . filesize($filename));
    // читаем файл и отправляем его пользователю
    readfile($filename);

} catch (Exception $e) {
    $Loger->log(Loger::WARN, 'Error: ' . $e->getCode() . ': ' . $e->getMessage() . "\n\n" . print_r($Input->get(), true) . print_r($_REQUEST, true));
    header("HTTP/1.0 404 Not Found");
}

exit();
