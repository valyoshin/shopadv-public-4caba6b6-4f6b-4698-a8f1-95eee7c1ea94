;
(function ($) {
    /**
     * ShopADV
     * WWW :: shopadv-base.js
     * $.sa - AhopADV engine object
     * (c) 2014-2017 Alyoshin V.A. master@ap-soft.com
     * http://shopadv.biz
     *
     **/

    'use strict';

    var _debugPassword = null;
    var _period_count = 12;
    var _report_period_count = 48;
    var _tableMaxCount = 200;
    var _maket_w_max = 640;
    var _country = '';
    var _redirect = document.location.protocol + '//' + document.location.hostname + '/';
    _redirect += 'result.html?%s';

    $.sa = $.sa || {};
    $.extend($.sa, {
        init: function () {
            initStart();
            if (_debugPassword) {
                $.saAPI.debug(true);
                $.saAPI.connect('ADMIN', _debugPassword, onInit, onFatalError);
            } else {
                $.saModalLogin.execute(onInit, onFatalError);
            }
        },
        destroy: function () {
            unbind();
        },
        _doTaskPlanRowUpdateAmount: function (planId) {
            doTaskPlanRowUpdateAmount(planId);
        }
    });

    function getRolesForSelect() {
        var roles = $.saAPI.roles();
        var res = new Array();
        for (var role in roles) {
            res.push({
                id: roles[role],
                title: $.lang.role[roles[role]]
            });
        }
        return res;
    }

    function getStatesForSelect() {
        var states = $.saAPI.states();
        var res = new Array();
        for (var s in states) {
            res.push({
                id: 's' + states[s],
                title: $.lang.state[states[s]]
            });
        }
        return res;
    }

    function getPeriodTypesForSelect() {
        var pt = $.saAPI.periodTypes();
        var res = new Array();
        for (var s in pt) {
            res.push({
                id: pt[s],
                title: $.lang.period_type[pt[s]]
            });
        }
        return res;
    }

    // Getters

    function getPeriodStart(value) {
        var r = new Date();
        var year = r.getFullYear();
        var month = r.getMonth() + 1;
        if (!value) {
            value = $.saAPI.config.period_start;
        }
        if (value) {
            var A = value.split('-');
            year = strToIntDefault(A[0], year);
            month = strToIntDefault(A[1], month);
        }
        return new Date(year, month - 1);
    }

    function getMonthName(p) {
        return $.lang.month[p.getMonth() + 1].name + ' ' + p.getFullYear() + $.lang.yearSuffix;
    }

    // GUI

    var _callback = null;

    function disable() {
        $('#busy').show();
    }

    function enable() {
        $('#busy').hide();
        if ($.isFunction(_callback)) {
            _callback();
            _callback = null;
        }
    }

    var _task = {};

    function taskAdd(task) {
        _task[task] = 1;
        disable();
    }

    function taskRemove(task) {
        if (_task[task]) {
            delete _task[task];
        }
        if (Object.keys(_task).length <= 0) {
            enable();
        }
    }

    // Events

    function onInit(data) {
        buildColModel();
        loadServicesCache();
        loadClientsCache();
        loadShopsCache();
        loadTownsCache();
        loadCategoriesCache();
        loadCategoryGroupsCache();
        setOrg($.saAPI.config().title + ' [v.' + $.saAPI.version() + ']');
        setUserName($.saAPI.user().name);
        setCountry($.saAPI.config().country);
        initEnd();
        bind();
        applyRole();
        if ($.saAPI.user().role === $.saAPI.roleShop()) {
            _callback = onReportShopTask;
        }
    }

    function onFatalError(error) {
        console.log(error);
        if (!processError(error)) {
            //document.location = 'error.php?error=init&msg=' + fixedEncodeURIComponent(error.code + ':' + error.msg);
        }
    }

    function onError(xhr, status, error) {
        if (!processError(error)) {
            alert('Error: ' + error.code + ':' + error.msg);
        }
    }

    function processError(error) {
        if (error.code) {
            switch (error.code) {
                case $.saErr.ERR_AUTH_REQUIRED:
                case $.saErr.ERR_USER_DENY:
                case $.saErr.ERR_OLD_VERSION:
                    document.location = 'index.php';
                    return true;
            }
        }

        return false;
    }

    function applyRole() {
        // only for ADMIN
        if ($.saAPI.isAdmin()) {
            $('#menu-users').show();
        } else {
            $('#menu-users').hide();
        }
        // only for ADMIN and MANAGERs
        if ($.saAPI.isAdmin() || $.saAPI.isManager()) {
            $('#menu-service-top').show();
            $('#menu-report-dislocation').show();
            $('#menu-report-charge').show();
            $('#menu-clients').show();
            $('#menu-categories').show();
            $('#menu-config-top').show();
        } else {
            $('#menu-service-top').hide();
            $('#menu-report-dislocation').hide();
            $('#menu-report-charge').hide();
            $('#menu-clients').hide();
            $('#menu-categories').hide();
            $('#menu-config-top').hide();
        }
        // for all users
        if ($.saAPI.isAdmin() || $.saAPI.isManager() || $.saAPI.isShop()) {
            $('#menu-report-shop-task').show();
        } else {
            $('#menu-report-shop-task').hide();
        }
    }

    // Services cache

    var _services = new apCache('service');

    function loadServicesCache() {
        taskAdd('loadServicesCache');
        $.saAPI.query('services_cache', {}, onLoadServicesCache, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadServicesCache');
        });
    }

    function onLoadServicesCache(data) {
        _services.setData(data.service_list);

        var serviceList = [];
        serviceList[0] = data.service_list.filter(function (service) {
            return service.state && service.service_type === 'M';
        });
        serviceList[1] = data.service_list.filter(function (service) {
            return service.state && service.service_type === 'A';
        });
        serviceList[2] = data.service_list.filter(function (service) {
            return service.state && service.service_type === 'C';
        });

        var menu = $('#menu-service').empty();
        var item = null;
        var itemIndex = null;
        var index = null;
        var exists = false;

        for (index in serviceList) {
            exists = false;
            for (itemIndex in serviceList[index]) {
                exists = true;
                item = serviceList[index][itemIndex];
                $('<li class="menu menu-service" id="menu-service-' + item.code + '" data-id="' + item.service + '"><a href="#service-' + item.code + '" data-id="' + item.service + '">' + item.title + '</a></li>').appendTo(menu);
            }
            if (exists) {
                $('<li><hr/></li>').appendTo(menu);
            }
        }
        taskRemove('loadServicesCache');
        rebind();
    }

    // Clients cache

    var _clients = new apCache('client');

    function loadClientsCache() {
        taskAdd('loadClientsCache');
        $.saAPI.query('clients_cache', {}, onLoadClientsCache, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadClientsCache');
        });
    }

    function onLoadClientsCache(data) {
        _clients.setData(data.client_list);
        taskRemove('loadClientsCache');
    }

    function reloadClientsServicePricesCache(serviceId) {
        taskAdd('loadClientServicePricesCache');
        $.saAPI.query('clients_service_prices_cache', {service: serviceId}, onLoadClientsServicePricesCache, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadClientServicePricesCache');
        });
    }

    function onLoadClientsServicePricesCache(data) {
        var serviceId;
        var clientId;
        var price;
        var clientData;

        for (var i in data.clients_service_prices_list) {
            serviceId = data.clients_service_prices_list[i]['service'];
            clientId = data.clients_service_prices_list[i]['client'];
            price = data.clients_service_prices_list[i]['price'];

            clientData = _clients.get(clientId);
            if (clientData) {
                clientData['service_prices'] = {};
                clientData['service_prices'][serviceId] = {
                    price: price
                };

                _clients.set(clientId, clientData);
            }
        }

        taskRemove('loadClientServicePricesCache');
    }

    // Shops cache

    var _shops = new apCache('shop');

    function loadShopsCache() {
        taskAdd('loadShopsCache');
        $.saAPI.query('shops_cache', {}, onLoadShopsCache, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadShopsCache');
        });
    }

    function onLoadShopsCache(data) {
        _shops.setData(data.shop_list);
        taskRemove('loadShopsCache');
    }

    // Town cache

    var _towns = new apCache('town');

    function loadTownsCache() {
        taskAdd('loadTownsCache');
        $.saAPI.query('towns_cache', {}, onLoadTownsCache, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadTownsCache');
        });
    }

    function onLoadTownsCache(data) {
        _towns.setData(data.town_list);
        taskRemove('loadTownsCache');
    }


    // Category cache

    var _categories = new apCache('category');

    function loadCategoriesCache() {
        taskAdd('loadCategoriesCache');
        $.saAPI.query('categories_cache', {}, onLoadCategoriesCache, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadCategoriesCache');
        });
    }

    function onLoadCategoriesCache(data) {
        var categoriesData = [];
        var item;
        for (var i in data.category_list) {
            item = data.category_list[i];
            item.style = getCategoryStyle(item);
            categoriesData.push(item);
        }

        _categories.setData(categoriesData);
        taskRemove('loadCategoriesCache');
    }

    function getCategoryStyle(categoryObject) {
        var categoryGroup = categoryObject.category_group;
        var styles = [
            'default',
            'warning',
            'danger',
            'primary',
            'success',
            'info'
        ];
        var n = styles.length;
        var index = categoryGroup % n;

        return styles[index];
    }

    var _categoryGroups = new apCache('category_group');

    function loadCategoryGroupsCache() {
        taskAdd('loadCategoryGroupsCache');
        $.saAPI.query('category_groups_cache', {}, onLoadCategoryGroupsCache, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadCategoryGroupsCache');
        });
    }

    function onLoadCategoryGroupsCache(data) {
        _categoryGroups.setData(data.category_group_list);
        taskRemove('loadCategoryGroupsCache');
    }

    // Common

    function setCaption(value) {
        $('#content-caption').text(value);
    }

    function setOrg(value) {
        $('#footer-org').text(value);
    }

    function setUserName(value) {
        $('#footer-user').text(value);
    }

    function setCountry(value) {
        _country = value;
    }

    // Tables

    var _table = null;
    var _colModel = {
        client_select: {
            name: 'client_select',
            label: $.lang.column.client, // can be function
            align: 'left', // left, right, center
            cssclass: 'table-cell-strong',
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'client_title', // index in data object
            search: {
                type: 'select',
                index: 'client',
                data: []
            },
            formatter: null,
            width: 220 // column width in pixels
        },
        client_title: {
            name: 'client_title',
            label: $.lang.column.client, // can be function
            align: 'left', // left, right, center
            cssclass: 'table-cell-strong',
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'client_title', // index in data object
            search: {
                type: 'text',
                index: 'client_title'
            },
            formatter: function (value, row) {
                return '<span class="client-title" data-id="' + row.client + '">' + value + '</span>';
            },
            width: 220 // column width in pixels
        },
        category_title: {
            name: 'category_title',
            label: $.lang.column.category, // can be function
            align: 'left', // left, right, center
            cssclass: 'table-cell-strong',
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'category_title', // index in data object
            search: {
                type: 'text',
                index: 'category_title'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 220 // column width in pixels
        },
        category_group_title: {
            name: 'category_group_title',
            label: $.lang.column.category_group, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'category_group_title', // index in data object
            search: {
                type: 'text',
                index: 'category_group_title'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 220 // column width in pixels
        },
        town_select: {
            name: 'town_select',
            label: $.lang.column.town, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'town_title', // index in data object
            search: {
                type: 'select',
                index: 'town',
                data: []
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 200 // column width in pixels
        },
        shop: {
            name: 'shop',
            label: $.lang.column.shop, // can be function
            align: 'center', // left, right, center
            cssclass: 'table-cell-strong',
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'shop', // index in data object
            search: {
                type: 'int',
                index: 'shop'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 100 // column width in pixels
        },
        service: {
            name: 'service',
            label: $.lang.column.serviceID, // can be function
            align: 'center', // left, right, center
            cssclass: 'table-cell-strong',
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'service', // index in data object
            search: {
                type: 'int',
                index: 'service'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 100 // column width in pixels
        },
        service_select: {
            name: 'service_select',
            label: $.lang.column.service, // can be function
            align: 'center', // left, right, center
            cssclass: 'table-cell-strong',
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'service_title', // index in data object
            search: {
                type: 'select',
                index: 'service',
                data: []
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 200 // column width in pixels
        },
        service_title: {
            name: 'service_title',
            label: $.lang.column.service, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'service_title', // index in data object
            search: {
                type: 'text',
                index: 'service_title'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 200 // column width in pixels
        },
        period_type: {
            name: 'period_type',
            label: $.lang.column.period_type, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'period_type', // index in data object
            search: {
                type: 'select',
                index: 'period_type',
                data: getPeriodTypesForSelect()
            },
            formatter: function (value) {
                var pr = getPeriodTypesForSelect();
                var item;
                for (var index in pr) {
                    item = pr[index];
                    if (item.id === value) {
                        return item.id + ': ' + item.title;
                    }
                }

                return value;
            },
            width: 100 // column width in pixels
        },
        code: {
            name: 'code',
            label: $.lang.column.code, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'code', // index in data object
            search: {
                type: 'int',
                index: 'code'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 90 // column width in pixels
        },
        inn: {
            name: 'inn',
            label: $.lang.column.inn, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'inn', // index in data object
            search: {
                type: 'int',
                index: 'inn'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 90 // column width in pixels
        },
        jur_name: {
            name: 'jur_name',
            label: $.lang.column.jur_name,
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'jur_name', // index in data object
            search: {
                type: 'text',
                index: 'jur_name'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 200 // column width in pixels
        },
        client_contact: {
            name: 'client_contact',
            label: $.lang.column.contact_name,
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'client_contact', // index in data object
            search: null,
            formatter: function (value, row) {
                var s = row.contact_name;
                if (row.client_phone) {
                    s += '<br><abbr title="' + $.lang.phone + '">' + $.lang.phone_sh + ': </abbr>' + row.client_phone;
                }
                if (row.client_email) {
                    s += '<br><a href="mailto:' + row.client_email + '">' + row.client_email + '</a>';
                }
                if (row.client_address) {
                    s += '<br>' + row.client_address;
                }
                return s;
            },
            width: 150 // column width in pixels
        },
        client_debt: {
            name: 'client_debt',
            label: $.lang.column.client_debt,
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'current_month_debt_percent', // index in data object
            search: {
                type: 'int',
                index: 'current_month_debt_percent'
            },
            formatter: function (value, row) {
                var s = '';
                if (row.plan_total_current_month_amount) {
                    s += '<abbr title="' + $.lang.plan_total_current_month_amount + '">P: </abbr><strong>' + row.plan_total_current_month_amount.format(2) + '</strong>';
                }
                if (row.total_period_charge) {
                    s += '<br><abbr title="' + $.lang.total_period_charge + '">T: </abbr><strong>' + row.total_period_charge.format(2) + '</strong>';
                }
                if (row.current_month_debt_amount !== null) {
                    s += '<br><abbr title="' + $.lang.debt + '">D: </abbr><strong>' + row.current_month_debt_amount.format(2) + '</strong>';
                    if (row.current_month_debt_percent !== null) {
                        s += ' (<strong>' + row.current_month_debt_percent.format(2) + '%</strong>)';
                    }
                }

                return s;
            },
            width: 100 // column width in pixels
        },
        client_address: {
            name: 'client_address',
            label: $.lang.column.address,
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'client_address', // index in data object
            search: null,
            formatter: null,
            width: 150 // column width in pixels
        },
        address: {
            name: 'address',
            label: $.lang.column.address,
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'address', // index in data object
            search: null,
            formatter: null,
            width: 250 // column width in pixels
        },
        district: {
            name: 'district',
            label: $.lang.column.district,
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'district', // index in data object
            search: null,
            formatter: null,
            width: 100 // column width in pixels
        },
        title: {
            name: 'title',
            label: $.lang.column.tm, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'title', // index in data object
            search: {
                type: 'text',
                index: 'title'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 180 // column width in pixels
        },
        description: {
            name: 'description',
            label: $.lang.column.description, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'description', // index in data object
            search: {
                type: 'text',
                index: 'description'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 200 // column width in pixels
        },
        quantity: {
            name: 'quantity',
            label: $.lang.column.quantityShort, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'quantity', // index in data object
            search: null,
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 90 // column width in pixels
        },
        service_price: {
            name: 'service_price',
            label: $.lang.column.price, // can be function
            cssclass: 'table-cell-strong money',
            align: 'right', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'price', // index in data object
            search: null,
            formatter: function (value, row) {
                var price = strToFloat(row.price);
                if (row.have_alcohol) {
                    var priceAlcohol = strToFloat(row.price_alcohol);
                    return price.format(2) + ' / ' + priceAlcohol.format(2);
                }

                return price.format(2);
            },
            width: 90 // column width in pixels
        },
        maket: {
            name: 'maket',
            label: $.lang.column.maketShort, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'maket', // index in data object
            search: null,
            formatter: function (value, row) {
                if (value) {
                    return '<span class="glyphicon glyphicon-picture span-maket" data-id="' + value + '"></span>';
                } else {
                    return '';
                }
            },
            width: 40 // column width in pixels
        },
        doc_list: {
            name: 'doc_list',
            label: $.lang.column.taskDoc, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'doc_list', // index in data object
            search: null,
            formatter: function (value, row) {
                if (!value) {
                    return '';
                }

                var arr = value.split(',');
                value = '';
                for (var i in arr) {
                    value += '<span class="glyphicon glyphicon-open-file span-doc" data-id="' + arr[i] + '"></span>';
                }

                return value;
            },
            width: 100 // column width in pixels
        },
        period_start: {
            name: 'period_start',
            label: $.lang.column.periodStart,
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'period_start', // index in data object
            search: null,
            formatter: function (value) {
                if (!value) {
                    return '';
                }
                var d = periodDate(value);
                return dateWin(d);
            },
            width: 150 // column width in pixels
        },
        period_end: {
            name: 'period_end',
            label: $.lang.column.periodEnd,
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'period_end', // index in data object
            search: null,
            formatter: function (value) {
                if (!value) {
                    return '';
                }
                var d = periodDate(value);
                return dateWin(d);
            },
            width: 150 // column width in pixels
        },
        date_open: {
            name: 'date_open',
            label: $.lang.column.dateOpen,
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'date_open', // index in data object
            search: null,
            formatter: function (value) {
                if (!value) {
                    return '';
                }
                var d = periodDate(value);
                return dateWin(d);
            },
            width: 150 // column width in pixels
        },
        action_client: {
            name: 'action_client',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: null,
            formatter: function (value, row) {
                var s = '';
                s += '<button type="button" class="btn btn-info action-clients-edit" data-id="' + row.client + '" title="' + $.lang.change + '"><span class="glyphicon glyphicon-info-sign"></span> ' + $.lang.change + '</button>';
                s += '<button type="button" class="btn btn-danger action-clients-delete" data-id="' + row.client + '" title="' + $.lang.remove + '"><span class="glyphicon glyphicon-remove-sign"></span> ' + $.lang.remove + '</button>';
                return s;
            },
            width: 150 // column width in pixels
        },
        action_category: {
            name: 'action_category',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: null,
            formatter: function (value, row) {
                var s = '';
                s += '<button type="button" class="btn btn-info action-categories-edit" data-id="' + row.category + '" title="' + $.lang.change + '"><span class="glyphicon glyphicon-info-sign"></span> ' + $.lang.change + '</button>';
                return s;
            },
            width: 120 // column width in pixels
        },
        action_task_edit: {
            name: 'action_task_edit',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: null,
            formatter: function (value, row) {
                var bgcolor = '';
                if (row.color) {
                    bgcolor = ' style="background-color: #' + row.color + '"';
                }

                var s = '';
                s += '<div' + bgcolor + '><button type="button" class="btn btn-info action-task-edit" data-id="' + row.task + '" title="' + $.lang.change + '"><span class="glyphicon glyphicon-info-sign"></span></button></div>';
                return s;
            },
            width: 50 // column width in pixels
        },
        action_task_plan: {
            name: 'action_task_plan',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: null,
            formatter: function (value, row) {
                var bgcolor = '';
                if (row.color) {
                    bgcolor = ' style="background-color: #' + row.color + '"';
                }

                var s = '';
                s += '<div' + bgcolor + '><button type="button" class="btn btn-info action-task-plan" data-id="' + row.task + '" title="' + $.lang.change + '"><span class="glyphicon glyphicon-info-sign"></span></button></div>';
                return s;
            },
            width: 50 // column width in pixels
        },
        action_task_check: {
            name: 'action_task_check',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: {type: 'check'},
            formatter: function (value, row) {
                return '<input type="checkbox" class="action-task-check" data-id="' + row.task + '"/>';
            },
            width: 50 // column width in pixels
        },
        login: {
            name: 'login',
            label: $.lang.column.login, // can be function
            align: 'left', // left, right, center
            cssclass: 'table-cell-strong',
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'login', // index in data object
            search: {
                type: 'text',
                index: 'login'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 220 // column width in pixels
        },
        name: {
            name: 'name',
            label: $.lang.column.name, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'name', // index in data object
            search: {
                type: 'text',
                index: 'name'
            },
            formatter: null, // can be function that reformat viewed cell by cell value
            width: 220 // column width in pixels
        },
        email: {
            name: 'email',
            label: $.lang.column.email,
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'email', // index in data object
            search: {
                type: 'text',
                index: 'email'
            },
            formatter: function (value, row) {
                if (value) {
                    return '<a href="mailto:' + value + '">' + value + '</a>';
                }
                return value;
            },
            width: 150 // column width in pixels
        },
        role: {
            name: 'role',
            label: $.lang.column.role, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'role', // index in data object
            search: {
                type: 'select',
                index: 'role',
                data: getRolesForSelect()
            },
            formatter: function (value, row) {
                if ($.lang.role[value]) {
                    return $.lang.role[value];
                }
                return value;
            }, // can be function that reformat viewed cell by cell value
            width: 100 // column width in pixels
        },
        state: {
            name: 'state',
            label: $.lang.column.state, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'state', // index in data object
            search: {
                type: 'select',
                index: 'state',
                default: 's1',
                data: getStatesForSelect(),
                map: function (v) {
                    switch (v) {
                        case 's0':
                            return 0;
                        case 's1':
                            return 1;
                    }
                    return null;
                }
            },
            formatter: function (value, row) {
                var cl;
                if (value) {
                    cl = 'state-enabled';
                } else {
                    cl = 'state-disabled';
                }
                if ($.lang.state[value]) {
                    value = $.lang.state[value];
                }
                return '<span class="' + cl + '">' + value + '</span>';
            }, // can be function that reformat viewed cell by cell value
            width: 50 // column width in pixels
        },
        use_in_list: {
            name: 'use_in_list',
            label: $.lang.column.use_in_list, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: true, // true or false
            index: 'use_in_list', // index in data object
            search: {
                type: 'select',
                index: 'use_in_list',
                default: 's1',
                data: getStatesForSelect(),
                map: function (v) {
                    switch (v) {
                        case 's0':
                            return 0;
                        case 's1':
                            return 1;
                    }
                    return null;
                }
            },
            formatter: function (value, row) {
                var cl;
                if (value) {
                    cl = 'state-enabled';
                } else {
                    cl = 'state-disabled';
                }
                if ($.lang.state[value]) {
                    value = $.lang.state[value];
                }
                return '<span class="' + cl + '">' + value + '</span>';
            }, // can be function that reformat viewed cell by cell value
            width: 50 // column width in pixels
        },
        action_user: {
            name: 'action_user',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: null,
            formatter: function (value, row) {
                var s = '';
                s += '<button type="button" class="btn btn-info action-users-edit" data-id="' + row.login + '" title="' + $.lang.change + '"><span class="glyphicon glyphicon-info-sign"></span> ' + $.lang.change + '</button>';
                s += '<button type="button" class="btn btn-danger action-users-delete" data-id="' + row.login + '" title="' + $.lang.remove + '"><span class="glyphicon glyphicon-remove-sign"></span> ' + $.lang.remove + '</button>';
                return s;
            },
            width: 150 // column width in pixels
        },
        action_shop: {
            name: 'action_shop',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: null,
            formatter: function (value, row) {
                var s = '';
                s += '<button type="button" class="btn btn-info action-shops-edit" data-id="' + row.shop + '" title="' + $.lang.change + '"><span class="glyphicon glyphicon-info-sign"></span> ' + $.lang.change + '</button>';
                s += '<button type="button" class="btn btn-danger action-shops-delete" data-id="' + row.shop + '" title="' + $.lang.remove + '"><span class="glyphicon glyphicon-remove-sign"></span> ' + $.lang.remove + '</button>';
                return s;
            },
            width: 150 // column width in pixels
        },
        action_service: {
            name: 'action_service',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: null,
            formatter: function (value, row) {
                var s = '';
                s += '<button type="button" class="btn btn-info action-services-edit" data-id="' + row.service + '" title="' + $.lang.change + '"><span class="glyphicon glyphicon-info-sign"></span> ' + $.lang.change + '</button>';
                return s;
            },
            width: 150 // column width in pixels
        }

    };

    function buildColModel() {
        // make additional columns
        var p;
        for (var i = 0; i < _period_count; i++) {
            p = incPeriod(getPeriodStart(), i);
            _colModel['period_' + i] = {
                name: 'period_' + i,
                label: getMonthName(p), // can be function
                align: 'center', // left, right, center
                cssclass: 'table-cell-strong',
                title: function (a, row) {
                    if (!a) {
                        return '';
                    }
                    var A = a.split(';');
                    var isCharged = strToIntDefault(A[0], 0) > 0;
                    //var isChecked = strToIntDefault(A[1], 0) > 0;

                    if (!isCharged) {
                        return '';
                    }

                    var quantity = strToIntDefault(row.quantity, 0);
                    var price = strToFloat(row.price);
                    var amount = strToFloatDefault(row.amount, 0);

                    if (price && quantity > 0) {
                        return quantity + ' ' + $.lang.unit + ' x ' + price.format(2) + ' ' + $.lang.currency_hi[_country];
                    }
                    if (amount > 0 && quantity > 0) {
                        return quantity + ' ' + $.lang.unit + ': ' + amount.format(2) + ' ' + $.lang.currency_hi[_country];
                    }
                    if (quantity > 0) {
                        return quantity + ' ' + $.lang.unit;
                    }
                    return '';
                },
                sortable: false, // true or false
                index: 'period_' + i, // index in data object
                search: null,
                formatter: function (a, row, c) {
                    if (!a) {
                        return '';
                    }

                    var A = a.split(';');
                    var isCharged = strToIntDefault(A[0], 0) > 0;
                    var isChecked = strToIntDefault(A[1], 0) > 0;

                    //var quantity = strToIntDefault(row.quantity, 0);
                    //var price = strToFloat(row.price);
                    var amount = strToFloatDefault(row.amount, 0);
                    var payment = strToFloatDefault(row.payment, -1);
                    var chargedAmount = amount;

                    var fgClass = '';
                    if (payment >= 0) {
                        chargedAmount = payment;
                        fgClass = 'payment';
                    }

                    var pIndex = strToIntDefault(strReplace(c, 'period_', ''), -1);
                    var pe = '';
                    if (pIndex >= 0) {
                        pe = periodSQL(incPeriod(getPeriodStart(getTasksPeriod()), pIndex));
                    }

                    var chClass;
                    var chValue;
                    if (isChecked) {
                        chClass = 'green';
                        chValue = 'checked="checked"';
                    } else {
                        chValue = '';
                        if (pe <= getCurrentPeriod()) {
                            chClass = 'yellow';
                        } else {
                            chClass = 'orange';
                        }
                    }

                    if (isCharged) {
                        return '<div class="table-cell-period-money ' + chClass + ' ' + fgClass + ' tcpm-charge-' + row.task + '-' + pe + '"><input type="checkbox" value="1" data-id="' + row.task + '" data-period="' + pe + '" ' + chValue + ' title="' + $.lang.isPayed + '">' + chargedAmount.format(2) + '<span class="glyphicon glyphicon-briefcase" data-id="' + row.task + '" data-period="' + pe + '"></span></div>';
                    }

                    var peFullDate = periodDate(pe + '-01');
                    var rps = periodDate(row.period_start);
                    rps.setDate(1);
                    var rpe = periodDate(row.period_end);

                    if (peFullDate >= rps && peFullDate <= rpe) {
                        return '<div class="table-cell-period-money ' + chClass + '">&nbsp;</div>';
                    }

                    return '';
                },
                width: 130 // column width in pixels
            };
        }
    }

    // Menu

    function onConfig() {
        console.log('config');
    }

    function onExit() {
        console.log('exit');
    }

    // USERS

    function onUsers() {
        //console.log('users');
        $('.content-tab').hide();
        setCaption($.lang.users);
        $('#users-container').empty();
        _table = new apTable($, 'users-container', false);
        _table.setColsModel(_colModel);
        _table.init(['login', 'name', 'email', 'role', 'state', 'action_user']);
        _table.setOnPageChange(function (event) {
            loadUsers(event.data.page);
        });
        _table.setOnSort(function () {
            loadUsers(_table.getPage());
        });
        _table.setOnFilter(function () {
            loadUsers(1);
        });
        loadUsers(1);
        $('#content-users').show();
    }

    function loadUsers(page) {
        taskAdd('loadUsers');
        var p = {};
        p.page = page;
        p.count = _tableMaxCount;
        var sort = _table.getSort();
        if (sort) {
            p.sort_col = sort.colIndex;
            p.sort_dir = (sort.isAsc) ? 'asc' : 'desc';
        } else {
            _table.setSort('login', true); // make it default
        }
        var filter = _table.getFilter();
        $.extend(p, filter);
        $.saAPI.query('user_list', p, onLoadUsers, function (xhr, status, error) {
            onError(xhr, status, error);
            bindUsers();
            taskRemove('loadUsers');
        });
    }

    function onLoadUsers(data) {
        _table.setPagerData(data.rstart, data.rcount, data.count);
        _table.loadData(data.user_list);
        bindUsers();
        taskRemove('loadUsers');
    }

    function bindUsers() {
        $('.action-users-add').off('click');
        $('.action-users-edit').off('click');
        $('.action-users-delete').off('click');
        $('.action-users-add').click(onUserAdd);
        $('.action-users-edit').click(function () {
            var u = $(this).attr('data-id');
            if (u) {
                onUserEdit(u);
            }
        });
        $('.action-users-delete').click(function () {
            var u = $(this).attr('data-id');
            if (u && window.confirm($.lang.confirmDelete)) {
                onUserDelete(u);
            }
        });
    }

    function onUserAdd() {
        $.saModalUser.add(function (c) {
            loadUsers(_table.getPage());
        });
    }

    function onUserEdit(u) {
        $.saModalUser.edit(u, function (c) {
            loadUsers(_table.getPage());
        });
    }

    function onUserDelete(u) {
        $.saAPI.query('user_delete', {login: u}, function () {
            loadUsers(_table.getPage());
        }, onError);
    }

    // CLIENTS

    function onClients() {
        //console.log('clients');
        $('.content-tab').hide();
        setCaption($.lang.clients);
        $('#clients-container').empty();
        _table = new apTable($, 'clients-container', false);
        _table.setColsModel(_colModel);
        _table.init(['client_debt', 'client_title', 'code', 'inn', 'jur_name', 'state', 'use_in_list', 'action_client']);
        _table.setOnPageChange(function (event) {
            loadClients(event.data.page);
        });
        _table.setOnSort(function () {
            loadClients(_table.getPage());
        });
        _table.setOnFilter(function () {
            loadClients(1);
        });
        loadClients(1);
        $('#content-clients').show();
    }

    function loadClients(page) {
        taskAdd('loadClients');
        var p = {};
        p.service_type = 'A';
        p.page = page;
        p.count = _tableMaxCount;
        var sort = _table.getSort();
        if (sort) {
            p.sort_col = sort.colIndex;
            p.sort_dir = (sort.isAsc) ? 'asc' : 'desc';
        } else {
            _table.setSort('client_title', true); // make it default
        }
        var filter = _table.getFilter();
        $.extend(p, filter);
        $.saAPI.query('client_list', p, onLoadClients, function (xhr, status, error) {
            onError(xhr, status, error);
            bindClients();
            taskRemove('loadClients');
        });
    }

    function onLoadClients(data) {
        _table.setPagerData(data.rstart, data.rcount, data.count);
        _table.loadData(data.client_list);
        bindClients();
        taskRemove('loadClients');
    }

    function bindClients() {
        $('.action-clients-add').off('click');
        $('.action-clients-edit').off('click');
        $('.action-clients-delete').off('click');
        $('.client-title').off('click');
        $('.action-clients-filter-20').off('click');

        $('.client-title').click(function () {
            var client = $(this).attr('data-id');
            client = strToIntDefault(client, 0);
            if (client > 0) {
                onIFrameContent($.lang.client + ' #' + client, 'client/' + client + '/info');
            }
        });
        $('.action-clients-add').click(onClientAdd);
        $('.action-clients-filter-20').click(function () {
            var isChecked = $(this).prop('checked');
            if (isChecked) {
                _table.setFilterData('client_debt', 20);
            } else {
                _table.setFilterData('client_debt', '');
            }
            loadClients(1);
        });
        $('.action-clients-edit').click(function () {
            var client = $(this).attr('data-id');
            client = strToIntDefault(client, 0);
            if (client > 0) {
                onClientEdit(client);
            }
        });
        $('.action-clients-delete').click(function () {
            var client = $(this).attr('data-id');
            client = strToIntDefault(client, 0);
            if (client > 0 && window.confirm($.lang.confirmDelete)) {
                onClientDelete(client);
            }
        });
    }

    function onClientAdd() {
        $.saModalClient.add(function (c) {
            loadClients(_table.getPage());
            loadClientsCache();
        });
    }

    function onClientEdit(client) {
        $.saModalClient.edit(client, function (c) {
            loadClients(_table.getPage());
            loadClientsCache();
        });
    }

    function onClientDelete(client) {
        $.saAPI.query('client_delete', {client: client}, function () {
            loadClients(_table.getPage());
            loadClientsCache();
        }, onError);
    }

    // CATEGORIES

    function onCategories() {
        //console.log('categories');
        $('.content-tab').hide();
        setCaption($.lang.categories);
        $('#categories-container').empty();
        _table = new apTable($, 'categories-container', false);
        _table.setColsModel(_colModel);
        _table.init(['category_group_title', 'category_title', 'action_category']);
        _table.setOnPageChange(function (event) {
            loadCategories(event.data.page);
        });
        _table.setOnSort(function () {
            loadCategories(_table.getPage());
        });
        _table.setOnFilter(function () {
            loadCategories(1);
        });
        loadCategories(1);
        $('#content-categories').show();
    }

    function loadCategories(page) {
        taskAdd('loadCategories');
        var p = {};
        p.page = page;
        p.count = _tableMaxCount;
        var sort = _table.getSort();
        if (sort) {
            p.sort_col = sort.colIndex;
            p.sort_dir = (sort.isAsc) ? 'asc' : 'desc';
        } else {
            _table.setSort('category_title', true); // make it default
        }
        var filter = _table.getFilter();
        $.extend(p, filter);
        $.saAPI.query('category_list', p, onLoadCategories, function (xhr, status, error) {
            onError(xhr, status, error);
            bindClients();
            taskRemove('loadCategories');
        });
    }

    function onLoadCategories(data) {
        _table.setPagerData(data.rstart, data.rcount, data.count);
        _table.loadData(data.category_list);
        bindCategories();
        taskRemove('loadCategories');
    }

    function bindCategories() {
        $('.action-categories-add').off('click');
        $('.action-categories-edit').off('click');
        //$('.action-categories-delete').off('click');
        $('.action-categories-add').click(onCategoryAdd);
        $('.action-categories-edit').click(function () {
            var category = $(this).attr('data-id');
            category = strToIntDefault(category, 0);
            if (category > 0) {
                onCategoryEdit(category);
            }
        });
        // $('.action-categories-delete').click(function () {
        //   var category = $(this).attr('data-id');
        //   category = strToIntDefault(category, 0);
        //   if (category > 0 && window.confirm($.lang.confirmDelete)) {
        //     onCategoryDelete(category);
        //   }
        // });
    }

    function onCategoryAdd() {
        $.saModalCategory.categoryGroupsCacheSet(_categoryGroups);
        $.saModalCategory.add(function (c) {
            loadCategories(_table.getPage());
            loadCategoriesCache();
        });
    }

    function onCategoryEdit(client) {
        $.saModalCategory.categoryGroupsCacheSet(_categoryGroups);
        $.saModalCategory.edit(client, function (c) {
            loadCategories(_table.getPage());
            loadCategoriesCache();
        });
    }

    // function onCategoryDelete(category) {
    //   $.saAPI.query('category_delete', {category: category}, function () {
    //     loadCategories(_table.getPage());
    //     loadCategoriesCache();
    //   }, onError);
    // }

    // SERVICES

    function onServices() {
        //console.log('services');
        $('.content-tab').hide();
        setCaption($.lang.services);
        $('#services-container').empty();
        _table = new apTable($, 'services-container', false);
        _table.setColsModel(_colModel);
        _table.init(['service', 'service_title', 'period_type', 'service_price', 'state', 'action_service']);
        _table.setOnPageChange(function (event) {
            loadServices(event.data.page);
        });
        _table.setOnSort(function () {
            loadServices(_table.getPage());
        });
        _table.setOnFilter(function () {
            loadServices(1);
        });
        loadServices(1);
        $('#content-services').show();
    }

    function loadServices(page) {
        taskAdd('loadServices');
        var p = {};
        p.page = page;
        p.count = _tableMaxCount;
        var sort = _table.getSort();
        if (sort) {
            p.sort_col = sort.colIndex;
            p.sort_dir = (sort.isAsc) ? 'asc' : 'desc';
        } else {
            _table.setSort('service', true); // make it default
        }
        var filter = _table.getFilter();
        $.extend(p, filter);
        $.saAPI.query('service_list', p, onLoadServices, function (xhr, status, error) {
            onError(xhr, status, error);
            bindServices();
            taskRemove('loadServices');
        });
    }

    function onLoadServices(data) {
        _table.setPagerData(data.rstart, data.rcount, data.count);
        _table.loadData(data.service_list);
        bindServices();
        taskRemove('loadServices');
    }

    function bindServices() {
        $('.action-services-edit').off('click');
        $('.action-services-edit').click(function () {
            var service = $(this).attr('data-id');
            service = strToIntDefault(service, 0);
            if (service > 0) {
                onServiceEdit(service);
            }
        });
    }

    function onServiceEdit(service) {
        $.saModalService.townsCacheSet(_towns);
        $.saModalService.edit(service, function (c) {
            loadServices(_table.getPage());
            loadServicesCache();
        });
    }

    // SHOPS

    function onShops() {
        //console.log('shops');
        $('.content-tab').hide();
        setCaption($.lang.shops);
        $('#shops-container').empty();
        _table = new apTable($, 'shops-container', false);
        _table.setColsModel(_colModel);
        _table.init(['shop', 'town_select', 'district', 'address', 'date_open', 'state', 'action_shop']);
        _table.setOnPageChange(function (event) {
            loadShops(event.data.page);
        });
        _table.setOnSort(function () {
            loadShops(_table.getPage());
        });
        _table.setOnFilter(function () {
            loadShops(1);
        });
        loadShops(1);
        loadShopsTownSelect();
        $('#content-shops').show();
    }

    function loadShopsTownSelect() {
        var r = [];
        _towns.start();
        var item;
        while (_towns.next()) {
            item = _towns.current();
            r.push({
                id: item.town,
                title: item.title
            });
        }
        _table.setFilterSearchData('town_select', r);
    }

    function loadShops(page) {
        taskAdd('loadShops');
        var p = {};
        p.page = page;
        p.count = _tableMaxCount;
        var sort = _table.getSort();
        if (sort) {
            p.sort_col = sort.colIndex;
            p.sort_dir = (sort.isAsc) ? 'asc' : 'desc';
        } else {
            _table.setSort('shop', true); // make it default
        }
        var filter = _table.getFilter();
        $.extend(p, filter);
        $.saAPI.query('shop_list', p, onLoadShops, function (xhr, status, error) {
            onError(xhr, status, error);
            bindShops();
            taskRemove('loadShops');
        });
    }

    function onLoadShops(data) {
        _table.setPagerData(data.rstart, data.rcount, data.count);
        _table.loadData(data.shop_list);
        bindShops();
        taskRemove('loadShops');
    }

    function bindShops() {
        $('.action-shops-add').off('click');
        $('.action-shops-edit').off('click');
        $('.action-shops-delete').off('click');
        $('.action-shops-add').click(onShopAdd);
        $('.action-shops-edit').click(function () {
            var shop = $(this).attr('data-id');
            shop = strToIntDefault(shop, 0);
            if (shop > 0) {
                onShopEdit(shop);
            }
        });
        $('.action-shops-delete').click(function () {
            var shop = $(this).attr('data-id');
            shop = strToIntDefault(shop, 0);
            if (shop > 0 && window.confirm($.lang.confirmDelete)) {
                onShopDelete(shop);
            }
        });
    }

    function onShopAdd() {
        $.saModalShop.townsCacheSet(_towns);
        $.saModalShop.add(function (c) {
            loadShops(_table.getPage());
            loadShopsCache();
        });
    }

    function onShopEdit(shop) {
        $.saModalShop.townsCacheSet(_towns);
        $.saModalShop.edit(shop, function (c) {
            loadShops(_table.getPage());
            loadShopsCache();
        });
    }

    function onShopDelete(shop) {
        $.saAPI.query('shop_delete', {shop: shop}, function () {
            loadShops(_table.getPage());
            loadShopsCache();
        }, onError);
    }

    // SERVICE

    var _service = 0;

    function onService(service) {
        $('.content-tab').hide();
        var serv = _services.get(service);
        if (!serv) {
            return;
        }
        _service = service;
        setCaption(serv.title);
        $('#tasks-container').empty();
        _table = new apTable($, 'tasks-container', true);
        _table.setColsModel(_colModel);
        var cols = new Array();
        cols.push('action_task_check');
        if (serv.have_plan_list) {
            cols.push('action_task_plan');
        } else {
            cols.push('action_task_edit');
        }
        cols.push('client_select');
        cols.push('code');
        if (!serv.have_plan_list) {
            cols.push('title');
        }
        if (serv.have_description) {
            cols.push('description');
        }
        if (serv.have_maket) {
            cols.push('maket');
        }
        if (serv.have_doc) {
            cols.push('doc_list');
        }
        for (var i = 0; i < _period_count; i++) {
            cols.push('period_' + i);
        }

        _table.init(cols);
        _table.setOnPageChange(function (event) {
            loadTasks(event.data.page);
        });
        _table.setOnSort(function () {
            loadTasks(_table.getPage());
        });
        _table.setOnFilter(function () {
            loadTasks(1);
        });
        _table.setOnFilterCheck(function (e) {
            checkAllRows(e);
        });
        loadTasksPeriod();
        loadTasks(1);
        $('#content-tasks').show();
    }

    function serviceTabShow() {
        $('.content-tab').hide();
        var serv = _services.get(_service);
        if (!serv) {
            return;
        }
        setCaption(serv.title);
        $('#content-tasks').show();
        loadTasks(_table.getPage());
    }

    function checkAllRows(e) {
        var obj = e.currentTarget;
        var ch = obj.checked;

        $('input.action-task-check').each(function () {
            this.checked = ch;
        });
        if (ch) {
            $('input.action-task-check').attr('checked', 'checked');
        } else {
            $('input.action-task-check').removeAttr('checked');
        }
    }

    function updateTasksColLabel() {
        var ps = getTasksPeriod();
        var d = getPeriodStart(ps);
        var p;
        for (var i = 0; i < _period_count; i++) {
            p = incPeriod(d, i);
            _table.setColLabel('period_' + i, getMonthName(p));
        }
    }

    function loadTasks(page) {
        var p = {};
        // page
        p.service = _service;
        p.page = page;
        p.period = getTasksPeriod();
        // sort order
        var sort = _table.getSort();
        if (sort) {
            p.sort_col = sort.colIndex;
            p.sort_dir = (sort.isAsc) ? 'asc' : 'desc';
        } else {
            _table.setSort('client', true); // make it default
        }
        // filter
        var filter = _table.getFilter();
        $.extend(p, filter);
        // action
        taskAdd('loadTasks');
        $.saAPI.query('service_task_list', p, onLoadTasks, function (xhr, status, error) {
            onError(xhr, status, error);
            bindPage();
            taskRemove('loadTasks');
        });
    }

    function onLoadTasks(data) {
        _table.setPagerData(data.rstart, data.rcount, data.count);
        _table.loadData(data.task_list);
        updateTasksColLabel();
        bindPage();
        taskRemove('loadTasks');
    }

    function getCheckedTaskList() {
        var taskList = [];

        $('input.action-task-check:checked').each(function () {
            var task = $(this).attr('data-id');
            task = strToIntDefault(task, 0);
            if (task) {
                taskList.push(task);
            }
        });

        return taskList;
    }

    function bindPage() {

        $('.action-task-add').off('click');
        $('.action-task-add').hide();
        $('#action-task-import').off('click');
        $('#action-task-import').hide();
        $('.action-task-edit').off('click');
        $('.action-task-plan').off('click');
        $('.action-task-delete').off('click');
        $('.action-task-plans-back').off('click');
        $('.action-task-plans-save').off('click');
        $('#tasks-period').off('change');
        $('.span-maket').off('hover click');
        $('.span-doc').off('click');
        $('.table-cell-period-money span').off('click');
        $('.table-cell-period-money input[type="checkbox"]').off('');
        $('#report-shop-task-period').off('change');
        $('#report-shop-task-shop').off('change');
        $('#report-dislocation-get').off('click');
        $('#report-charge-get').off('click');
        $('#report-promo-get').off('click');
        $('#report-prikassa-get').off('click');
        $('#action-tasks-copy').off('click');
        $('#task-plans-container').off('click');
        $('#tasks-copy').hide();

        $('#report-shop-task-period').change(function () {
            loadReportShopTask(1);
            //loadReportShopTaskClientSelect();
        });
        $('#report-shop-task-shop').change(function () {
            loadReportShopTask(1);
            //loadReportShopTaskClientSelect();
        });

        $('.action-task-edit').click(function () {
            var task = $(this).attr('data-id');
            task = strToIntDefault(task, 0);
            if (task > 0) {
                onTaskEdit(task);
            }
        });
        $('.action-task-plan').click(function () {
            var task = $(this).attr('data-id');
            task = strToIntDefault(task, 0);
            if (task > 0) {
                onTaskPlan(task);
            }
        });
        $('.action-task-delete').click(function () {
            var taskList = getCheckedTaskList();
            if (taskList.length <= 0) {
                return;
            }
            if (!window.confirm($.lang.confirmDelete + ' [' + taskList.length + ']')) {
                return;
            }
            onTaskDelete(taskList);
        });
        $('#tasks-period').change(function () {
            loadTasks(1);
            loadTasksClientSelect();
        });
        $('.span-maket').popover({
            trigger: 'focus',
            html: true,
            container: '#wrap',
            placement: 'bottom',
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                var maket = $(this).attr('data-id');
                if (!maket) {
                    return false;
                }
                return '<img src="' + $.saAPI.urlMaket(maket) + '" alt="" style="max-width: ' + _maket_w_max + 'px">';
            }
        });
        $('.span-maket').hover(function () {
            $(this).popover('show');
        }, function () {
            $(this).popover('hide');
        }).click(function () {
            var maket = $(this).attr('data-id');
            if (!maket) {
                return false;
            }

            $('<a href="' + $.saAPI.urlMaket(maket) + '" target="_blank" style="display: none" class="maket-opennew">...</a>').appendTo('body');
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            $('a.maket-opennew')[0].dispatchEvent(evt);
            $('a.maket-opennew').remove();
        });
        $('.span-doc').click(function () {
            var doc = $(this).data('id');
            if (doc) {
                document.location = $.saAPI.urlDoc(doc);
            }
        });
        $('.table-cell-period-money span').click(function () {
            var task = $(this).attr('data-id');
            if (!task) {
                return false;
            }
            var period = $(this).attr('data-period');
            if (!period) {
                return false;
            }

            $.saModalTaskPeriod.execute(task, period);
        });
        $('.table-cell-period-money input[type="checkbox"]').click(function () {
            var task = $(this).attr('data-id');
            if (!task) {
                return false;
            }
            var period = $(this).attr('data-period');
            if (!period) {
                return false;
            }
            var checked = this.checked;

            updateTaskPayed(task, period, checked);
        });
        $('#report-dislocation-get').click(onReportDislocationGet);
        $('#report-charge-get').click(onReportChargeGet);
        $('#report-promo-get').click(onReportPromoGet);
        $('#report-prikassa-get').click(onReportPrikassaGet);

        var serv = _services.get(_service);
        if (!serv) {
            return;
        }
        var serviceId = _service;
        if (serv.have_plan_list || serv.import_access) {
            $('#action-task-import').show();

            $('#action-task-import-fileupload').fileupload({
                dataType: 'jsonp',
                forceIframeTransport: true,
                redirect: _redirect,
                done: function (e, data) {
                    if (data.result) {
                        if (data.result.list !== undefined) {
                            var msg = '';
                            if (data.result.list) {
                                loadTasks(_table.getPage());
                                loadClientsCache();
                                loadTasksClientSelect();

                                var tasks = data.result.list.toString().split(',');
                                msg = $.lang.importedCount;
                                msg = strReplace(msg, '{1}', tasks.length);
                            } else {
                                msg = $.lang.importedNothing;
                            }
                            alert(msg);
                        } else if (data.result.error) {
                            onError('', '', data.result.error);
                        } else {
                            onError('', '', {code: $.saErr.ERR_CLIENT, msg: 'wrong file'});
                        }
                    } else {
                        onError('', '', {code: $.saErr.ERR_CLIENT, msg: 'client error'});
                    }
                    taskRemove('taskImport');
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('#action-task-import-fileupload').bind('fileuploadsubmit', function (e, data) {
                taskAdd('taskImport');
                data.url = $.saAPI.urlAPI();
                data.formData = {
                    sid: $.saAPI.sid(),
                    version: $.saAPI.version(),
                    service: serviceId,
                    period: getTasksPeriod(),
                    action: 'task_import'
                };
            });

            $('.action-task-plans-back').on('click', function () {
                if (_service) {
                    serviceTabShow();
                }
            });

            $('.action-task-plans-save').on('click', function () {
                onTaskPlanSave(this);
            });

        }
        if (!serv.have_plan_list) {
            $('.action-task-add').show();
            $('.action-task-add').click(onTaskAdd);
        }
        if (serv.copy_access) {
            $('#action-tasks-copy').click(onTasksCopy);
            $('#tasks-copy').show();
            loadTasksCopyPeriodLoad();
        }

        $('#task-plans-container').on('click', '.task-plan-tm-row .task-plan-data-tm', function () {
            var tmIndex = $(this).data('tmindex');
            $('#task-plans-container').find('.task-plan-row-' + tmIndex).slideToggle(0);
        });
        $('#task-plans-container').on('keypress', '.task-plan-data-quantity input', function (e) {
            if (intKeypress(e)) {
                var planId = $(this).data('plan');
                if (planId) {
                    setTimeout('$.sa._doTaskPlanRowUpdateAmount(' + planId + ');', 100);
                }
                return true;
            }
            return false;
        });
    }

    function onTaskAdd() {
        reloadClientsServicePricesCache(_service);

        $.saModalTask.clientsCacheSet(_clients);
        $.saModalTask.categoriesCacheSet(_categories);
        $.saModalTask.shopsCacheSet(_shops);
        $.saModalTask.servicesCacheSet(_services);
        $.saModalTask.maketUploadConfig($.saAPI.urlAPI(), {
            sid: $.saAPI.sid(),
            version: $.saAPI.version(),
            action: 'maket_add'
        });
        $.saModalTask.docUploadConfig($.saAPI.urlAPI(), {
            sid: $.saAPI.sid(),
            version: $.saAPI.version(),
            action: 'doc_add'
        });
        $.saModalTask.setRedirect(_redirect);
        $.saModalTask.add(_service, function (c) {
            loadTasks(_table.getPage());
            if ($.saModalTask.isNewClient) {
                loadClientsCache();
            }
            loadTasksClientSelect();
        });
    }

    function onTaskEdit(task) {
        reloadClientsServicePricesCache(_service);

        $.saModalTask.clientsCacheSet(_clients);
        $.saModalTask.categoriesCacheSet(_categories);
        $.saModalTask.shopsCacheSet(_shops);
        $.saModalTask.servicesCacheSet(_services);
        $.saModalTask.maketUploadConfig($.saAPI.urlAPI(), {
            sid: $.saAPI.sid(),
            version: $.saAPI.version(),
            action: 'maket_add'
        });
        $.saModalTask.docUploadConfig($.saAPI.urlAPI(), {
            sid: $.saAPI.sid(),
            version: $.saAPI.version(),
            action: 'doc_add'
        });
        $.saModalTask.setRedirect(_redirect);
        $.saModalTask.edit(task, function (c) {
            loadTasks(_table.getPage());
            if ($.saModalTask.isChangeClient()) {
                loadTasksClientSelect();
            }
        });
    }

    function onTaskDelete(taskList) {
        $.saAPI.query('task_delete', {task_list: taskList}, function () {
            loadTasks(_table.getPage());
            loadTasksClientSelect();
        }, onError);
    }

    function loadTasksClientSelect() {
        taskAdd('loadTasksClientSelect');
        $.saAPI.query('period_client_list', {period: getTasksPeriod()}, onLoadTasksClientSelect, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadTasksClientSelect');
        });
    }

    function onLoadTasksClientSelect(data) {
        var r = new Array();
        for (var i in data.client_list) {
            r.push({
                id: data.client_list[i].client,
                title: data.client_list[i].title
            });
        }
        _table.setFilterSearchData('client_select', r);
        taskRemove('loadTasksClientSelect');
    }

    function loadTasksCopyPeriodLoad() {
        var period = new Date(getTasksPeriod());
        loadReportPeriod('#tasks-copy-period-from', period, 0, _period_count);
        loadReportPeriod('#tasks-copy-period-to', period, 0, _period_count);
        loadReportPeriodCount('#tasks-copy-period-count');
    }

    function getTasksCopyPeriodFrom() {
        return getReportPeriod('#tasks-copy-period-from');
    }

    function getTasksCopyPeriodTo() {
        return getReportPeriod('#tasks-copy-period-to');
    }

    function getTasksCopyPeriodCount() {
        return parseInt( $('#tasks-copy-period-count option:selected').val() );
    }

    function onTasksCopy() {
        var ps = getTasksCopyPeriodFrom();
        var pe = getTasksCopyPeriodTo();
        if (ps >= pe) {
            alert($.lang.tasksCopyPeriodWrong);
            return false;
        }

        var taskList = getCheckedTaskList();
        var periodCount = getTasksCopyPeriodCount();
        if (periodCount <= 0) {
            periodCount = 1;
        }

        taskAdd('tasksCopy');
        $.saAPI.query('tasks_copy', {
            period_s: ps,
            period_e: pe,
            period_count: periodCount,
            service: _service,
            task_list: taskList
        }, onTasksCopyDone, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('tasksCopy');
        });
    }

    function onTasksCopyDone() {
        taskRemove('tasksCopy');
        loadTasks(_table.getPage());
    }

    function updateTaskPayed(task, period, check) {
        taskAdd('updateTaskPayed');
        $.saAPI.query('task_period_check', {task: task, period: period, check: check}, function (data) {
            if (check) {
                $('.table-cell-period-money.tcpm-charge-' + task + '-' + period).removeClass('yellow').removeClass('orange').addClass('green');
            } else {
                var currentPeriod = getCurrentPeriod();
                if (period > currentPeriod) {
                    $('.table-cell-period-money.tcpm-charge-' + task + '-' + period).removeClass('green').removeClass('yellow').addClass('orange');
                } else {
                    $('.table-cell-period-money.tcpm-charge-' + task + '-' + period).removeClass('green').removeClass('orange').addClass('yellow');
                }
            }
            taskRemove('updateTaskPayed');
        }, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('updateTaskPayed');
        });
    }

    function getCurrentPeriod()
    {
        var ps = new Date();
        ps.setDate(1);

        return periodSQL(ps);
    }

    function loadTasksPeriod() {
        var ps = new Date();
        ps.setDate(1);
        var p = ps;
        var sel = $('#tasks-period').empty();
        for (var i = -_period_count; i < _period_count; i++) {
            p = incPeriod(ps, i);
            $('<option value="' + periodSQL(p) + '">' + getMonthName(p) + '</option>').appendTo(sel);
        }
        setTasksPeriod(periodSQL(ps));
        loadTasksClientSelect();
    }

    function setTasksPeriod(value) {
        $('#tasks-period').val(value);
        updateTasksColLabel();
    }

    function getTasksPeriod() {
        return $('#tasks-period option:selected').val();
    }

    // TASK PLAN

    function onTaskPlan(task) {
        if (!_service) {
            return;
        }
        $('.content-tab').hide();
        var serv = _services.get(_service);
        setCaption(serv.title);
        $('#task-plans-container').empty();
        $('#content-task-plans').show();
        loadTaskPlanTask(task);
        $('.action-task-plans-save').attr('data-task', task);
    }

    function loadTaskPlanTask(task) {
        var p = {
            task: task
        };
        // action
        taskAdd('loadTaskPlanTask');
        $.saAPI.query('task_info', p, onLoadTaskPlanTask, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadTaskPlanTask');
            onService(_service);
        });
    }

    function onLoadTaskPlanTask(data) {
        taskRemove('loadTaskPlanTask');
        var serv = _services.get(_service);
        var clientId = data.task.client;
        var clientData = _clients.get(clientId);
        var period = periodDate(data.task.period_start);

        setCaption(serv.title + ': ' + clientData.title + ' / ' + periodWin(period));
        loadTaskPlanList(data.task.task);
    }

    function loadTaskPlanList(task) {
        var p = {
            task: task
        };
        taskAdd('loadTaskPlanList');
        $.saAPI.query('task_plan_list', p, onLoadTaskPlanList, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadTaskPlanList');
            onService(_service);
        });
    }

    function onLoadTaskPlanList(data) {
        bindPage();
        taskRemove('loadTaskPlanList');

        var container = $('#task-plans-container');
        var table = $('<table class="table table-bordered table-striped table-hover"></table>');
        var thead = $('<thead></thead>').appendTo(table);
        var tbody = $('<tbody></tbody>').appendTo(table);
        var tr;
        var plan;
        var tm = null;
        var tmIndex = 0;
        var amount = 0.0;
        var price = 0.0;
        var quantity = 0;
        var tmTotal = {
            q: 0,
            a: 0.0,
            p: function () {
                return this.q > 0 ? this.a / this.q : 0.0;
            },
            clear: function () {
                this.q = 0;
                this.a = 0;
            }
        };
        tr = $('<tr></tr>');
        $('<th>' + $.lang.column.tm + ' / ' + $.lang.column.plan + '</th>').appendTo(tr);
        $('<th>' + $.lang.column.category + '</th>').appendTo(tr);
        $('<th>' + $.lang.column.price + '</th>').appendTo(tr);
        $('<th>' + $.lang.column.quantityShort + '</th>').appendTo(tr);
        $('<th>' + $.lang.column.amount + '</th>').appendTo(tr);
        tr.appendTo(thead);
        for (var index in data.task_plan_list) {
            if (data.task_plan_list.hasOwnProperty(index)) {
                plan = data.task_plan_list[index];
                if (tm != plan.tm) {
                    // apply prev. tmTotal
                    tr = tbody.find('.task-plan-tm-row-' + tmIndex);
                    tr.find('.task-plan-data-price').text(tmTotal.p().toFixed(2));
                    tr.find('.task-plan-data-quantity').text(tmTotal.q);
                    tr.find('.task-plan-data-amount').text(tmTotal.a.toFixed(2));

                    tmIndex++;
                    tm = plan.tm;
                    tmTotal.clear();

                    // add TM header
                    tr = $('<tr class="task-plan-tm-row task-plan-tm-row-' + tmIndex + '" data-tmindex="' + tmIndex + '"></tr>');
                    $('<td class="task-plan-data-tm" data-tmindex="' + tmIndex + '"><span class="glyphicon glyphicon-chevron-down"></span>&nbsp;' + plan.tm + '</td>').appendTo(tr);
                    $('<td class="task-plan-data-category">' +
                        '<select id="task-plan-data-category-select-' + tmIndex + '" class="task-plan-data-category-select selectpicker show-tick" data-selected-text-format="count > 2" data-live-search="true" data-categories="' + plan.category_list + '" multiple></select>' +
                        '</td>').appendTo(tr);
                    $('<td class="task-plan-data-price"></td>').appendTo(tr);
                    $('<td class="task-plan-data-quantity"></td>').appendTo(tr);
                    $('<td class="task-plan-data-amount"></td>').appendTo(tr);
                    tr.appendTo(tbody);
                }
                // add plan row
                price = strToFloatDefault(plan.price, 0.0);
                quantity = strToIntDefault(plan.quantity, 0);
                amount = price * quantity;

                tr = $('<tr class="task-plan-row task-plan-row-' + tmIndex + '" data-tmindex="' + tmIndex + '" data-plan="' + plan.plan + '" data-price="' + price + '"></tr>');
                $('<td class="task-plan-data-title" colspan="2">' + plan.title + '</td>').appendTo(tr);
                $('<td class="task-plan-data-price">' + price.toFixed(2) + '</td>').appendTo(tr);
                $('<td class="task-plan-data-quantity"><input data-plan="' + plan.plan + '" type="text" value="' + quantity + '"></td>').appendTo(tr);
                $('<td class="task-plan-data-amount">' + amount.toFixed(2) + '</td>').appendTo(tr);
                tr.appendTo(tbody);

                // update totals
                tmTotal.q += quantity;
                tmTotal.a += amount;
            }
        }
        // apply prev. totals
        tr = tbody.find('.task-plan-tm-row-' + tmIndex);
        tr.find('.task-plan-data-price').text(tmTotal.p().toFixed(2));
        tr.find('.task-plan-data-quantity').text(tmTotal.q);
        tr.find('.task-plan-data-amount').text(tmTotal.a.toFixed(2));

        // hide details
        tbody.find('.task-plan-row').hide();

        table.appendTo(container);
        reloadTaskPlanCategories(container);
    }

    function reloadTaskPlanCategories($container) {
        $container.find('.task-plan-data-category-select').each(function () {
            var $select = $(this);
            var categoriesData = $select.data('categories').toString();
            var categoriesList = [];
            if (categoriesData) {
                categoriesList = categoriesData.split(',');
                if (!Array.isArray(categoriesList) || categoriesList.length <= 0) {
                    categoriesList = [];
                }
            }

            $select.empty();
            _categories.start();
            var c = null;
            var styler = '';
            var selected = '';
            while (_categories.next()) {
                c = _categories.current();
                styler = 'data-content="<span class=\'label label-' + c.style + '\'>' + c.title + '</span>"';
                if ($.inArray(c.category, categoriesList) >= 0) {
                    selected = ' selected="selected"';
                } else {
                    selected = '';
                }
                $('<option value="' + c.category + '" ' + styler + selected + '>' + c.title + '</option>').appendTo($select);
            }
            $select.selectpicker();
        });
    }

    function doTaskPlanRowUpdateAmount(planId) {
        var container = $('#task-plans-container');
        var row = container.find('.task-plan-row[data-plan="' + planId + '"]');
        if (row.is('*')) {
            var price = strToFloatDefault(row.data('price'), 0.0);
            var quantity = strToIntDefault(row.find('.task-plan-data-quantity input').val(), 0);
            var amount = price * quantity;
            row.find('.task-plan-data-amount').text(amount.toFixed(2));

            var tmIndex = row.data('tmindex');
            var tmTotal = {
                q: 0,
                a: 0.0,
                p: function () {
                    return this.q > 0 ? this.a / this.q : 0.0;
                },
                clear: function () {
                    this.q = 0;
                    this.a = 0;
                }
            };
            container.find('.task-plan-row-' + tmIndex).each(function () {
                var p = strToFloatDefault($(this).data('price'), 0.0);
                var q = strToIntDefault($(this).find('.task-plan-data-quantity input').val(), 0);

                tmTotal.q += q;
                tmTotal.a += p * q;
            });

            var tmRow = container.find('.task-plan-tm-row-' + tmIndex);

            tmRow.find('.task-plan-data-price').text(tmTotal.p().toFixed(2));
            tmRow.find('.task-plan-data-amount').text(tmTotal.a.toFixed(2));
            tmRow.find('.task-plan-data-quantity').text(tmTotal.q);
        }
    }

    function onTaskPlanSave(sender) {
        var task = strToIntDefault($(sender).data('task'), 0);
        if (task) {
            taskPlanSave(task);
        }
    }

    function taskPlanSave(task) {
        var planList = [];

        var $container = $('#task-plans-container');
        $container.find('.task-plan-row').each(function () {
            var $tr = $(this);
            var plan = strToIntDefault($tr.data('plan'), 0);
            if (plan) {
                var tmIndex = strToIntDefault($tr.data('tmindex'), 0);
                var quantity = strToIntDefault($tr.find('.task-plan-data-quantity input').val(), 0);
                var categoryList = $container.find('#task-plan-data-category-select-' + tmIndex).selectpicker('val');
                if (!Array.isArray(categoryList)) {
                    if (categoryList) {
                        categoryList = [categoryList];
                    } else {
                        categoryList = [];
                    }
                }
                var data = {
                    plan: plan,
                    quantity: quantity,
                    category_list: categoryList
                };
                planList.push(data);
            }
        });

        var p = {
            task: task,
            plan_list: planList
        };
        taskAdd('taskPlanSave');
        $.saAPI.query('task_plan_update', p, onTaskPlanSaveSuccess, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('taskPlanSave');
        });
    }

    function onTaskPlanSaveSuccess(data) {
        taskRemove('taskPlanSave');
        alert($.lang.taskPlanSaved);
    }


    // Report Shop-Task

    function onReportShopTask() {
        $('.content-tab').hide();
        setCaption($.lang.reportShopTask);
        $('#report-shop-task-container').empty();
        _table = new apTable($, 'report-shop-task-container', true);
        _table.setColsModel(_colModel);
        var cols = [];
        cols.push('service_select');
        cols.push('title');
        cols.push('maket');
        cols.push('period_start');
        cols.push('period_end');
        cols.push('quantity');
        _table.init(cols);
        _table.setOnPageChange(function (event) {
            loadReportShopTask(event.data.page);
        });
        _table.setOnSort(function () {
            loadReportShopTask(_table.getPage());
        });
        _table.setOnFilter(function () {
            loadReportShopTask(1);
        });
        loadReportShopTaskPeriod();
        loadReportShopTaskShop();
        loadReportShopTask(1);
        loadReportShopTaskServiceSelect();
        $('#content-report-shop-task').show();
    }

    function loadReportShopTask(page) {
        var p = {};
        // page
        p.page = page;
        p.period = getReportShopTaskPeriod();
        p.shop = getReportShopTaskShop();
        // sort order
        var sort = _table.getSort();
        if (sort) {
            p.sort_col = sort.colIndex;
            p.sort_dir = (sort.isAsc) ? 'asc' : 'desc';
        } else {
            _table.setSort('client', true); // make it default
        }
        // filter
        var filter = _table.getFilter();
        $.extend(p, filter);
        // action
        taskAdd('loadReportShopTask');
        $.saAPI.query('report_shop_task', p, onLoadReportShopTask, function (xhr, status, error) {
            onError(xhr, status, error);
            bindPage();
            taskRemove('loadReportShopTask');
        });
    }

    function onLoadReportShopTask(data) {
        _table.setPagerData(data.rstart, data.rcount, data.count);
        _table.loadData(data.report_shop_task_list);
        bindPage();
        taskRemove('loadReportShopTask');
    }

    function loadReportShopTaskClientSelect() {
        taskAdd('loadReportShopTaskClientSelect');
        $.saAPI.query('period_client_list', {period: getReportShopTaskPeriod()}, onLoadReportShopTaskClientSelect, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('loadReportShopTaskClientSelect');
        });
    }

    function onLoadReportShopTaskClientSelect(data) {
        var r = new Array();
        for (var i in data.client_list) {
            r.push({
                id: data.client_list[i].client,
                title: data.client_list[i].title
            });
        }
        _table.setFilterSearchData('client_select', r);
        taskRemove('loadReportShopTaskClientSelect');
    }

    function loadReportShopTaskPeriod() {
        var ps = new Date();
        ps.setDate(1);
        var p = ps;
        var sel = $('#report-shop-task-period').empty();
        for (var i = -_report_period_count; i < _report_period_count; i++) {
            p = incPeriod(ps, i);
            $('<option value="' + periodSQL(p) + '">' + getMonthName(p) + '</option>').appendTo(sel);
        }
        setReportShopTaskPeriod(periodSQL(ps));
        // loadReportShopTaskClientSelect(); not need
    }

    function setReportShopTaskPeriod(value) {
        $('#report-shop-task-period').val(value);
    }

    function getReportShopTaskPeriod() {
        return $('#report-shop-task-period option:selected').val();
    }

    function loadReportShopTaskShop() {
        var sel = $('#report-shop-task-shop').empty();
        var c;
        _shops.start();
        while (_shops.next()) {
            c = _shops.current();
            $('<option value="' + c.shop + '">' + c.shop + '</option>').appendTo(sel);
        }
        setReportShopTaskShop(_shops.first().shop);
    }

    function setReportShopTaskShop(value) {
        $('#report-shop-task-shop').val(value);
    }

    function getReportShopTaskShop() {
        return $('#report-shop-task-shop option:selected').val();
    }

    function loadReportShopTaskServiceSelect() {
        var r = [];
        _services.start();
        var item;
        while (_services.next()) {
            item = _services.current();
            if (item.shop_access) {
                r.push({
                    id: item.service,
                    title: item.title
                });
            }
        }
        _table.setFilterSearchData('service_select', r);
    }

    // Reports common

    function loadReportPeriod(selector, periodDate, start, end) {
        periodDate.setDate(1);
        var p = periodDate;
        var sel = $(selector).empty();
        for (var i = start; i < end; i++) {
            p = incPeriod(periodDate, i);
            $('<option value="' + periodSQL(p) + '">' + getMonthName(p) + '</option>').appendTo(sel);
        }
        setReportPeriod(selector, periodSQL(periodDate));
    }

    function loadReportPeriodCount(selector) {
        var sel = $(selector).empty();
        for (var i = 1; i <= _period_count; i++) {
            $('<option value="' + i + '">' + i + '</option>').appendTo(sel);
        }
        $(selector).val(1);
    }

    function setReportPeriod(selector, value) {
        $(selector).val(value);
    }

    function getReportPeriod(selector) {
        return $(selector + ' option:selected').val();
    }

    function loadReportService(container, isChecked, onlyHaveShopList, withServiceType) {
        var sel = $(container).empty();
        var service;

        var st = {
            M: [],
            A: []
        };

        _services.start();
        while (_services.next()) {
            service = _services.current();
            if (service.state && (!onlyHaveShopList || service.have_shop_list)) {
                if (service.service_type === 'M' || service.service_type === 'C') {
                    st.M.push(service.service);
                }
                if (service.service_type === 'A' || service.service_type === 'C') {
                    st.A.push(service.service);
                }
            }
        }

        var i;
        var serviceId;
        if (st.M.length > 0) {
            if (withServiceType) {
                $('<input class="service-type-check" type="checkbox" value="M">' + $.lang.serviceType.M + '<br><br>').appendTo(sel);
            }
            for (i in st.M) {
                serviceId = st.M[i];
                service = _services.get(serviceId);
                $('<input class="service-check" type="checkbox" value="' + serviceId + '">' + service.title + '<br>').appendTo(sel);
            }
            $('<hr>').appendTo(sel);
        }

        if (st.A.length > 0) {
            if (withServiceType) {
                $('<input class="service-type-check" type="checkbox" value="A">' + $.lang.serviceType.A + '<br><br>').appendTo(sel);
            }
            for (i in st.A) {
                serviceId = st.A[i];
                service = _services.get(serviceId);
                $('<input class="service-check" type="checkbox" value="' + serviceId + '">' + service.title + '<br>').appendTo(sel);
            }
        }

        sel.find('input.service-type-check').click(function () {
            var slist = [];
            if (isServiceTypeCheck(container, 'M')) {
                slist = slist.concat(st.M);
            }
            if (isServiceTypeCheck(container, 'A')) {
                slist = slist.concat(st.A);
            }
            setReportServices(container, slist);
        });
        if (isChecked) {
            setReportServicesAll(container);
        } else {
            setReportServices(container, []);
        }
    }

    function isServiceTypeCheck(container, serviceType) {
        return $(container).find('input.service-type-check[value="' + serviceType + '"]:checked').length > 0;
    }

    function setReportServices(container, values) {
        $(container + ' input.service-check').removeAttr('checked').each(function () {
            this.checked = false;
        });
        for (var i in values) {
            $(container + ' input.service-check[value="' + values[i] + '"]').attr('checked', 'checked').each(function () {
                this.checked = true;
            });
        }
    }

    function setReportServicesAll(container) {
        $(container + ' input.service-check').attr('checked', 'checked').each(function () {
            this.checked = true;
        });
    }

    function getReportServices(container) {
        var res = new Array();
        $(container + ' input.service-check:checked').each(function () {
            res.push($(this).val());
        });
        return res;
    }

    function loadReportClient(selector) {
        var sel = $(selector).empty();
        $('<option value="0">' + $.lang.all + '</option>').appendTo(sel);

        var c;

        _clients.start();
        while (_clients.next()) {
            c = _clients.current();
            $('<option value="' + c.client + '">' + c.title + '</option>').appendTo(sel);
        }

        sel.val(0);
    }

    // Report dislocation

    function onReportDislocation() {
        $('.content-tab').hide();
        setCaption($.lang.reportDislocation);
        loadReportDislocationPeriod();
        loadReportDislocationService();
        $('#content-report-dislocation').show();
        bindPage();
    }

    function loadReportDislocationPeriod() {
        loadReportPeriod('#report-dislocation-period', new Date(), -_report_period_count, _report_period_count);
    }

    function setReportDislocationPeriod(value) {
        setReportPeriod('#report-dislocation-period', value);
    }

    function getReportDislocationPeriod() {
        return getReportPeriod('#report-dislocation-period');
    }

    function loadReportDislocationService() {
        loadReportService('#report-dislocation-service', false, true, false);
    }

    function setReportDislocationServices(values) {
        setReportServices('#report-dislocation-service', values);
    }

    function getReportDislocationServices() {
        return getReportServices('#report-dislocation-service');
    }

    function getReportDislocationFull() {
        return $('#report-dislocation-full:checked').length > 0;
    }

    function onReportDislocationGet() {

        var p = {};
        p.period = getReportDislocationPeriod();
        p.services = getReportDislocationServices().join(',');
        if (getReportDislocationFull()) {
            p.full = true;
        }

        if (p.services === '') {
            alert($.lang.errChooseServices);
            return;
        }

        taskAdd('reportDislocationGet');
        $.saAPI.query('report_dislocation_make', p, onReportDislocationSuccess, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('reportDislocationGet');
        });
    }

    function onReportDislocationSuccess(data) {
        if (data.doc) {
            document.location = $.saAPI.urlDoc(data.doc);
        }
        taskRemove('reportDislocationGet');
    }


    // Report charge

    function onReportCharge() {
        $('.content-tab').hide();
        setCaption($.lang.reportCharge);
        loadReportChargePeriod();
        loadReportChargeClient();
        loadReportChargeService();
        $('#content-report-charge').show();
        bindPage();
    }

    function loadReportChargePeriod() {
        loadReportPeriod('#report-charge-period-start', new Date(), -_report_period_count, _report_period_count);
        loadReportPeriod('#report-charge-period-end', new Date(), -_report_period_count, _report_period_count);
    }

    function setReportChargePeriod(valueStart, valueEnd) {
        setReportPeriod('#report-charge-period-start', valueStart);
        setReportPeriod('#report-charge-period-end', valueEnd);
    }

    function getReportChargePeriodStart() {
        return getReportPeriod('#report-charge-period-start');
    }

    function getReportChargePeriodEnd() {
        return getReportPeriod('#report-charge-period-end');
    }

    function loadReportChargeService() {
        loadReportService('#report-charge-service', true, false, true);
    }

    function loadReportChargeClient() {
        loadReportClient('#report-charge-client');
    }

    function setReportChargeServices(values) {
        setReportServices('#report-charge-service', values);
    }

    function getReportChargeServices() {
        return getReportServices('#report-charge-service');
    }

    function getReportChargeType() {
        return $('#report-charge-type option:selected').val();
    }

    function getReportChargeClient() {
        return $('#report-charge-client option:selected').val();
    }

    function onReportChargeGet() {

        var isM = isServiceTypeCheck('#report-charge-service', 'M');
        var isA = isServiceTypeCheck('#report-charge-service', 'A');

        var p = {};
        p.period_s = getReportChargePeriodStart();
        p.period_e = getReportChargePeriodEnd();
        p.client = getReportChargeClient();
        p.services = getReportChargeServices().join(',');
        if (!isM && isA) {
            p.service_type = 'A';
        } else if (isM && !isA) {
            p.service_type = 'M';
        }
        p.charge_type = getReportChargeType();
        if (p.services === '') {
            alert($.lang.errChooseServices);
            return;
        }
        if (p.period_s > p.period_e) {
            alert($.lang.errPeriodRange);
            return;
        }

        taskAdd('reportChargeGet');
        $.saAPI.query('report_charge_make', p, onReportChargeSuccess, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('reportChargeGet');
        });
    }

    function onReportChargeSuccess(data) {
        //console.log(data);
        if (data.doc) {
            document.location = $.saAPI.urlDoc(data.doc);
        }
        taskRemove('reportChargeGet');
    }


    // Report promo

    function onReportPromo() {
        $('.content-tab').hide();
        setCaption($.lang.reportPromo);
        loadReportPromoPeriod();
        loadReportPromoClient();
        $('#content-report-promo').show();
        bindPage();
    }

    function loadReportPromoPeriod() {
        loadReportPeriod('#report-promo-period', new Date(), -_report_period_count, _report_period_count);
    }

    function setReportPromoPeriod(value) {
        setReportPeriod('#report-promo-period', value);
    }

    function getReportPromoPeriod() {
        return getReportPeriod('#report-promo-period');
    }

    function loadReportPromoClient() {
        loadReportClient('#report-promo-client');
    }

    function getReportPromoClient() {
        return $('#report-promo-client option:selected').val();
    }

    function onReportPromoGet() {

        var p = {};
        p.period = getReportPromoPeriod();
        p.client = getReportPromoClient();

        taskAdd('reportPromoGet');
        $.saAPI.query('report_promo_make', p, onReportPromoSuccess, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('reportPromoGet');
        });
    }

    function onReportPromoSuccess(data) {
        //console.log(data);
        if (data.doc) {
            document.location = $.saAPI.urlDoc(data.doc);
        }
        taskRemove('reportPromoGet');
    }

    // Report prikassa

    function onReportPrikassa() {
        $('.content-tab').hide();
        setCaption($.lang.reportPrikassa);
        loadReportPrikassaPeriod();
        loadReportPrikassaClient();
        $('#content-report-prikassa').show();
        bindPage();
    }

    function loadReportPrikassaPeriod() {
        loadReportPeriod('#report-prikassa-period-s', new Date(), -_report_period_count, _report_period_count);
        loadReportPeriod('#report-prikassa-period-e', new Date(), -_report_period_count, _report_period_count);
    }

    function setReportPrikassaPeriod(from, to) {
        setReportPeriod('#report-prikassa-period-s', from);
        setReportPeriod('#report-prikassa-period-e', to);
    }

    function getReportPrikassaPeriodStart() {
        return getReportPeriod('#report-prikassa-period-s');
    }

    function getReportPrikassaPeriodEnd() {
        return getReportPeriod('#report-prikassa-period-e');
    }

    function loadReportPrikassaClient() {
        loadReportClient('#report-prikassa-client');
    }

    function getReportPrikassaClient() {
        return $('#report-prikassa-client option:selected').val();
    }

    function onReportPrikassaGet() {

        var p = {};
        p.period_s = getReportPrikassaPeriodStart();
        p.period_e = getReportPrikassaPeriodEnd();
        p.client = getReportPrikassaClient();

        if (p.period_s > p.period_e) {
            alert($.lang.msgPeriodStartMustBeLessThanPeriodEnd);
            return false;
        }

        taskAdd('reportPrikassaGet');
        $.saAPI.query('report_prikassa_make', p, onReportPrikassaSuccess, function (xhr, status, error) {
            onError(xhr, status, error);
            taskRemove('reportPrikassaGet');
        });
    }

    function onReportPrikassaSuccess(data) {
        if (data.doc) {
            document.location = $.saAPI.urlDoc(data.doc);
        }
        taskRemove('reportPrikassaGet');
    }

    // IFrame content

    function onIFrameContent(caption, pageId) {
        $('.content-tab').hide();
        setCaption(caption);
        var $content = $('#content-iframe');
        var $iframe = $content.find('iframe');
        $iframe.attr('src', '/v2/' + pageId + '?sid=' + $.saAPI.sid() + '&v=' + $.saAPI.version());
        $content.show();
    }

    // Bindings

    function bind() {
        // menu
        $('.menu').click(function () {
            $('.menu').removeClass('active');
            $(this).addClass('active');
        });
        $('.menu-service a').click(function () {
            var id = $(this).data('id');
            if (id) {
                onService(id);
            }
        });

        $('#menu-report-shop-task a').click(onReportShopTask);
        $('#menu-report-dislocation a').click(onReportDislocation);
        $('#menu-report-promo a').click(onReportPromo);
        $('#menu-report-prikassa a').click(onReportPrikassa);
        $('#menu-report-charge a').click(onReportCharge);
        $('#menu-users a').click(onUsers);
        $('#menu-config a').click(onConfig);
        $('#menu-shops a').click(onShops);
        $('#menu-clients a').click(onClients);
        $('#menu-categories a').click(onCategories);
        $('#menu-services a').click(onServices);
        $('#menu-budget a').click(function () {
            onIFrameContent($.lang.budget, 'budget');
        });
        $('#menu-budget-planning a').click(function () {
            onIFrameContent($.lang.planning, 'budget_planning');
        });
        $('#menu-exit a').click(onExit);
    }

    function unbind() {
        $('.menu').off('click');
        $('.menu a').off('click');
    }

    function rebind() {
        unbind();
        bind();
    }

    // Init

    function initStart() {
        pageLoading(true);
        taskAdd('init');
    }

    function initEnd() {
        taskRemove('init');
        pageLoading(false);
    }

    // loaders

    function pageLoading(value) {
        if (value) {
            $('#wrap').hide();
            $('#page-loading').show();
        } else {
            $('#page-loading').hide();
            $('#wrap').show();
        }
    }
})(jQuery);

// $.saAPI.query('client_stat', {'client': client, 'year': 2016}, function (data) {
// }, function (xhr, status, error) {
//     onError(xhr, status, error);
// });
