;
(function ($) {
    /**
     * ShopADV
     * WWW :: modal/task.js
     * $.saModalTask
     * (c) 2014 Alyoshin V.A. master@ap-soft.com
     * http://shopadv.biz
     *
     **/

    'use strict';

    var _callback = null;
    var _id = null; // task ID
    var _service = null; // current service ID
    var _servObj = {}; // service object
    var _clients = null; // apCache obj
    var _shops = null; // apCache obj
    var _services = null; // apCache obj
    var _categories = null; // apCache obj
    var _data = {};
    var _table = null;
    var _is_new_client = false;
    var _is_change_client = false;
    var _client_start = 0;
    var _doc_list = [];

    var _maket_upload = {
        url: 'localhost',
        params: {}
    };
    var _doc_upload = {
        url: 'localhost',
        params: {}
    };

    var _maket_w_max = 640;
    var _ps = null;
    var _pe = null;
    var _amount_timer = null;
    var _redirect = document.location.protocol + '//' + document.location.hostname + '/';
//    if(document.location.pathname) {
//        _redirect += document.location.pathname;
//    } else {
//        _redirect += '/';
//    }
    _redirect += 'result.html?%s';

    $.saModalTask = $.saModalTask || {};
    $.extend($.saModalTask, {
        clientsCacheSet: function (cache) {
            _clients = cache;
            reloadClients();
        },
        categoriesCacheSet: function (cache) {
            _categories = cache;
            reloadCategories();
        },
        shopsCacheSet: function (cache) {
            _shops = cache;
        },
        servicesCacheSet: function (cache) {
            _services = cache;
        },
        maketUploadConfig: function (url, params) {
            _maket_upload.url = url;
            _maket_upload.params = params;
        },
        docUploadConfig: function (url, params) {
            _doc_upload.url = url;
            _doc_upload.params = params;
        },
        setRedirect: function (value) {
            _redirect = value;
        },
        add: function (service, onAdd) {
            _id = null;
            clear();
            setService(service);
            _callback = onAdd;
            loadDefault();
            $('#modal-task').modal('show');
            enable();
        },
        edit: function (taskID, onEdit) {
            _id = taskID;
            _callback = onEdit;
            clear();
            if (load()) {
                $('#modal-task').modal('show');
            }
        },
        isNewClient: function () {
            return _is_new_client;
        },
        isChangeClient: function () {
            return _is_change_client;
        }
    });

    function setService(value) {
        _service = value;
        _servObj = _services.get(_service);
        updateViewByService();
    }

    function disable() {
        $('#modal-task-save').attr('disabled', 'disabled');
        $('#modal-task-shop-add').attr('disabled', 'disabled');
        $('#modal-task-busy').show();
    }

    function enable() {
        $('#modal-task-save').removeAttr('disabled');
        $('#modal-task-shop-add').removeAttr('disabled');
        $('#modal-task-busy').hide();
    }

    function clear() {
        _data = {};
        $('#modal-task input').val('');
        setClient(0);
        setCategory(null);
        setMaket('');
        setDocList('');
        setAlcohol(false);
        setServiceType('M');
        setColor('');
        $('#modal-task .form-group').removeClass('has-error');
        _table.clearData();
        _is_new_client = false;
        _is_change_client = false;
        _client_start = 0;
        $('#modal-task-price-block').hide();
        $('#modal-task-table').hide();
        $('#modal-task-maket-block').hide();
        $('#modal-task-doc-block').hide();
        $('#modal-task-shop-add-block').hide();
        $('#modal-task-alcohol-block').hide();
        $('#modal-task-service-type-block').hide();
        $('#modal-task-description-block').hide();
        $('#modal-task-payment-block').hide();
    }

    function updateViewByService() {
        if (_servObj.have_maket) {
            $('#modal-task-maket-block').show();
        } else {
            $('#modal-task-maket-block').hide();
        }
        if (_servObj.have_doc) {
            $('#modal-task-doc-block').show();
        } else {
            $('#modal-task-doc-block').hide();
        }
        if (_servObj.have_description) {
            $('#modal-task-description-block').show();
        } else {
            $('#modal-task-description-block').hide();
        }
        if (_servObj.have_payment) {
            $('#modal-task-payment-block').show();
            if (_servObj.have_fine) {
                $('#modal-task-fine-block').show();
            } else {
                $('#modal-task-fine-block').hide();
            }
        } else {
            $('#modal-task-payment-block').hide();
        }
        if (_servObj.have_shop_list) {
            $('#modal-task-table').show();
            $('#modal-task-shop-add-block').show();
        } else {
            $('#modal-task-table').hide();
            $('#modal-task-shop-add-block').hide();
        }
        if (_servObj.have_shop_list && !_servObj.have_days) {
            $('#modal-task-price-block').hide();
            $('#modal-task-shop-amount-set-block').show();
        } else {
            $('#modal-task-price-block').show();
            $('#modal-task-shop-amount-set-block').hide();
        }
        if (isQuantityByDays()) {
            $('#modal-task-quantity-block').hide();
        } else if (_servObj.have_payment) {
            $('#modal-task-quantity-block').show();
            $('#modal-task-agreement-block').show();
        } else if (_servObj.have_shop_list && !_servObj.have_days) {
            $('#modal-task-quantity-block').hide();
        } else {
            $('#modal-task-quantity-block').show();
            $('#modal-task-agreement-block').hide();
        }

        //if (isPeriodCountOne() || isQuarter()) {
        if (isPeriodCountOne()) {
            $('#modal-task-period-end-block').hide();
        } else {
            $('#modal-task-period-end-block').show();
        }
        if (_servObj.have_alcohol) {
            $('#modal-task-alcohol-block').show();
        } else {
            $('#modal-task-alcohol-block').hide();
        }
        if (_servObj.service_type === 'C') {
            $('#modal-task-service-type-block').show();
        } else {
            $('#modal-task-service-type-block').hide();
        }

//        $('#modal-task-period-start-dp').html(_init_date_start_html);
//        $('#modal-task-period-end-dp').html(_init_date_end_html);

        var opt = {
            language: "ru",
            autoclose: true,
            todayHighlight: true
        };
        var mode;
        if (isPeriod()) {
            opt['format'] = "MM yyyy";
            opt['minViewMode'] = 1;
            opt['startView'] = 1;
            mode = 0;
        } else {
            opt['format'] = "dd.mm.yyyy";
            opt['minViewMode'] = 0;
            opt['startView'] = 1;
            mode = -1;
        }
        if (!_ps) {
            _ps = $('#modal-task-period-start-dp').datepicker(opt).on('changeDate', function (ev) {
                if (ev.date.valueOf() > _pe.getDate().valueOf()) {
                    var newDate = new Date(ev.date);
                    _pe.setDate(newDate);
                }
                onPeriodChange();
            }).data('datepicker');
        } else {
            _ps._process_options(opt);
            _ps.update();
        }
        _ps.showMode(mode);
        if (!_pe) {
            _pe = $('#modal-task-period-end-dp').datepicker(opt).on('changeDate', function (ev) {
                if (ev.date.valueOf() < _ps.getDate().valueOf()) {
                    var newDate = new Date(ev.date);
                    _ps.setDate(newDate);
                }
                onPeriodChange();
            }).data('datepicker');
        } else {
            _pe._process_options(opt);
            _pe.update();
        }
        _pe.showMode(mode);

        var sd = new Date();
        var ed = new Date();

        if (isPeriod()) {
            sd.setDate(1);
            ed.setDate(1);
            var n = _servObj.min_period_count - 1;
            ed.setMonth(ed.getMonth() + n);
        }
        setPeriod(sd, ed);

        // table
        var tableCols = [];
        if (_servObj.have_shop_list) {
            tableCols.push('action_delete');
            tableCols.push('shop');
            tableCols.push('description');
            if (!isForceDays()) {
                tableCols.push('price');
                if (isDays()) {
                    tableCols.push('days');
                } else {
                    tableCols.push('quantity');
                }
            }
        } else if (_servObj.have_plan_list) {
//            tableCols.push('plan_title');
//            tableCols.push('plan_quantity');
//            tableCols.push('prod_code');
//            tableCols.push('prod_title');
//            tableCols.push('plan_date_entry');
        }
        if (tableCols.length) {
            _table.init(tableCols);
        }

        if (_servObj.have_plan_list) {
            $('#modal-task-price').attr('disabled', 'disabled');
            $('#modal-task-quantity').attr('disabled', 'disabled');
        } else {
            $('#modal-task-price').removeAttr('disabled');
            $('#modal-task-quantity').removeAttr('disabled');
        }
    }

    function isPeriod() {
        return _servObj.period_type === 'M' || _servObj.period_type === 'Q' || isPeriodCountOne();
    }

    function isDays() {
        return _servObj.period_type === 'D' || (_servObj.period_type === 'W' && !_servObj.have_shop_list);
    }

    function isForceDays() {
        return (_servObj.period_type === 'D' && _servObj.have_days) || (_servObj.period_type === 'W' && _servObj.have_shop_list);
    }

    function isQuantityByDays() {
        return (isDays() && (isForceDays() || !_servObj.have_shop_list)) ||
            (_servObj.period_type === 'W' && _servObj.have_shop_list);
    }

    function isPeriodCountOne() {
        return (isDays() && _servObj.have_shop_list && !isForceDays());
    }

    function isQuarter() {
        return _servObj.period_type === 'Q';
    }

    function isWeek() {
        return _servObj.period_type === 'W';
    }

    function loadDefault() {
        $('#modal-task-price').val(getDefaultPrice());
        if (isDays()) {
            $('#modal-task-quantity').val(_servObj.min_period_count);
        } else {
            $('#modal-task-quantity').val(1);
        }
    }

    function load() {
        if (!_id) {
            return false;
        }
        disable();
        $.saAPI.query('task_info', {task: _id}, onLoad, onLoadError);
        return true;
    }

    function onLoad(data) {
        if (data.task) {
            _data = data.task;

            setService(_data.service);
            _client_start = _data.client;
            _data.ts = data.ts;
            //console.log('load:', _data);
            setData();
            enable();
        } else {
            onLoadError();
        }
    }

    function onLoadError() {
        $('#modal-task').modal('hide');
    }

    // GUI -> _data
    function getData() {
        _data.service = _service;
        _data.client = getClient();
        _data.client_title = getClientTitle();
        _data.category_list = getCategory();
        _data.maket = getMaket();
        _data.doc_list = getDocList();
        _data.title = $('#modal-task-title').val();
        _data.description = $('#modal-task-description').val();
        _data.period_start = getPeriodStartSQL();
        _data.period_end = getPeriodEndSQL();
        _data.quantity = getQuantity();
        _data.price = getPrice();
        _data.payment = $('#modal-task-payment').val();
        _data.have_alcohol = getAlcohol();
        _data.service_type = getServiceType();
        _data.ts = getTaskShop();
    }

    // _data -> GUI
    function setData() {
        $('#modal-task-title').val(_data.title || '');
        $('#modal-task-description').val(_data.description || '');
        setPeriod(periodDate(_data.period_start), periodDate(_data.period_end));
        if (_data.client) {
            setClient(_data.client);
        }
        if (_data.category_list) {
            setCategory(_data.category_list);
        }
        if (_servObj.have_maket && _data.maket) {
            setMaket(_data.maket);
        }
        if (_servObj.have_doc && _data.doc_list) {
            setDocList(_data.doc_list);
        }
        if (_servObj.have_shop_list) {
            loadTaskShop(_data.ts);
        }
        $('#modal-task-quantity').val(_data.quantity);
        $('#modal-task-price').val(_data.price);
        if (_servObj.have_alcohol) {
            setAlcohol(_data.have_alcohol);
        }
        setServiceType(_data.service_type);
        if (_servObj.have_payment) {
            $('#modal-task-payment').val(_data.payment);
            updateFine();
        }
        setColor(_data.color);
    }

    function test() {
        getData();
        $('#modal-task .form-group').removeClass('has-error');
        if (!_data.title) {
            $('#modal-task-title').parent().addClass('has-error');
            $('#modal-task-title').focus();
            return false;
        }
        if (!_data.client && !_data.client_title) {
            $('#modal-task-client').parent().addClass('has-error');
            $('#modal-task-client').focus();
            return false;
        }
        if (!_data.period_start) {
            _ps.show();
            return false;
        }
        if (!_data.period_end || _data.period_start > _data.period_end) {
            _ps.show();
            return false;
        }
        if (isPeriod() && (getPeriodCount() < _servObj.min_period_count)) {
            var msg = strReplace($.lang.msgMinPeriodCount, '{1}', _servObj.min_period_count);
            window.alert(msg);
            _pe.show();
            return false;
        }
        if (isQuarter() && (getPeriodCount() % 3 !== 0)) {
            window.alert($.lang.msgPeriodCountMustBeQuarter);
            _pe.show();
            return false;
        }
        if (isWeek() && (getDaysCount() % 7 !== 0)) {
            window.alert($.lang.msgDaysCountMustBeWeek);
            _pe.show();
            return false;
        }

//        if(isPeriodCountOne() && (getPeriodCount() !== 1)) {
//            window.alert($.lang.msgPeriodCountMustBe1);
//            _pe.show();
//            return false;
//        }

        return true;
    }

    function process() {
        disable();
        var p = _data;
        _is_new_client = !_data.client;
        if (_id) {
            p.task = _id;
            _is_change_client = (_client_start !== _data.client);
            $.saAPI.query('task_update', p, onProcess, onProcessError);
        } else {
            _is_change_client = true;
            $.saAPI.query('task_add', p, onProcess, onProcessError);
        }
    }

    function onProcess(data) {
        $('#modal-task').modal('hide');
        _id = data.task;
        callback();
    }

    function onProcessError(xhr, s, err) {
        window.alert($.lang.errorAddEdit);
        console.log(err);
        enable();
    }

    function callback() {
        if ($.isFunction(_callback)) {
            _callback(_id);
        }
    }

    function getQuantity() {
        if (isQuantityByDays()) {
            var days = daysBetween(_ps.getDate(), _pe.getDate());
            if (isWeek()) {
                days = Math.round(days / 7);
            }
            $('#modal-task-quantity').val(days);
        }

        return strToIntDefault($('#modal-task-quantity').val(), 0);
    }

    function getPrice() {
        return strToFloatDefault($('#modal-task-price').val(), 0.0);
    }

    function setPeriod(dateStart, dateEnd) {
        _ps.setDate(dateStart);
        _pe.setDate(dateEnd);
    }

    function onPeriodChange() {
        if (isQuantityByDays()) {
            updateAmountStart();
        }
    }

    function getPeriodStartSQL() {
        var D = _ps.getDate();
        if (isPeriod()) {
            D.setDate(1); // first day of the month
        }
        return dateSQL(D);
    }

    function getPeriodEndSQL() {
        var D;
        if (isPeriodCountOne()) {
            D = _ps.getDate();
//        } else if (isQuarter()) {
//            D = _ps.getDate();
//            D = new Date(D.getFullYear(), D.getMonth() + 3, 0); // last day in the month after quarter
        } else {
            D = _pe.getDate();
        }
        if (isPeriod()) {
            D = new Date(D.getFullYear(), D.getMonth() + 1, 0); // last day in the month
        }
        return dateSQL(D);
    }

    function getPeriodCount() {
        var sd = _ps.getDate();
        var ed = _pe.getDate();
        return (ed.getFullYear() - sd.getFullYear()) * 12 + ed.getMonth() - sd.getMonth() + 1;
    }

    function getDaysCount() {
        var sd = _ps.getDate();
        var ed = _pe.getDate();

        return daysBetween(sd, ed);
    }

    function getClient() {
        //var v = $('#modal-task-client option:selected').val();
        var v = $('#modal-task-client').selectpicker('val');
        return strToIntDefault(v, 0);
    }

    function setClient(value) {
        $('#modal-task-client').val(value);
        $('#modal-task-client').selectpicker('val', value);
    }

    function getCategory() {
        var value = $('#modal-task-category').selectpicker('val');
        if (Array.isArray(value)) {
            return value;
        } else if (value) {
            return [value];
        }

        return [];
    }

    function setCategory(value) {
        var $select = $('#modal-task-category');
        if (!Array.isArray(value)) {
            $select.selectpicker('deselectAll');
        } else {
            $select.selectpicker('val', value);
            $select.selectpicker('refresh');
        }
    }

    function getAlcohol() {
        return $('#modal-task-alcohol:checked').length > 0;
    }

    function setAlcohol(value) {
        if (value) {
            $('#modal-task-alcohol').attr('checked', 'checked');
            $('#modal-task-alcohol').each(function () {
                this.checked = true;
            });
        } else {
            $('#modal-task-alcohol').removeAttr('checked');
            $('#modal-task-alcohol').each(function () {
                this.checked = false;
            });
        }
    }

    function getServiceType() {
        return ($('#modal-task-service-type:checked').length > 0) ? 'A' : 'M';
    }

    function setServiceType(value) {
        if (value === 'A') {
            $('#modal-task-service-type').attr('checked', 'checked');
            $('#modal-task-service-type').each(function () {
                this.checked = true;
            });
        } else {
            $('#modal-task-service-type').removeAttr('checked');
            $('#modal-task-service-type').each(function () {
                this.checked = false;
            });
        }
    }

    function setColor(value) {
        if (value) {
            $('#modal-task .task-color').css('background-color', '#' + value);
        } else {
            $('#modal-task .task-color').css('background-color', 'transparent');
        }
    }

    function getClientTitle() {
        var c = getClient();
        if (c && c > 0) {
            var cd = _clients.get(c);
            return cd ? cd.title : '';
        } else {
            return $('#modal-task-client-new').val();
        }
    }

    function getClientAgreementPrice() {
        var c = getClient();
        if (c && c > 0) {
            var cd = _clients.get(c);
            if (cd) {
                if (cd.service_prices[_service] && cd.service_prices[_service]['price']) {
                    return cd.service_prices[_service]['price'];
                }
            }
        }

        return null;
    }

    function getDefaultPrice() {
        var price = _servObj.price;
        var clientAgreementPrice = getClientAgreementPrice();

        return clientAgreementPrice ? clientAgreementPrice : price;
    }

    function loadTaskShop(arr) {
        var D = [];
        var item;
        for (var i in arr) {
            item = arr[i];
            item['s'] = i;
            D.push(arr[i]);
        }
        _table.loadData(D);
        updateAmountStart();

        $('.modal-task-table-quantity').keypress(function (e) {
            if (intKeypress(e)) {
                updateAmountStart();
                return true;
            }
            return false;
        });
        $('.modal-task-table-days').keypress(function (e) {
            if (intListKeypress(e)) {
                updateAmountStart();
                return true;
            }
            return false;
        });
        $('.modal-task-table-price').keypress(function (e) {
            if (eventFloatKeypress(e, this)) {
                updateAmountStart();
                return true;
            }
            return false;
        });
    }

    function getTaskShop() {
        if (!_servObj.have_shop_list) {
            return [];
        }
        var A = {};
        var shop = null;

        var defaultPrice = getPrice();
        var defaultQuantity = getQuantity();

        for (var i = 0; i < _table.rowCount(); i++) {
            shop = _table.getCellRaw('shop', i);
            if (shop) {
                A[shop] = {};
                A[shop]['desc'] = $('.modal-task-table-description[data-id="' + shop + '"]').val();
                if (isForceDays()) {
                    A[shop]['p'] = defaultPrice;
                    A[shop]['q'] = defaultQuantity;
                } else {
                    A[shop]['p'] = floatValidate($('.modal-task-table-price[data-id="' + shop + '"]').val());
                    if (isDays()) {
                        A[shop]['d'] = $('.modal-task-table-days[data-id="' + shop + '"]').val();
                    } else {
                        A[shop]['q'] = $('.modal-task-table-quantity[data-id="' + shop + '"]').val();
                    }
                }
            }
        }
        return A;
    }

    function getTaskShopAmount() {
        if (!_servObj.have_shop_list) {
            return 0;
        }

        if (isForceDays()) {
            return getQuantity() * getPrice() * _table.rowCount();
        }

        var amount = 0;
        var p = 0;
        var d = '';
        var q = 0;
        var isVisible = false;
        var shop = null;
        for (var i = 0; i < _table.rowCount(); i++) {
            shop = _table.getCellRaw('shop', i);
            isVisible = ($('#modal-task-table').find('tr[data-index="' + (i+1).toString() + '"]:visible').length > 0);
            if (shop && isVisible) {
                p = floatValidate($('.modal-task-table-price[data-id="' + shop + '"]').val());
                if (isDays()) {
                    d = $('.modal-task-table-days[data-id="' + shop + '"]').val();
                    if (d === '') {
                        q = 0;
                    } else {
                        q = d.split(',').length;
                    }
                } else {
                    q = $('.modal-task-table-quantity[data-id="' + shop + '"]').val();
                }
                amount += p * q;
            }
        }

        return amount;
    }

    function getMaket() {
        return $('#modal-task-maket').val();
    }

    function setMaket(value) {
        $('#modal-task-maket').val(value);
        updateMaket();
    }

    function getDocList() {
        return _doc_list.join(',');
    }

    function setDocList(value) {
        var cont = $('#modal-task-doc-list');
        cont.empty();
        _doc_list = [];

        if (!_servObj.have_doc || !value) {
            return;
        }

        var arr = value.split(',');
        var btn = null;
        for (var i in arr) {
            btn = $('<button data-id="' + arr[i] + '" type="button" class="btn btn-default modal-task-doc-open"><span class="glyphicon glyphicon-open-file"></span></button>');
            btn.click(onDocClick);
            cont.append(btn);
            _doc_list.push(arr[i]);
        }
    }

    function onDocClick() {
        var doc = $(this).data('id');
        if (doc) {
            document.location = $.saAPI.urlDoc(doc);
        }
    }

    function updateMaket() {
        $('#modal-task-maket-img').popover('hide');
        if (getMaket()) {
            $('#modal-task-maket-noimg').hide();
            $('#modal-task-maket-img').show();
            $('#modal-task-maket-save').removeAttr('disabled');
            $('#modal-task-maket-delete').removeAttr('disabled');
        } else {
            $('#modal-task-maket-img').hide();
            $('#modal-task-maket-noimg').show();
            $('#modal-task-maket-save').attr('disabled', 'disabled');
            $('#modal-task-maket-delete').attr('disabled', 'disabled');
        }
    }

    function onMaketError(err) {
        alert(err.code + ': ' + err.msg);
    }

    function updateFine() {
        var quantity = strToIntDefault($('#modal-task-quantity').val(), 0);
        var price = strToFloatDefault($('#modal-task-price').val(), 0.0);
        var payment = strToFloatDefault($('#modal-task-payment').val(), 0.0);

        var fine = payment - price * quantity;

        $('#modal-task-fine').val(fine.format(2));
    }

    function updateAgreement() {
        var clientAgreementPrice = getClientAgreementPrice();
        if (!clientAgreementPrice) {
            $('#modal-task-agreement-value').html('');
            return;
        }

        var quantity = strToIntDefault($('#modal-task-quantity').val(), 0);
        var amount = quantity * clientAgreementPrice;

        var str = $.lang.agreementPrice + ': ' + clientAgreementPrice.format(2) + '. ' + $.lang.agreementAmount + ': <b>' + amount.format(2) + '</b>';

        $('#modal-task-agreement-value').html(str);
    }

    $('#modal-task-fileupload-maket').fileupload({
        dataType: 'jsonp',
        forceIframeTransport: true,
        redirect: _redirect,
        done: function (e, data) {
            if (data.result) {
                if (data.result.maket) {
                    setMaket(data.result.maket);
                } else if (data.result.error) {
                    onMaketError(data.result.error);
                } else {
                    onMaketError({code: $.saErr.ERR_CLIENT, msg: ''});
                }
            } else {
                onMaketError({code: $.saErr.ERR_CLIENT, msg: ''});
            }
            enable();
        }
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#modal-task-fileupload-maket').bind('fileuploadsubmit', function (e, data) {
        disable();
        data.url = _maket_upload.url;
        data.formData = _maket_upload.params;
    });

    $('#modal-task-maket-delete').click(function () {
        setMaket('');
    });

    $('#modal-task-maket-img').popover({
        trigger: 'focus',
        html: true,
        placement: 'bottom',
        content: function () {
            return '<img src="' + $.saAPI.urlMaket(getMaket()) + '" alt="" style="max-width: ' + _maket_w_max + 'px">';
        }
    });

    function onDocError(err) {
        alert(err.code + ': ' + err.msg);
    }

    $('#modal-task-fileupload-doc').fileupload({
        dataType: 'jsonp',
        forceIframeTransport: true,
        redirect: _redirect,
        done: function (e, data) {
            if (data.result) {
                if (data.result.doc) {
                    _doc_list.push(data.result.doc);
                    setDocList(_doc_list.join(','));
                } else if (data.result.error) {
                    onDocError(data.result.error);
                } else {
                    onDocError({code: $.saErr.ERR_CLIENT, msg: ''});
                }
            } else {
                onDocError({code: $.saErr.ERR_CLIENT, msg: ''});
            }
            enable();
        }
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#modal-task-fileupload-doc').bind('fileuploadsubmit', function (e, data) {
        disable();
        data.url = _doc_upload.url;
        data.formData = _doc_upload.params;
    });

    $('#modal-task-doc-delete').click(function () {
        setDocList('');
    });

    $('#modal-task-save').click(function () {
        if (test()) {
            process();
        }
    });

    $('#modal-task-client').change(function () {
        var v = getClient();
        if (v && v > 0) {
            $('#modal-task-client-new').hide();
        } else {
            $('#modal-task-client-new').show().val('').focus();
        }
        $('#modal-task-price').val(getDefaultPrice());
        updateAgreement();
    });

    $('#modal-task-quantity').keypress(intKeypress);
    $('#modal-task-quantity').on('input', function () {
        updateAmountStart();
        updateFine();
        updateAgreement();
    });
    $('#modal-task-price').keypress(eventFloatKeypress);
    $('#modal-task-price').on('input', function () {
        updateAmountStart();
        updateFine();
    });
    $('#modal-task-payment').keypress(eventFloatKeypress);
    $('#modal-task-payment').on('input', function () {
        updateFine();
    });

    $('#modal-task-shop-add').click(function () {

        $.saModalShopList.execute([], function (data) {
            var S = data.numbers;
            var A = getTaskShop();
            var b = false;
            var sh = null;

            for (var i in S) {
                sh = _shops.get(S[i]);
                if (sh && !A[S[i]]) {
                    A[S[i]] = {};
                    if (!isForceDays()) {
                        if (data.prices[S[i]]) {
                            A[S[i]]['p'] = data.prices[S[i]];
                        } else {
                            if (sh.town && _servObj.town_price[sh.town]) {
                                A[S[i]]['p'] = _servObj.town_price[sh.town];
                            } else {
                                A[S[i]]['p'] = getDefaultPrice();
                            }
                        }
                        if (isDays()) {
                            A[S[i]]['d'] = data.days[S[i]].join(',');
                        } else {
                            A[S[i]]['q'] = 1;
                        }
                    }
                    b = true;
                }
            }
            if (b) {
                loadTaskShop(A);
            }
        });

    });

    function promtFloat(caption, value) {
        var str = window.prompt(caption, value);

        if (isNull(str)) {
            return null;
        }

        return strToFloat(floatValidate(str));
    }

    $('#modal-task-shop-amount-set').click(function () {
        var v = promtFloat($.lang.promtPrice, '');
        if (!isNull(v) && v >= 0) {
            $('.modal-task-table-price').val(v);
            updateAmountStart();
        }
    });
    $('#modal-task-shop-amount-plus').click(function () {
        var v = promtFloat($.lang.promtAmount, 0);
        if (!isNull(v) && v !== 0) {
            $('.modal-task-table-price').each(function () {
                var pv = $(this).val();
                pv = strToFloatDefault(pv, 0);
                pv = pv + v;
                if (pv >= 0) {
                    $(this).val(pv);
                }
            });
            updateAmountStart();
        }
    });
    $('#modal-task-shop-amount-mult').click(function () {
        var v = promtFloat($.lang.promtKoef, 1);
        if (!isNull(v) && v !== 1 && v >= 0) {
            $('.modal-task-table-price').each(function () {
                var pv = $(this).val();
                pv = strToFloatDefault(pv, 0);
                $(this).val(pv * v);
            });
            updateAmountStart();
        }
    });

    function reloadClients() {
        var $select = $('#modal-task-client');
        $select.empty();
        $('<option value="0">' + $.lang.addnewclient + '</option>').appendTo($select);
        _clients.start();
        var c = null;
        while (_clients.next()) {
            c = _clients.current();
            $('<option value="' + c.client + '">' + c.title + '</option>').appendTo($select);
        }
        $select.val(0);
        $select.selectpicker();
        $select.change();
    }

    function reloadCategories() {
        var $select = $('#modal-task-category');
        $select.empty();
        _categories.start();
        var c = null;
        var styler = '';
        while (_categories.next()) {
            c = _categories.current();
            styler = 'data-content="<span class=\'label label-'+c.style+'\'>'+c.title+'</span>"';
            $('<option value="' + c.category + '" ' + styler + '>' + c.title + '</option>').appendTo($select);
        }
        $select.selectpicker('deselectAll');
    }

    function updateAmountStart() {
        if (!_servObj.have_shop_list) {
            return;
        }
        updateAmountStop();
        $('#modal-task-shop-amount').val('...');
        _amount_timer = setTimeout(function () {
            updateAmount();
        }, 1000);
    }

    function updateAmountStop() {
        if (_amount_timer) {
            clearTimeout(_amount_timer);
            _amount_timer = null;
        }
    }

    function updateAmount() {
        $('#modal-task-shop-amount').val(getTaskShopAmount().toFixed(2));
    }

    $('#modal-task-alcohol').change(function () {
        if (!_servObj.have_alcohol || _servObj.have_shop_list) {
            return;
        }
        if (getAlcohol()) {
            $('#modal-task-price').val(_servObj.price_alcohol);
        } else {
            $('#modal-task-price').val(getDefaultPrice());
        }
    });

    var _colModel = {
        shop: {
            name: 'shop',
            label: $.lang.shop, // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 's', // index in data object
            search: null,
            formatter: null,
            width: 200 // column width in pixels
        },
        price: {
            name: 'price',
            label: $.lang.price, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'p', // index in data object
            search: null,
            formatter: function (value, row) {
                var shop = row.shop ? row.shop : row.s;
                if (value === null) {
                    value = '';
                }

                return '<input type="text" class="form-control modal-task-table-price" data-id="' + shop + '" value="' + value + '">';
            },
            width: 200 // column width in pixels
        },
        quantity: {
            name: 'quantity',
            label: $.lang.quantity, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'q', // index in data object
            search: null,
            formatter: function (value, row) {
                var shop = row.shop ? row.shop : row.s;
                if (value === null) {
                    value = '';
                }

                return '<input type="text" class="form-control modal-task-table-quantity" data-id="' + shop + '" value="' + value + '">';
            },
            width: 200 // column width in pixels
        },
        days: {
            name: 'days',
            label: $.lang.days, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'd', // index in data object
            search: null,
            formatter: function (value, row) {
                var shop = row.shop ? row.shop : row.s;
                if (value === null) {
                    value = '';
                }

                return '<input type="text" class="form-control modal-task-table-days" data-id="' + shop + '" value="' + value + '">';
            },
            width: 280 // column width in pixels
        },
        description: {
            name: 'description',
            label: $.lang.column.description, // can be function
            align: 'left', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: 'desc', // index in data object
            search: null,
            formatter: function (value, row) {
                var shop = row.shop ? row.shop : row.s;
                if (value === null) {
                    value = '';
                }

                return '<input type="text" class="form-control modal-task-table-description" data-id="' + shop + '" value="' + value + '">';
            },
            width: 150 // column width in pixels
        },
        action_delete: {
            name: 'action_delete',
            label: '', // can be function
            align: 'center', // left, right, center
            title: false, // can be: String or Function depends on cell value
            sortable: false, // true or false
            index: null, // index in data object
            search: null,
            formatter: function (value, row) {
                var s = '';
                s += '<button type="button" class="btn btn-danger modal-task-action-delete" title="' + $.lang.remove + '"><span class="glyphicon glyphicon-remove-sign"></span></button>';
                return s;
            },
            width: 50 // column width in pixels
        }
    };

    _table = new apTable($, 'modal-task-table', true);
    _table.setColsModel(_colModel);
    _table.setHeightValue(160);
    _table.hideFooter();

    $('#modal-task-table').on('click', '.modal-task-action-delete', function() {
        $(this).parentsUntil('tbody', 'tr').hide().find('td').html('');
        updateAmountStart();
    });

})(jQuery);
