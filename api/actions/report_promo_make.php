<?php

// Action: report_promo_make
// Input:
//    period (M)
//    client (O)
//    shop (O)
// Output:
//    doc: str[32]
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

require_once(API_DIR . 'classes/ExcelGeneratorLines.php');
require_once(API_DIR . 'classes/period.php');
require_once(API_DIR . 'lang/uk.php');

$period = $Input->getParam('period', true);
if (!$period) {
    throw new Exception('<period> is required', ERR_PARAM_MISSING);
}

$shop = $Input->getParam('shop', true);

$p = Period::decode($period);
$month = $p['m'];
$monthNameFr = $LANG['month_names_fr'][$month - 1];
$year = $p['y'];
$daysInMonth = Period::daysInMonth($month, $year);

$client = $Input->getParam('client');
$service = $DB->getServiceByCode(5); // ID of THE promo service

$template = 'promo';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

define('ROW_DAYS', 1);
define('ROW_WEEKDAYS', 2);
define('ROW_TOP', 3);
define('COL_SHOP', 0);
define('COL_LEFT', 1);

$EG = new ExcelGeneratorLines(TEMP_DIR);

try {

    $EG->open($templateFilename);
    $EG->setSheetName($EG->getSheetIndex(), Period::periodAsWin($period));

    // styles
    $EG->addStyle('text', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11
        ),
        'alignment' => array(
            'horizontal' => 'center'
        )
    ));
    $EG->addStyle('boldcenter', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11,
            'bold' => true
        ),
        'alignment' => array(
            'horizontal' => 'center'
        )
    ));

    // write days and weekdays

    $R = array();
    $W = array();
    for ($d = 1; $d <= $daysInMonth; $d++) {
        $weekday = Period::dayOfWeek($year, $month, $d);
        $R[] = array('value' => $d . ' ' . $monthNameFr, 'style' => 'boldcenter');
        $W[] = array('value' => $LANG['weekdays'][$weekday], 'style' => 'boldcenter');
    }

    $EG->writeRow(COL_LEFT, ROW_DAYS, $R);
    $EG->writeRow(COL_LEFT, ROW_WEEKDAYS, $W);

    // write data

    $taskList = $DB->taskShopListServicePeriod($period, $service, $client, $shop);
    if ($taskList) {

        $RS = array();
        foreach ($taskList as $task) {

            $days = explode(',', $task['days']);
            if (!$days || count($days) <= 0) {
                continue;
            }

            // style
            if ($task['color']) {
                $styleID = 'text_' . $task['color'];
                if (!$EG->getStyle($styleID)) {
                    $styleArray = array_merge($EG->getStyle('text'), array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array(
                                'rgb' => strtoupper($task['color'])
                            )
                        )
                    ));
                    $EG->addStyle($styleID, $styleArray);
                }
            } else {
                $styleID = 'text';
            }

            $R = array();

            $R[] = array('value' => $task['shop'], 'style' => 'boldcenter');
            for ($d = 1; $d <= $daysInMonth; $d++) {
                if (in_array($d, $days)) {
                    $R[] = array('value' => $task['title'], 'style' => $styleID);
                } else {
                    $R[] = '';
                }
            }

            $RS[] = $R;
        }

        $EG->writeRows(COL_SHOP, ROW_TOP, $RS);

        $rowCount = count($RS);
    } else {
        $rowCount = 0;
    }

    $lastCol = COL_LEFT + $daysInMonth - 1;
    $lastRow = ROW_TOP + $rowCount - 1;

    // make borders
    $EG->writeBorders(0, 1, $lastCol, $lastRow);

    $doc = md5(Utils::uniqid());
    $destFilename = DOC_DIR . $doc . '.xls';

    $EG->save($destFilename);
    $EG->close();

    $title = 'Промо_' . Period::periodAsWin($period) . '_' . date('Y_m_d') . '.xls';

    if (!$DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
        throw new Exception('Error creating document at DB');
    }
} catch (Exception $ex) {
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
