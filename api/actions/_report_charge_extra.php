<?php

// Action: _report_charge SUB template
// Input:
//    services 
//    period_s
//    period_e
//    client
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

function getPeriodArray($plist) {
  $res = array();

  if (!$plist) {
    return $res;
  }

  $res = explode(',', $plist);

  return $res;
}

function lastPeriod($plist) {
  if (!$plist) {
    return '';
  }
  $res = getPeriodArray($plist);

  return end($res);
}

$template = 'charge_extra';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
  throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

define('ROW_TOP', 7);

$EG = new ExcelGeneratorLines(TEMP_DIR);

try {

  $EG->open($templateFilename);
  $EG->setSheetName($EG->getSheetIndex(), Period::periodAsWin($periodStart) . '_' . Period::periodAsWin($periodEnd));

  // styles
  $EG->addStyle('bold', array(
    'font' => array(
      'name' => 'Tahoma',
      'size' => 9,
      'bold' => true
    )
  ));
  $EG->addStyle('quantity', array(
    'font' => array(
      'name' => 'Tahoma',
      'size' => 9,
      'bold' => false
    ),
    'numberformat' => array(
      'code' => '0'
    )
  ));
  $EG->addStyle('money', array(
    'font' => array(
      'name' => 'Tahoma',
      'size' => 9,
      'bold' => true
    ),
    'numberformat' => array(
      'code' => '0.00'
    )
  ));
  $EG->addStyle('int', array(
    'font' => array(
      'name' => 'Tahoma',
      'size' => 9,
      'bold' => false
    ),
    'numberformat' => array(
      'code' => '0'
    )
  ));

  $tax = $TAX_NDS[$DB->Country];
  $currencyTitle = $LANG['currency'][$DB->Country];

  $row = ROW_TOP;
  $lastCol = 0;
  foreach ($services as $service) {
    $serviceInfo = $DB->serviceInfo($service);
    if (!$serviceInfo) {
      continue;
    }
    if ($serviceInfo['service_type'] === 'C') {
      $st = $serviceType;
    } else {
      $st = false;
    }
    $tasks = $DB->taskClientListByServicePeriod($periodStart, $periodEnd, $service, $client, $st);

    if ($tasks) {
      $RS = array();
      $i = 1;
      $totalQuantity = 0;
      $totalAmount = 0;
      foreach ($tasks as $task) {
        $chargeCount = (int)$task['charge_count'];
        $price = '';
        $amount = (float)$task['amount'];
        $payment = $amount;
        $quantity = (int)$task['quantity'];
        if ($serviceInfo['have_payment']) {
          if ($task['payment']) {
            $payment = (float)$task['payment'];
          }
        } else {
          if ($task['price']) {
            $price = (float)$task['price'];
          }
        }

        $chargeQuantity = $chargeCount * $quantity;
        $chargePayment = $chargeCount * $payment;
        $sum = round($chargePayment / (1 + $tax), 2);

        $lastDay = '';
        $lastPeriod = lastPeriod($task['period_list']);
        if ($lastPeriod) {
          $lastDay = Period::dateAsWin(Period::endOfTheMonth($lastPeriod));
        }

        $R = array();

        $R[] = ''; // empty
        $R[] = ''; // upl
        $R[] = $task['client_code']; // client_code
        $R[] = array('value' => $task['client_inn'], 'style' => 'int'); // client_inn
        $R[] = $task['client_title']; // client_title
        $R[] = ''; // agreement
        $R[] = ''; // agreement date
        $R[] = ''; // TRX
        $R[] = $lastDay; // period end
        $R[] = $serviceInfo['title_buh']; // service_title_buh
        $R[] = array('value' => $sum, 'style' => 'money'); // sum
        $R[] = array('value' => $chargePayment, 'style' => 'money'); // charge payment
        $R[] = array('value' => $chargeQuantity, 'style' => 'quantity'); // quantity
        $R[] = $currencyTitle['hi']; // currency
        $R[] = $serviceInfo['service_point']; // service_point
        $R[] = ''; // desc
        $R[] = ''; // tax code
        $R[] = ''; // payment cond.
        $R[] = ''; // article
        $R[] = ''; // cfo
        $R[] = ''; // goods category
        $R[] = ''; // title for print
        $R[] = $lastDay; // period end
        $R[] = ''; // comment
        $R[] = ''; // 1
        $R[] = ''; // 2
        $R[] = ''; // message

        $lastCol = count($R) - 1;

        $RS[] = $R;
        // total
        $totalQuantity += $chargeQuantity;
        $totalAmount += $chargePayment;
        $i++;
      }
      // total row
//      $T = array();
//      $T[] = ''; // no
//      $T[] = array('value' => $serviceInfo['title'], 'style' => 'bold');
//      $T[] = '';
//      $T[] = '';
//      $T[] = '';
//      $T[] = '';
//      $T[] = ''; // price
//      $T[] = array('value' => $totalQuantity, 'style' => 'quantity');
//      $T[] = array('value' => $totalAmount, 'style' => 'money');
//      $T[] = ''; // comment
      // excel
//      $EG->writeRow(0, $row, $T);
      $EG->writeRows(0, $row + 0, $RS);
      $row += count($RS) + 0;
    }
  } // services
  // make borders
  $EG->writeBorders(0, ROW_TOP, $lastCol, $row - 1);

  $doc = md5(Utils::uniqid());
  $destFilename = DOC_DIR . $doc . '.xls';

  $EG->save($destFilename);
  $EG->close();

  $title = 'ОБ_XXX_' . date('Y_m_d') . '.xls';

  if (!$DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
    throw new Exception('Error creating document at DB');
  }

} catch (Exception $ex) {
  throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
