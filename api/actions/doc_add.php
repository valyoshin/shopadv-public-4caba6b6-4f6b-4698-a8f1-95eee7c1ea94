<?php

// Action: doc_add
// Input:
//    fdata - file data
// Output:
//    doc: str(32)
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

if (!isset($_FILES['fdata']) || !$_FILES['fdata']['tmp_name']) {
    throw new Exception('<fdata> is required', ERR_PARAM_MISSING);
}

$filename = $_FILES['fdata']['name'];
if (!$filename) {
    throw new Exception('<fdata> is required: filename', ERR_PARAM_MISSING);
}

$ext = pathinfo($filename, PATHINFO_EXTENSION);

$doc = md5(Utils::uniqid());

// UPLOAD FILE
$destFilename = DOC_DIR . $doc;
if ($ext) {
    $destFilename .= '.' . $ext;
}
if (!@move_uploaded_file($_FILES['fdata']['tmp_name'], $destFilename)) {
    throw new Exception('system error (move_uploaded_file)', ERR_SYSTEM);
}

if (!$DB->docAdd($doc, $ext, Utils::asFilename($filename))) {
    throw new Exception('Error creating document at DB');
}

$res['doc'] = $doc;