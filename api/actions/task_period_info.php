<?php

// Action: task_period_info
// Input:
//    task
//    period (not used)
// Output:
//    task_period: array 
//        shop_list
//        district_list
//        region_list
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$task = $Input->getParam('task', true);
if(!$task) {
    throw new Exception('<task> is required', ERR_PARAM_MISSING);
}

$res['task_period'] = $DB->taskPeriodInfo($task);
