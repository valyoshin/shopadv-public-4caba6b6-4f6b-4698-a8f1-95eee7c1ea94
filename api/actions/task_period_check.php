<?php

// Action: task_period_check
// Input:
//    task
//    period
//    check
// Output:
//    none
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$task = $Input->getParam('task', true);
if(!$task) {
    throw new Exception('<task> is required', ERR_PARAM_MISSING);
}

$period = $Input->getParam('period', true);
if(!$period) {
    throw new Exception('<period> is required', ERR_PARAM_MISSING);
}

$check = $Input->getParam('check');

if ($check) {
    $success = $DB->taskPeriodCheck($task, $period);
} else {
    $success = $DB->taskPeriodUncheck($task, $period);
}

if (!$success) {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}
