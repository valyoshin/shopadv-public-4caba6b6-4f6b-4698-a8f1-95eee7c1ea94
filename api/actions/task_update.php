<?php

// Action: task_update
// Input:
//    task
//    client
//    maket
//    title
//    description
//    period_start
//    period_end
//    quantity
//    price
//    payment
//    service_type : M,A
//    ts - task shop data
//    have_alcohol
//    category_list
//    doc_list
// Output:
//    task: int
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$task = $Input->getParam('task', true);
if(!$task) {
    throw new Exception('<task> is required', ERR_PARAM_MISSING);
}

$taskInfo = $DB->taskInfo($task);
if(!$taskInfo) {
    throw new Exception('<task> is required (2)', ERR_PARAM_MISSING);
}

$serviceInfo = $DB->serviceInfo($taskInfo['service']);
if(!$serviceInfo) {
    throw new Exception('System error', ERR_SYSTEM);
}

$data = $Input->getParamDataArr(array('client', 'maket', 'period_start', 'period_end', 'title', 'description', 'quantity', 'price', 'payment', 'have_alcohol', 'service_type'));

if ($serviceInfo['service_type'] != 'C') {
    $data['service_type'] = $serviceInfo['service_type'];
}
if (isset($data['payment']) && !$serviceInfo['have_payment']) {
    unset($data['payment']);
}

require_once('_task_shop.php');
$TS = $serviceInfo['have_shop_list'] ? $Input->getParam('ts', true) : null;
processTaskShop($Input, $TS, $data);

if($DB->taskUpdate($task, $data)) {
    $res['task'] = $task;
    if($TS) {
        $DB->taskShopClear($task);
        foreach($TS as $shop => $row) {
            $DB->taskShopAdd($task, $shop, $row['q'], $row['p'], $row['d'], $row['desc']);
        }
    }
    $docList = $Input->getParam('doc_list', true);
    $DB->taskDocClear($task);
    if ($docList) {
        foreach ($docList as $doc) {
            $DB->taskDocAdd($task, $doc);
        }
    }

    $categoryList = $Input->getParam('category_list', true);
    $DB->taskCategoryClear($task);
    if ($categoryList) {
        foreach ($categoryList as $category) {
            $DB->taskCategoryAdd($task, $category);
        }
    }
} else {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}
