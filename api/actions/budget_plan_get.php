<?php
// Action: budget_plan_get
// Input:
//    budget_plan: int (M) - i budget
// Output:
//    budget_plan: object
//      budget_plan: int
//      year: int
//      title: string
//      data: array of object
//          client: int
//          service: int
//          data: object
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$budgetPlanId = $Input->getParam('budget_plan', true);
if (!$budgetPlanId) {
    throw new Exception('<budget_plan> is required', ERR_PARAM_MISSING);
}

$budgetPlan = $DB->budgetPlan($budgetPlanId);
if (!$budgetPlan) {
    throw new Exception('budget plan is not found', ERR_OBJ_NOT_FOUND);
}

$res['budget_plan'] = array(
    'budget_plan' => (int) $budgetPlan['budget_plan'],
    'year' => (int) $budgetPlan['year'],
    'title' => $budgetPlan['title'],
    'data' => array()
);

$budgetPlanData = $DB->budgetPlanData($budgetPlanId);

if ($budgetPlanData) {
    foreach ($budgetPlanData as $budgetPlanDataRow) {
        $res['budget_plan']['data'][] = array(
            'client' => (int) $budgetPlanDataRow['client'],
            'service' => (int) $budgetPlanDataRow['service'],
            'data' => json_decode($budgetPlanDataRow['data'], true) ?: null
        );
    }
}