<?php

// Action: category_add
// Input:
//    category_group
//    title
// Output:
//    category: int
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$data = $Input->getParamValidArr(array('category_group', 'title'));

if (!isset($data['category_group']) || !$data['category_group']) {
    throw new Exception('<category_group> is required', ERR_PARAM_MISSING);
}

$category = $DB->categoryAdd($data);
if( $category ) {
    $res['category'] = $category;
} else {
    throw new Exception('Error while adding. Probably, value has already exist.', ERR_ADD);
}
