<?php
// Action: budget_plan_set
// Input:
//    budget_plan: int (O)
//    year: int (M)
//    title: string (M)
//    data: array of object (O)
//        client: int
//        service: int
//        data: object
// Output:
//    budget_plan: int
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$title = $Input->getParam('title', true);
if (!$title) {
    throw new Exception('<title> is required', ERR_PARAM_MISSING);
}

$budgetPlanId = $Input->getParam('budget_plan', true);
$data = $Input->getParam('data', true);
if (!is_array($data)) {
    $data = array();
}

$DB->startTransaction();
if ($budgetPlanId) {
    $budgetPlan = $DB->budgetPlan($budgetPlanId);
    if (!$budgetPlan) {
        throw new Exception('budget plan is not found', ERR_OBJ_NOT_FOUND);
    }

    $DB->budgetPlanUpdate($budgetPlanId, $title);
    $DB->budgetPlanDataClear($budgetPlanId);
} else {
    $budgetPlanId = $DB->budgetPlanAdd($year, $title);
    if (!$budgetPlanId) {
        $DB->rollback();
        throw new Exception('Can not add new budget plan', ERR_ADD);
    }
}

foreach ($data as $row) {
    $service = (int) $row['service'];
    $client = (int) $row['client'];
    $rawData = json_encode($row['data']);

    if (!$DB->budgetPlanDataAdd($budgetPlanId, $service, $client, $rawData)) {
        $DB->rollback();
        throw new Exception('Can not add new budget plan data row', ERR_ADD);
    }
}

$DB->commit();

$res['budget_plan'] = array(
    'budget_plan' => $budgetPlanId,
    'year' => $year,
    'title' => $title
);
