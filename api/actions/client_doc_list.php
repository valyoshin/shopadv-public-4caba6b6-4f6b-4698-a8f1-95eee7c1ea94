<?php
// Action: client_doc_list
// Input:
//    client: int (M) - client ID
// Output:
//    client_doc_list: array of object
//      client_doc: int
//      doc: string
//      title: string
//      ext: string
//      created: datetime
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$rows = $DB->clientDocList($clientId);
$res['client_doc_list'] = array();
if ($rows) {
    foreach ($rows as $row) {
        $row['client_doc'] = (int) $row['client_doc'];
        $res['client_doc_list'][] = $row;
    }
}