<?php

// Action: shop_update
// Input:
//    shop
//    town
//    address
//    district
//    date_open 
//    state
// Output:
//    shop: int
//    
 
defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$shop = $Input->getParam('shop', true);
if(!$shop) {
    throw new Exception('<shop> is required', ERR_PARAM_MISSING);
}
$data = $Input->getParamDataArr(array('town', 'address', 'district', 'date_open', 'state'));

if($DB->shopUpdate($shop, $data)) {
    $res['shop'] = $shop;
} else {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}

