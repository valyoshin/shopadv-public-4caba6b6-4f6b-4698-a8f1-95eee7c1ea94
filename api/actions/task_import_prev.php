<?php

// Action: task_import
// Input:
//    fdata - file data
//    service
//    period
//    
//    ONLY FOR prikassa!!!
// Output:
//    none
//    

defined('IN_SITE') or die();

define('PERIOD_MAX_COUNT', 1);
define('SKIP_CLIENT_TITLE', '=INDEX');

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

if (!isset($_FILES['fdata']) || !$_FILES['fdata']['tmp_name']) {
    throw new Exception('<fdata> is required', ERR_PARAM_MISSING);
}

$filename = $_FILES['fdata']['tmp_name'];
if (!$filename) {
    throw new Exception('<fdata> is required: filename', ERR_PARAM_MISSING);
}

$service = $Input->getParam('service', true);
if (!$service) {
    throw new Exception('<service> is required 1', ERR_PARAM_MISSING);
}

$period = $Input->getParam('period', true);
if (!$period) {
    throw new Exception('<period> is required 1', ERR_PARAM_MISSING);
}

$periodLast = Period::incMonth($period, PERIOD_MAX_COUNT - 1);
$pd = Period::decode($periodLast);
$periodLast = Period::encode($pd['y'], $pd['m'], Period::daysInMonth($pd['m'], $pd['y']));

$Loger->debug($periodLast);

$info = $DB->serviceInfo($service);
if (!$info || !$info['have_plan_list']) {
    throw new Exception('<service> is required 2', ERR_PARAM_MISSING);
}

if ($info['service_type'] == 'C') {
    $serviceType = 'M';
} else {
    $serviceType = $info['service_type'];
}

$match = array(
    'plan_title' => array(
        'map' => 'имя планограм',
        'index' => -1,
        'type' => 'string'
    ),
    'quantity' => array(
        'map' => 'колич',
        'index' => -1,
        'type' => 'int'
    ),
    'prod_code' => array(
        'map' => 'id',
        'index' => -1,
        'type' => 'int'
    ),
    'prod_title' => array(
        'map' => 'имя',
        'index' => -1,
        'type' => 'string'
    ),
    'prod_tm' => array(
        'map' => 'марка',
        'index' => -1,
        'type' => 'string'
    ),
    'client_title' => array(
        'map' => 'поставщ',
        'index' => -1,
        'type' => 'string'
    ),
    'prod_date' => array(
        'map' => 'дата ввода прод',
        'index' => -1,
        'type' => 'date'
    )
);

function convert($type, $value) {
    switch ($type) {
        case 'int':
            return (int) $value;
        case 'float':
            return (float) $value;
        case 'date':
            return PHPExcel_Style_NumberFormat::toFormattedString($value, PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
        default:
            return Utils::asStr((string) $value);
    }
}

function getMatchIndex($str) {
    global $match;

    foreach ($match as $key => $map) {
        if (strpos($str, $map['map']) !== false) {
            return $key;
        }
    }

    return false;
}

function isValid($dataRow) {
    global $Loger;
    if (!$dataRow['quantity']) {
        $Loger->debugStr('>>> empty quantity');
        return false;
    }

    if (!$dataRow['prod_tm']) {
        $Loger->debugStr('>>> prod tm');
        return false;
    }

    if (!$dataRow['client_title']) {
        $Loger->debugStr('>>> empty client');
        return false;
    }

    if (!$dataRow['prod_date']) {
        $Loger->debugStr('>>> empty prod date');
        return false;
    }

    return true;
}

require_once(API_DIR . 'classes/ExcelGenerator.php');

$Loger->timestamp('open gen');

$EG = new ExcelGenerator(TEMP_DIR);

try {
    $EG->open($filename);

    $Loger->timestamp('open done');

    // read header

    $rowStart = 1;
    $colCount = $EG->getLastColumn();
    $rowCount = $EG->getLastRow();

    $Loger->debugStr('Excel dim: ' . $colCount . ': ' . $rowCount);

    for ($col = 0; $col < $colCount; $col++) {
        $cell = $EG->getCellValue($col, $rowStart);
        $cell = mb_strtolower($cell);

        $index = getMatchIndex($cell);
        $Loger->debugStr($col . ': ' . $cell . ': ' . $index);
        if ($index) {
            $match[$index]['index'] = $col;
        }
    }

    $Loger->timestamp('match done');

    // read data

    $data = array();
    for ($row = $rowStart + 1; $row <= $rowCount; $row++) {
        $dataRow = array();

        foreach ($match as $key => $map) {
            $dataRow[$key] = '';
            if ($map['index'] >= 0) {
                $cellStr = $EG->getCellValue($map['index'], $row);
                if ($cellStr) {
                    $dataRow[$key] = convert($map['type'], $cellStr);
                }
            }
        }

        if (isValid($dataRow)) {
            $data[] = $dataRow;
        } else {
            $Loger->debugStr('Invalid data row: ' . print_r($dataRow, true));
        }
    }

    $Loger->timestamp('read done. count: ' . count($data));

    $EG->close();

    // processing

    $Loger->debugStr('processing...');

    $tasks = array();
    $prods = array();

    foreach ($data as $dataRow) {
        
        $clientTitle = $dataRow['client_title'];
        
        if (mb_strpos($clientTitle, SKIP_CLIENT_TITLE) !== false) {
            throw new Exception('Wrong client title', ERR_SYSTEM);
        }
        
        // client
        $client = $DB->clientByTitle($clientTitle);
        if (!$client) {
            $client = $DB->clientAdd(array(
                'title' => $dataRow['client_title']
            ));
        }
        if (!$client) {
            $Loger->debugStr('Client is not found and created: ' . $dataRow['client_title']);
            continue;
        }
        // prod
        if ($dataRow['prod_code']) {
            $prod = $DB->prodByCode($dataRow['prod_code']);
        } else {
            $prod = null;
        }
        $prodData = array(
            'code' => $dataRow['prod_code'],
            'title' => $dataRow['prod_title'],
            'tm' => $dataRow['prod_tm']
        );
        if (!$prod) {
            $prod = $DB->prodAdd($prodData);
        } else {
            $DB->prodUpdate($prod, $prodData);
        }
        if (!$prod) {
            $Loger->debugStr('Prod is not found and created: ' . print_r($prodData, true));
            continue;
        }

        $pd = Period::decode($dataRow['prod_date']);
        $de = Period::encode($pd['y'], $pd['m'], $pd['d']);
        if ($pd['d'] > 15) {
            $pd['m'] ++;
        }
        $ps = Period::encode($pd['y'], $pd['m'], 1);
        //$pe = Period::encode($pd['y'], $pd['m'], Period::daysInMonth($pd['m'], $pd['y']));

        if ($ps > $periodLast) {
            $Loger->debugStr('Period skip: ' . $ps);
            continue;
        }
        
        $ps = $period;
        $pe = $periodLast;
        
        $tm = $dataRow['prod_tm'];
        
        $key = $client . ':' . $tm . ':' . $ps . ':' . $pe;
        
        $q = (int) $dataRow['quantity'];
        $p = (float) $info['price'];
        $amount = $p * $q;

        $prods[$key][] = array(
            'task' => null,
            'prod' => $prod,
            'title' => $dataRow['plan_title'],
            'quantity' => $dataRow['quantity'],
            'price' => $p,
            'date_entry' => $de
        );

        if (!isset($tasks[$key])) {
            $tasks[$key] = array(
                'service' => $service,
                'client' => $client,
                'title' => $tm,
                'period_start' => $ps,
                'period_end' => $pe,
                'quantity' => $q,
                'price' => $p,
                'amount' => $amount,
                'service_type' => $serviceType
            );
        } else {
            $tasks[$key]['quantity'] += $q;
            $tasks[$key]['amount'] += $amount;
        }
    }

    // saving

    $Loger->debugStr('saving...');

    $taskList = array();
    foreach ($tasks as $key => $taskData) {
        $task = $DB->taskAdd($taskData);
        $taskList[] = $task;
        $color = getNewColor($task);
        $DB->taskUpdate($task, array('color' => $color));
        foreach ($prods[$key] as $prodData) {
            $prodData['task'] = $task;
            $DB->taskPlanAdd($prodData);
        }
    }
} catch (Exception $ex) {
    $msg = 'Error: ' . $ex->getMessage();
    $Loger->debug($msg);
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$Loger->debug($taskList);

$res['list'] = implode(',', $taskList);
