<?php

// Action: _report_charge_promo SUB template
// Input:
//    services 
//    period_s
//    period_e
//    client
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

$template = 'charge_promo';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

// COLS
//    'no'
//    'shop'
//    'shop town title'
//    'shop address'
//    'task shop day'
//    'task title'

define('ROW_TOP', 2);

$EG = new ExcelGeneratorLines(TEMP_DIR);

try {

    $EG->open($templateFilename);
    $EG->setSheetName($EG->getSheetIndex(), Period::periodAsWin($periodStart) . '_' . Period::periodAsWin($periodEnd));

    $periodCount = Period::between($periodStart, $periodEnd);

    if ($periodCount > PERIOD_COUNT) {
        $periodCount = PERIOD_COUNT;
    }

    $row = ROW_TOP;
    $lastCol = 0;

    $RS = array();
    $i = 1;

    foreach ($services as $service) {
        $serviceInfo = $DB->serviceInfo($service);
        if(!$serviceInfo) {
            continue;
        }
        // check promo service
        if ($serviceInfo['code'] != 5) {
            continue;
        }

        for ($pIndex = 0; $pIndex < $periodCount; $pIndex++) {
            $period = Period::incMonth($periodStart, $pIndex);
            $pd = Period::decode($period);

            $tasks = $DB->taskShopListPromoPeriod($period, $service, $client, null);
            if (!is_array($tasks)) {
                continue;
            }

            foreach ($tasks as $taskData) {
                $days = explode(',', $taskData['days']);

                foreach ($days as $day) {
                    $date = Period::encode($pd['y'], $pd['m'], $day);

                    $R = array();
                    $R[] = $i; // no
                    $R[] = $taskData['shop']; // shop
                    $R[] = $taskData['town_title']; // shop town title
                    $R[] = $taskData['shop_address']; // shop address
                    $R[] = Period::dateAsWin($date); // day of the task
                    $R[] = $taskData['title']; // task title

                    $lastCol = count($R) - 1;

                    $RS[] = $R;
                    $i++;
                } // days
            } // task rows
            // excel
            $EG->writeRows(0, $row, $RS);
            $row += count($RS) + 1;
        }

    } // services
    // make borders
    $EG->writeBorders(0, ROW_TOP, $lastCol, $row - 2);

    $doc = md5(Utils::uniqid());
    $destFilename = DOC_DIR . $doc . '.xls';

    $EG->save($destFilename);
    $EG->close();
    
    $title = 'ПРОМО_ПОЛНЫЙ_XXX_' . date('Y_m_d') . '.xls';
    
    if (! $DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
        throw new Exception('Error creating document at DB');
    }
    
} catch (Exception $ex) {
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
