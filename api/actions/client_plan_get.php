<?php
// Action: client_plan_get
// Input:
//    client: int (M)
//    year: int (M)
// Output:
//    client_plan: object
//      client: int
//      year: int
//      turnover: float|null
//      turnoverRate: float|null
//      amount: float|null

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$clientData = $DB->clientInfo($clientId);
if (!$clientData) {
    throw new Exception('Client is not found.', ERR_OBJ_NOT_FOUND);
}

$clientPlanData = $DB->clientPlan($clientId, $year);

$res['client_plan'] = array(
    'client' => $clientId,
    'year' => $year,
    'turnover' => isset($clientPlanData['turnover']) && $clientPlanData['turnover'] ? (float) $clientPlanData['turnover'] : null,
    'turnoverRate' => isset($clientPlanData['turnoverRate']) && $clientPlanData['turnoverRate'] ? (float) $clientPlanData['turnoverRate'] : null,
    'amount' => isset($clientPlanData['amount']) && $clientPlanData['amount'] ? (float) $clientPlanData['amount'] : null
);