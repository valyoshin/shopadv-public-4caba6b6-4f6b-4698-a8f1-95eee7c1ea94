<?php

// Action: user_delete
// Input:
//    login
// Output:
//    none
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$login = $Input->getParam('login', true);
if(!$login) {
    throw new Exception('<login> is required', ERR_PARAM_MISSING);
}
if($login == $_SESSION[$sid][$ip]['user']['login']) {
    throw new Exception('<login> is the same as one to delete', ERR_USER_DENY);
}
if($login == 'SHOP' || $login == 'ADMIN') {
    throw new Exception('operation was denied by server', ERR_USER_DENY);
}

if(!$DB->userDelete($login)) {
    throw new Exception('Error while deleting. Probably, some record could not to be deleted.', ERR_DELETE);
}
