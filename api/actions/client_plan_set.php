<?php

// Action: client_plan_set
// Input:
//      client: int
//      year: int
//      turnover: float|null
//      turnoverRate: float|null
//      amount: float|null
// Output:
//    client_plan: object
//      client: int
//      year: int
//      turnover: float|null
//      turnoverRate: float|null
//      amount: float|null
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$clientData = $DB->clientInfo($clientId);
if (!$clientData) {
    throw new Exception('Client is not found.', ERR_OBJ_NOT_FOUND);
}

$data = $Input->getParamDataArr(array('turnover', 'turnoverRate', 'amount'));

$data['client'] = $clientId;
$data['year'] = $year;

if (!$DB->clientPlanSet($data)) {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}

$res['client_plan'] = $data;