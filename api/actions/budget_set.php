<?php

// Action: budget_set
// Input:
//    year (M) - year of the budget
//    data (M) - array
// Output:
//    budget: array
//      year: int
//      data: mixed
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$data = $Input->getParam('data', true);
if (!$data) {
    throw new Exception('<data> is required', ERR_PARAM_MISSING);
}

$dataEncoded = json_encode($data);

if (!$DB->budgetSet($year, $dataEncoded)) {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}

$res['budget'] = array(
    'year' => $year,
    'data' => $data,
);
