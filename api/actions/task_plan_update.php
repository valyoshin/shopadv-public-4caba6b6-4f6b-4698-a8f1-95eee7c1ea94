<?php

// Action: task_plan_update
// Input:
//    task
//    plan_list:
//        plan
//        quantity
// Output:
//    task
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$task = $Input->getParam('task', true);
if(!$task) {
    throw new Exception('<task> is required', ERR_PARAM_MISSING);
}

$taskInfo = $DB->taskInfo($task);
if(!$taskInfo) {
    throw new Exception('<task> is required (2)', ERR_PARAM_MISSING);
}

$serviceInfo = $DB->serviceInfo($taskInfo['service']);
if(!$serviceInfo) {
    throw new Exception('System error', ERR_SYSTEM);
}

if (!$serviceInfo['have_plan_list']) {
    throw new Exception('System error', ERR_SYSTEM);
}

$planList = $Input->getParam('plan_list');

foreach ($planList as $planData) {
    $plan = $Input->getParamCustom('plan', $planData['plan'], true);
    if (!$plan) {
        continue;
    }
    $data['quantity'] = $Input->getParamCustom('quantity', $planData['quantity'], true);
    $DB->taskPlanUpdate($task, $plan, $data);
    if (array_key_exists('category_list', $planData)) {
        $categoryList = $Input->getParamCustom('category_list', $planData['category_list'], true);
        $DB->taskPlanCategoryClear($plan);
        if ($categoryList) {
            foreach ($categoryList as $category) {
                $DB->taskPlanCategoryAdd($plan, $category);
            }
        }
    }
}

$DB->taskUpdateByPlan($task);

$res['task'] = $task;
