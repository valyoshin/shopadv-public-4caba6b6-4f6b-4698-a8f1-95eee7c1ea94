<?php

// Action: client_dialog_update
// Input:
//    client_dialog: int (M)
//    active: bool|int
//    description: string
// Output:
//      client_dialog: int
//      client: int
//      active: bool
//      description: string
//      created: datetime
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientDialogId = $Input->getParam('client_dialog', true);
if (!$clientDialogId) {
    throw new Exception('<client_dialog> is required', ERR_PARAM_MISSING);
}

$clientDialogData = $DB->clientDialog($clientDialogId);
if (!$clientDialogData) {
    throw new Exception('Client dialog is not found', ERR_OBJ_NOT_FOUND);
}

$data = $Input->getParamDataArr(array('active', 'description'));

if ($DB->clientDialogUpdate($clientDialogId, $data)) {
    $res['client_dialog'] = array(
        'client_dialog' => $clientDialogId,
        'client' => (int) $clientDialogData['client'],
    );
} else {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}