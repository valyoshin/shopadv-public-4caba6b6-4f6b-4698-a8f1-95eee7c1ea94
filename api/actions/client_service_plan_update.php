<?php

// Action: client_service_plan_update
// Input:
//    client_service_plan: int (M)
//    active: bool|int
//    description: string
//    quantity: int|null
//    period_count: int|null
//    price: float|null
//    amount: float
// Output:
//    client_service_plan: int
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientServicePlanId = $Input->getParam('client_service_plan', true);
if (!$clientServicePlanId) {
    throw new Exception('<client_service_plan> is required', ERR_PARAM_MISSING);
}

$clientServicePlanData = $DB->clientServicePlan($clientServicePlanId);
if (!$clientServicePlanData) {
    throw new Exception('Client service plan is not found', ERR_OBJ_NOT_FOUND);
}

$data = $Input->getParamDataArr(array('active', 'description', 'quantity', 'period_count', 'price', 'amount', 'removed'));

if ($DB->clientServicePlanUpdate($clientServicePlanId, $data)) {
    $res['client_service_plan'] = $clientServicePlanId;
} else {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}