<?php

// Action: shop_info
// Input:
//    shop
// Output:
//    shop: array 
//        shop
//        town
//        district
//        address
//        date_open
//        state
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$shop = $Input->getParam('shop', true);
if(!$shop) {
    throw new Exception('<shop> is required', ERR_PARAM_MISSING);
}

$res['shop'] = $DB->shopInfo($shop);
if ($res['shop']) {
    $res['shop']['state'] = (bool) $res['shop']['state'];
}

        