<?php

// Action: doc_add
// Input:
//    template - str - template id
//    task - int - task id
//    period - str
// Output:
//    doc: str[32]
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

require_once(API_DIR . 'classes/ExcelGenerator.php');
require_once(API_DIR . 'classes/money.php');
require_once(API_DIR . 'lang/uk.php');

$task = $Input->getParam('task', true);
if (!$task) {
    throw new Exception('<task> is required', ERR_PARAM_MISSING);
}

$period = $Input->getParam('period', true);
if (!$period) {
    throw new Exception('<period> is required', ERR_PARAM_MISSING);
}

$taskData = $DB->taskFullInfo($task);
if (!$taskData) {
    throw new Exception('<task> is wrong', ERR_PARAM_MISSING);
}

$country = $DB->Country;



$template = $Input->getParam('template', true);
$templateFilename = TEMPLATE_DIR . $template . '_' . $country . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

$EG = new ExcelGenerator(TEMP_DIR);

$p = Period::decode($period);
$p['last'] = Period::daysInMonth($p['m'], $p['y']);
$ps = Period::decode($taskData['period_start']);
$pe = Period::decode($taskData['period_end']);

switch ($taskData['period_type']) {
    case 'M':
        $ss = $p;
        $se = $p;
        $se['d'] = $p['last'];
        break;
    default:
        $ss = $ps;
        $se = $pe;
}

$taxNds = $TAX_NDS[$DB->Country];

$payment = $taskData['payment'] ? (float)$taskData['payment'] : (float)$taskData['amount'];
$nds = round(100 * $taxNds * $payment / (1 + $taxNds)) / 100;
$sum = $payment - $nds;

$hi = $LANG['currency'][$DB->Country]['hi'];
$lo = $LANG['currency'][$DB->Country]['lo'];
$rod = $LANG['currency'][$DB->Country]['rod'];

$EG->Data = array_merge($taskData, array(
    'period_day' => $p['d'],
    'period_month' => $p['m'],
    'period_month_name' => $LANG['month_names'][$p['m'] - 1],
    'period_month_name_fr' => $LANG['month_names_fr'][$p['m'] - 1],
    'period_month_name_ir' => $LANG['month_names_ir'][$p['m'] - 1],
    'period_year' => $p['y'],
    'period_last_day' => Period::daysInMonth($p['m'], $p['y']),

    'period_start_day' => $ps['d'],
    'period_start_month' => $ps['m'],
    'period_start_month_name' => $LANG['month_names'][$ps['m'] - 1],
    'period_start_month_name_fr' => $LANG['month_names_fr'][$ps['m'] - 1],
    'period_start_month_name_ir' => $LANG['month_names_ir'][$ps['m'] - 1],
    'period_start_year' => $ps['y'],

    'period_end_day' => $pe['d'],
    'period_end_month' => $pe['m'],
    'period_end_month_name' => $LANG['month_names'][$pe['m'] - 1],
    'period_end_month_name_fr' => $LANG['month_names_fr'][$pe['m'] - 1],
    'period_end_month_name_ir' => $LANG['month_names_ir'][$pe['m'] - 1],
    'period_end_year' => $pe['y'],

    'service_start_day' => $ss['d'],
    'service_start_month' => $ss['m'],
    'service_start_month_name' => $LANG['month_names'][$ss['m'] - 1],
    'service_start_month_name_fr' => $LANG['month_names_fr'][$ss['m'] - 1],
    'service_start_month_name_ir' => $LANG['month_names_ir'][$ss['m'] - 1],
    'service_start_year' => $ss['y'],

    'service_end_day' => $se['d'],
    'service_end_month' => $se['m'],
    'service_end_month_name' => $LANG['month_names'][$se['m'] - 1],
    'service_end_month_name_fr' => $LANG['month_names_fr'][$se['m'] - 1],
    'service_end_month_name_ir' => $LANG['month_names_ir'][$se['m'] - 1],
    'service_end_year' => $se['y'],

    'amount' => $payment,
    'nds' => $nds,
    'sum' => $sum,

    'tax_nds' => $taxNds,

    'amount_str' => money2str($payment, ' ' . $hi . ' ', ' ' . $lo, $LANG, $rod),
    'sum_str' => money2str($sum, ' ' . $hi . ' ', ' ' . $lo, $LANG, $rod),
    'nds_str' => money2str($nds, ' ' . $hi . ' ', ' ' . $lo, $LANG, $rod),

    'currency_hi' => $hi,
    'currency_lo' => $lo,

    'country' => $country
));

try {
    $EG->open($templateFilename);
    $EG->apply();

    $doc = md5(Utils::uniqid());
    $destFilename = DOC_DIR . $doc . '.xls';

    $EG->save($destFilename);
    $EG->close();

    $title = 'П_' . $taskData['client_title'] . '_' . $taskData['title'] . '_' . $p['m'] . '_' . $p['y'] . '_' . date('Y_m_d') . '.xls';

    if (!$DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
        throw new Exception('Error creating document at DB');
    }

} catch (Exception $ex) {
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
