<?php

// Action: clients_service_prices_cache
// Input:
//    service: int (M)
// Output:
//    clients_service_prices_list: array
//        client: int
//        service: int
//        price: float|null
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$service = $Input->getParam('service', true);
if (!$service) {
    throw new Exception('<service> is required', ERR_PARAM_MISSING);
}

$data = $DB->clientServicePlanList(null, $service, true);

$clientPrices = array();
if ($data) {
    foreach ($data as $row) {
        $client = (int)$row['client'];
        $price = is_numeric($row['price']) ? (float)$row['price'] : null;
        if ($price && !isset($clientPrices[$client])) {
            $clientPrices[$client] = $price;
        }
    }
}

$res['clients_service_prices_list'] = array();
foreach ($clientPrices as $client => $price) {
    $res['clients_service_prices_list'][] = array(
        'client' => $client,
        'service' => $service,
        'price' => $price
    );
}