<?php

// Action: task_delete
// Input:
//    task_list
// Output:
//    none
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
  throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$taskList = $Input->getParam('task_list', true);
if (!$taskList) {
  throw new Exception('<task_list> is required', ERR_PARAM_MISSING);
}

if (!$DB->taskDelete($taskList)) {
  throw new Exception('Error while deleting. Probably, some record could not to be deleted.', ERR_DELETE);
}
