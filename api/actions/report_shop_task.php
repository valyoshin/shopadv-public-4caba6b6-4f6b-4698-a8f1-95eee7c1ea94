<?php

// Action: report_shop_task
// Return task list for shop report 
// Input:
//    period - str format YYYY-MM (start period)
//    page - page to view
//    count - max count of rows to return
//    sort_col - column index to sort
//    sort_dir - ASC | DESC
//    shop - shop id (required)
//    --- filter:
//    service
//    client
//    code
//    title
// Output:
//    rstart
//    rcount
//    count
//    report_shop_task_list: array (sorted by service, client_title, title)
//        service
//        service_title
//        client
//        client_title
//        code
//        task
//        maket
//        title
//        period_start
//        period_end
//        quantity
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$shop = $Input->getParam('shop', true);
if(!$shop) {
    throw new Exception('<shop> is required', ERR_PARAM_MISSING);
}

$period = $Input->getParam('period', true);
if (!$period) {
    $period = $_SESSION[$sid][$ip]['config']['period_start'] . '-01';
}

$filter = $Input->getParamDataArr(array('client', 'code', 'title', 'service'));
$page = $Input->getParam('page');
$count = $Input->getParam('count');
if(!$count || $count > MAX_ROWS) {
    $count = MAX_ROWS;
}
$start = ($page - 1) * $count;

if (defined('MAX_ROWS_SKIP')) {
    $start = false;
    $count = false;
}

$sort_col = $Input->getParam('sort_col');
if(!$sort_col) {
    $sort_col = 'service';
}
$sort_dir = $Input->getParam('sort_dir');

$res['report_shop_task_list'] = $DB->reportShopTaskList($shop, $period, $filter, $start, $count, $sort_col, $sort_dir);
if(!$res['report_shop_task_list']) {
    $res['report_shop_task_list'] = array();
    $res['count'] = 0;
} else {
    $res['count'] = $DB->foundRows();
}    
   
$res['rstart'] = $start;
$res['rcount'] = $count;

