<?php

// Action: categories_cache
// Input:
//    none
// Output:
//    category_list: array (sorted by title)
//        category
//        category_group
//        title
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$res['category_list'] = $DB->categoryListCache();

if(!$res['category_list']) {
    $res['category_list'] = array();
}
