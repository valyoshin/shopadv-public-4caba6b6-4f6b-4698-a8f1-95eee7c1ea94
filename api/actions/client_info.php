<?php

// Action: client_info
// Input:
//    client
// Output:
//    client: array 
//        client: int - client id
//        code: string - client's code
//        title: string - client title
//        state: bool
//        jur_name: string
//        email: string
//        phone: string
//        name: string
//        address: string
//        inn: string
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
  throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
  throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$data = $DB->clientInfo($clientId);
if (!$data) {
    throw new Exception('Client is not found', ERR_OBJ_NOT_FOUND);
}

$data['client'] = (int) $data['client'];
$data['state'] = (bool) $data['state'];
$data['use_in_list'] = (bool) $data['use_in_list'];

$res['client'] = $data;
