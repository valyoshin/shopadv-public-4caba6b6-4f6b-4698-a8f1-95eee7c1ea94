<?php
// Action: client_service_plan_add
// Input:
//    client: int (M)
//    service: int (M)
//    active: bool
//    description: string
//    period_count: int|null
//    quantity: int|null
//    price: float|null
//    amount: float
// Output:
//    client_service_plan: int
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$serviceId = $Input->getParam('service', true);
if (!$serviceId) {
    throw new Exception('<service> is required', ERR_PARAM_MISSING);
}

$data = $Input->getParamValidArr(array('active', 'description', 'quantity', 'period_count', 'price', 'amount'));
$data['client'] = $clientId;
$data['service'] = $serviceId;
$data['created'] = date('Y-m-d H:i:s');

$clientServicePlanId = $DB->clientServicePlanAdd($data);

if ($clientServicePlanId) {
    $res['client_service_plan'] = $clientServicePlanId;
} else {
    throw new Exception('Error while adding.', ERR_ADD);
}
