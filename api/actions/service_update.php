<?php

// Action: service_update
// Input:
//    service
//    title
//    description
//    title_buh
//    service_point
//    price
//    price_alcohol
//    state
//    st
// Output:
//    service: int
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$service = $Input->getParam('service', true);
if(!$service) {
    throw new Exception('<service> is required', ERR_PARAM_MISSING);
}

$data = $Input->getParamDataArr(array('title', 'description', 'title_buh', 'service_point', 'price', 'price_alcohol', 'state'));

require_once('_service_town.php');
$ST = $Input->getParam('st', true);
processServiceTown($Input, $ST);

if($DB->serviceUpdate($service, $data)) {
    $res['service'] = $service;
    if($ST) {
        $DB->serviceTownClear($service);
        foreach($ST as $town => $row) {
            $DB->serviceTownAdd($service, $town, $row['p']);
        }
    }
} else {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}

