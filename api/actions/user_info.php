<?php

// Action: user_info
// Input:
//    login
// Output:
//    client: array 
//        login
//        name
//        email
//        role
//        state
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$login = $Input->getParam('login', true);
if(!$login) {
    throw new Exception('<login> is required', ERR_PARAM_MISSING);
}

$res['user'] = $DB->userInfo($login);
if ($res['user']) {
    $res['user']['state'] = (bool) $res['user']['state'];
}
        