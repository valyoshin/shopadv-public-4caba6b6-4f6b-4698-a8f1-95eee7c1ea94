<?php

// Action: client_stat
// Input:
//     client: int
//     year: int
//     service_type: string (O)
// Output:
//     client_stat: object
//         month_stat: array of
//             month: int
//             total_task_amount: float
//

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$serviceType = $Input->getParam('service_type', true);

$periodStart = $year . '-01-01';
$data = $DB->serviceClientList($periodStart, 12, null, $clientId, $serviceType);

if (count($data) !== 1) {
    throw new Exception('Client is not found.', ERR_OBJ_NOT_FOUND);
}

$monthStat = array();
$data = $data[0]; // Only one client.
for ($month = 1; $month <= 12; $month++) {
    $dataIndex = $month - 1;
    $monthStat[] = array(
        'month' => $month,
        'total_task_amount' => (float) $data['period_' . $dataIndex]
    );
}

$res['client_stat'] = array(
    'month_stat' => $monthStat
);
