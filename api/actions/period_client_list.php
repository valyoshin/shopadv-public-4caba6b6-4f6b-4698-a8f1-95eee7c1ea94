<?php

// Action: period_client_list
// Return client list for the specific period (used by tasks started from this period or only used in this period (flag))
// Input:
//    period - str format YYYY-MM 
//             period can be empty, it means current period
//    flag - if exists, than clients will be only in task for targed period
// Output:
//    client_list: array (sorted by title)
//        client - client id
//        title - client title

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$period = $Input->getParam('period', true);
if (!$period) {
    $period = $_SESSION[$sid][$ip]['config']['period_start'] . '-01';
}

$flag = $Input->getIsSet('flag');
$res['client_list'] = $DB->periodClientList($period, $flag);
if(!$res['client_list']) {
    $res['client_list'] = array();
}

