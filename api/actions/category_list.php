<?php

// Action: category_list
// Return category list
// Input:
//    category_title - string for search (by title "LIKE style")
//    category_group_title - string for search ("LIKE style")
//    page - page to view
//    count - max count of rows to return
//    sort_col - column index to sort
//    sort_dir - ASC | DESC
// Output:
//    category_list: array (sorted by title)
//        category - category id
//        category_group
//        category_group_title
//        category_title - client title
//    rstart
//    rcount
//    count
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$filter = $Input->getParamDataArr(array('category_title', 'category_group_title'));
$page = $Input->getParam('page');
$count = $Input->getParam('count');
if(!$count || $count > MAX_ROWS) {
    $count = MAX_ROWS;
}
$start = ($page - 1) * $count;
$sort_col = $Input->getParam('sort_col');
if(!$sort_col) {
    $sort_col = 'category_title';
}
$sort_dir = $Input->getParam('sort_dir');

$res['category_list'] = $DB->categoryList($filter, $start, $count, $sort_col, $sort_dir);

if(!$res['category_list']) {
    $res['category_list'] = array();
    $res['count'] = 0;
} else {
    $res['count'] = $DB->foundRows();
}
$res['rstart'] = $start;
$res['rcount'] = $count;
