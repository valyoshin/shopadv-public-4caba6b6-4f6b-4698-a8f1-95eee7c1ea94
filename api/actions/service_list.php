<?php

// Action: service_list
// Return service list
// Input:
//    service
//    service_title - string for search (by title "LIKE style")
//    period_type
//    state
//    page - page to view
//    count - max count of rows to return
//    sort_col - column index to sort
//    sort_dir - ASC | DESC
// Output:
//    service_list: array (sorted by service ID)
//        service : int - service id
//        service_title : str - service title
//        period_type : D,W,M,Q
//        service_type : M,A,C
//        min_period_count : int
//        price : currency
//        price_alcohol : currency
//        have_alcohol : true | false
//        have_shop_list : true | false
//        have_plan_list : true | false
//        have_maket : true | false
//        have_doc : true | false
//        have_description : true | false
//        have_payment : true | false
//        have_fine : true | false
//        have_days : true | false
//        have_town_price : true | false
//        shop_access : true | false
//        copy_access : true | false
//        state : true | false
//    rstart
//    rcount
//    count

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$filter = $Input->getParamDataArr(array('service', 'service_title', 'period_type', 'state'));
$page = $Input->getParam('page');
$count = $Input->getParam('count');
if(!$count || $count > MAX_ROWS) {
    $count = MAX_ROWS;
}
$start = ($page - 1) * $count;
$sort_col = $Input->getParam('sort_col');
if(!$sort_col) {
    $sort_col = 'service';
}
$sort_dir = $Input->getParam('sort_dir');

$arr = $DB->serviceList($filter, $start, $count, $sort_col, $sort_dir);
$res['service_list'] = array();
if ($arr) {
    foreach ($arr as $row) {
        $row['have_alcohol'] = (bool) $row['have_alcohol'];
        $row['have_shop_list'] = (bool) $row['have_shop_list'];
        $row['have_plan_list'] = (bool) $row['have_plan_list'];
        $row['have_maket'] = (bool) $row['have_maket'];
        $row['have_doc'] = (bool) $row['have_doc'];
        $row['have_description'] = (bool) $row['have_description'];
        $row['have_payment'] = (bool) $row['have_payment'];
        $row['have_fine'] = (bool) $row['have_fine'];
        $row['have_days'] = (bool) $row['have_days'];
        $row['shop_access'] = (bool) $row['shop_access'];
        $row['copy_access'] = (bool) $row['copy_access'];
        $row['state'] = (bool) $row['state'];
        $res['service_list'][] = $row;
    }
}