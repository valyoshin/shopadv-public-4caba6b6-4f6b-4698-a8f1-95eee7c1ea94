<?php

// Action: report_client_sale_history_make
// Input:
//    client: int (M)
//    period_start: date (M)
//    period_end: date (M)
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

require_once(API_DIR . 'classes/ExcelGeneratorLines.php');

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$clientInfo = $DB->clientInfo($clientId);
if (!$clientInfo) {
    throw new Exception('Client is not found', ERR_OBJ_NOT_FOUND);
}

$periodStart = $Input->getParam('period_start', true);
if (!$periodStart) {
    throw new Exception('<period_start> is required', ERR_PARAM_MISSING);
}

$periodEnd = $Input->getParam('period_end', true);
if (!$periodEnd) {
    throw new Exception('<period_end> is required', ERR_PARAM_MISSING);
}

$template = 'client_sale_history';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

$data = $DB->taskHistory($periodStart, $periodEnd, $clientId);

define('ROW_TOP', 2);
define('COL_RIGHT', 7);

$docTitle = $periodStart.'_'.$periodEnd;

$EG = new ExcelGeneratorLines(TEMP_DIR);

$EG->open($templateFilename);
$EG->setSheetName($EG->getSheetIndex(), $docTitle);

$EG->addStyle('quantity', array(
    'font' => array(
        'name' => 'Tahoma',
        'size' => 9,
        'bold' => false
    ),
    'numberformat' => array(
        'code' => '0'
    )
));
$EG->addStyle('money', array(
    'font' => array(
        'name' => 'Tahoma',
        'size' => 9,
        'bold' => false
    ),
    'numberformat' => array(
        'code' => '0.00'
    )
));
$EG->addStyle('money_bold', array(
    'font' => array(
        'name' => 'Tahoma',
        'size' => 9,
        'bold' => true
    ),
    'numberformat' => array(
        'code' => '0.00'
    )
));

$services = array();

$totalQuantity = 0;
$totalAmount = 0.0;
$totalFine = 0.0;

$rows = array();
foreach ($data as $dataRow) {
    $period = $dataRow['period'];
    $task = (int) $dataRow['task'];
    $serviceId = (int) $dataRow['service'];
    $title = $dataRow['title'];
    $description = $dataRow['description'];
    $quantity = (int) $dataRow['quantity'];
    $price = is_numeric($dataRow['price']) ? (float) $dataRow['price'] : null;
    $amount = (float) $dataRow['amount'];
    $payment = is_numeric($dataRow['payment']) ? (float) $dataRow['payment'] : null;
    $fine = !is_null($payment) ? $payment - $amount : null;
    $taskPeriodCheck = (bool) $dataRow['task_period_check'];

    $totalQuantity += $quantity;
    $totalAmount += $amount;
    $totalFine += $fine;

    if (!isset($services[$serviceId])) {
        $serviceTitle = $DB->serviceTitle($serviceId);
        if (!$serviceTitle) {
            continue;
        }
        $services[$serviceId] = $serviceTitle;
    } else {
        $serviceTitle = $services[$serviceId];
    }

    $row = array();

    $row[] = Period::periodAsWin($period); // Period
    $row[] = $serviceTitle; // Service title
    $row[] = $title; // Task title
    $row[] = array('value' => $quantity, 'style' => 'quantity');
    $row[] = array('value' => $price, 'style' => 'money');
    $row[] = array('value' => $amount, 'style' => 'money');
    $row[] = array('value' => $fine, 'style' => 'money');
    $row[] = $description; // Task description
    $row[] = $taskPeriodCheck ? 'да' : ''; // Check doc

    $rows[] = $row;
}

// Totals

$row = array();

$row[] = '';
$row[] = '';
$row[] = 'Всего';
$row[] = array('value' => $totalQuantity, 'style' => 'quantity');
$row[] = '';
$row[] = array('value' => $totalAmount, 'style' => 'money');
$row[] = array('value' => $totalFine, 'style' => 'money');
$row[] = '';
$row[] = '';

$rows[] = $row;

$EG->writeRows(0, ROW_TOP, $rows);
$EG->writeBorders(0, ROW_TOP, COL_RIGHT, ROW_TOP + count($rows) - 1);

$doc = md5(Utils::uniqid());
$destFilename = DOC_DIR . $doc . '.xls';

$EG->save($destFilename);
$EG->close();

$title = $clientId.'_'.$clientInfo['title'].'_'.$docTitle . '_' . date('Y_m_d') . '.xls';

if (!$DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
    throw new Exception('Error creating document at DB');
}

$res['doc'] = $doc;
