<?php

// Action: task_add
// Input:
//    service
//    client
//    client_title
//    maket
//    title
//    description
//    period_start
//    period_end
//    quantity
//    price
//    payment
//    ts
//    have_alcohol
//    category_list
//    doc_list
//    service_type
// Output:
//    task: int
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$data = $Input->getParamValidArr(array('service', 'client', 'client_title', 'maket', 'period_start', 'period_end', 'title', 'description', 'quantity', 'price', 'payment', 'have_alcohol', 'service_type'));
if (!isset($data['service']) || !$data['service']) {
    throw new Exception('<service> is required', ERR_PARAM_MISSING);
}

$serviceInfo = $DB->serviceInfo($data['service']);
if (!$serviceInfo) {
    throw new Exception('<service> is required (2)', ERR_PARAM_MISSING);
}
if ($serviceInfo['have_plan_list']) {
    throw new Exception('Task cannot be added - use import instead', ERR_ACTION_WRONG);
}
if (!isset($data['service_type']) || !$data['service_type'] || $serviceInfo['service_type'] != 'C') {
    $data['service_type'] = $serviceInfo['service_type'];
}
if (isset($data['payment']) && !$serviceInfo['have_payment']) {
    unset($data['payment']);
}

if (!isset($data['client']) || !$data['client']) {
    if (isset($data['client_title']) && $data['client_title']) {
        $data['client'] = $DB->clientByTitle($data['client_title']);
        if (!$data['client']) {
            $data['client'] = $DB->clientAdd(array('title' => $data['client_title']));
        }
        if (!$data['client']) {
            throw new Exception('<client> is required', ERR_PARAM_MISSING);
        }
    } else {
        throw new Exception('<client> is required', ERR_PARAM_MISSING);
    }
}
unset($data['client_title']);
if (!isset($data['period_start']) || !$data['period_start']) {
    throw new Exception('<period_start> is required', ERR_PARAM_MISSING);
}
if (!isset($data['period_end']) || !$data['period_end']) {
    throw new Exception('<period_end> is required', ERR_PARAM_MISSING);
}

require_once('_task_shop.php');
$TS = $Input->getParam('ts', true);
processTaskShop($Input, $TS, $data);

$task = $DB->taskAdd($data);

if ($task) {

    // set new color
    $DB->taskUpdate($task, array('color' => getNewColor($task)));

    $res['task'] = $task;
    if ($TS) {
        foreach ($TS as $shop => $row) {
            $DB->taskShopAdd($task, $shop, $row['q'], $row['p'], $row['d']);
        }
    }

    $docList = $Input->getParam('doc_list', true);
    if ($docList) {
        foreach ($docList as $doc) {
            $DB->taskDocAdd($task, $doc);
        }
    }

    $categoryList = $Input->getParam('category_list', true);
    if ($categoryList) {
        foreach ($categoryList as $category) {
            $DB->taskCategoryAdd($task, $category);
        }
    }

} else {
    throw new Exception('Error while adding. Probably, value has already exist.', ERR_ADD);
}
