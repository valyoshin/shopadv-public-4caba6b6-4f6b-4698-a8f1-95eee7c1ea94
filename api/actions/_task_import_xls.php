<?php

/** @var array $serviceInfo */

$match = array(
  'plan_title' => array(
    'map' => 'имя планограм',
    'index' => -1,
    'type' => 'string',
  ),
  'quantity' => array(
    'map' => 'колич',
    'index' => -1,
    'type' => 'int',
  ),
  'amount' => array(
    'map' => 'стоимост',
    'index' => -1,
    'type' => 'float',
  ),
  'prod_code' => array(
    'map' => 'id',
    'index' => -1,
    'type' => 'int',
  ),
  'prod_title' => array(
    'map' => 'имя',
    'index' => -1,
    'type' => 'string',
  ),
  'prod_tm' => array(
    'map' => 'марка',
    'index' => -1,
    'type' => 'string',
  ),
  'client_title' => array(
    'map' => 'поставщ',
    'index' => -1,
    'type' => 'string',
  ),
  'prod_date' => array(
    'map' => 'дата ввода прод',
    'index' => -1,
    'type' => 'date',
  ),
);

function convert($type, $value) {
  switch ($type) {
    case 'int':
      return (int) $value;
    case 'float':
      return (float) $value;
    case 'date':
      return PHPExcel_Style_NumberFormat::toFormattedString($value, PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
    default:
      return Utils::asStr((string) $value);
  }
}

function getMatchIndex($str) {
  global $match;

  foreach ($match as $key => $map) {
    if (strpos($str, $map['map']) !== false) {
      return $key;
    }
  }

  return false;
}

function isValid($dataRow) {
  global $Loger;
  if (!$dataRow['quantity']) {
    $Loger->debugStr('>>> empty quantity');

    return false;
  }

  if (!$dataRow['prod_tm']) {
    $Loger->debugStr('>>> prod tm');

    return false;
  }

  if (!$dataRow['client_title']) {
    $Loger->debugStr('>>> empty client');

    return false;
  }

  if (!$dataRow['prod_date']) {
    $Loger->debugStr('>>> empty prod date');

    return false;
  }

  return true;
}

require_once(API_DIR . 'classes/ExcelGenerator.php');

$Loger->timestamp('open gen');

$EG = new ExcelGenerator(TEMP_DIR);

$EG->open($filename);

$Loger->timestamp('open done');

// read header

$rowStart = 1;
$colCount = $EG->getLastColumn();
$rowCount = $EG->getLastRow();

$Loger->debugStr('Excel dim: ' . $colCount . ': ' . $rowCount);

for ($col = 0; $col < $colCount; $col++) {
  $cell = $EG->getCellValue($col, $rowStart);
  $cell = mb_strtolower($cell);

  $index = getMatchIndex($cell);
  $Loger->debugStr($col . ': ' . $cell . ': ' . $index);
  if ($index) {
    $match[$index]['index'] = $col;
  }
}

$Loger->timestamp('match done');

// read data

$data = array();
for ($row = $rowStart + 1; $row <= $rowCount; $row++) {
  $dataRow = array();

  foreach ($match as $key => $map) {
    $dataRow[$key] = '';
    if ($map['index'] >= 0) {
      $cellStr = $EG->getCellValue($map['index'], $row);
      if ($cellStr) {
        $dataRow[$key] = convert($map['type'], $cellStr);
      }
    }
  }

  if (isValid($dataRow)) {
    $data[] = $dataRow;
  } else {
    $Loger->debugStr('Invalid data row: ' . print_r($dataRow, true));
  }
}

$Loger->timestamp('read done. count: ' . count($data));

$EG->close();

// processing

$Loger->debugStr('processing...');

$tasks = array();
$prods = array();

foreach ($data as $dataRow) {

  $clientTitle = $dataRow['client_title'];

  if (mb_strpos($clientTitle, SKIP_CLIENT_TITLE) !== false) {
    throw new Exception('Wrong client title', ERR_SYSTEM);
  }

  // client
  $client = $DB->clientByTitle($clientTitle);
  if (!$client) {
    $client = $DB->clientAdd(array(
      'title' => $dataRow['client_title'],
    ));
  }
  if (!$client) {
    $Loger->debugStr('Client is not found and created: ' . $dataRow['client_title']);
    continue;
  }
  // prod
  if ($dataRow['prod_code']) {
    $prod = $DB->prodByCode($dataRow['prod_code']);
  } else {
    $prod = null;
  }
  $prodData = array(
    'code' => $dataRow['prod_code'],
    'title' => $dataRow['prod_title'],
    'tm' => $dataRow['prod_tm'],
  );
  if (!$prod) {
    $prod = $DB->prodAdd($prodData);
  } else {
    $DB->prodUpdate($prod, $prodData);
  }
  if (!$prod) {
    $Loger->debugStr('Prod is not found and created: ' . print_r($prodData, true));
    continue;
  }

  $pd = Period::decode($dataRow['prod_date']);
  $de = Period::encode($pd['y'], $pd['m'], $pd['d']);
  if ($pd['d'] > 15) {
    $pd['m']++;
  }
  $ps = Period::encode($pd['y'], $pd['m'], 1);
  //$pe = Period::encode($pd['y'], $pd['m'], Period::daysInMonth($pd['m'], $pd['y']));

  if ($ps > $periodLast) {
    $Loger->debugStr('Period skip: ' . $ps);
    continue;
  }

  $ps = $period;
  $pe = $periodLast;

  $tm = $dataRow['prod_tm'];

  $key = $client . ':' . $ps;

  $q = (int) $dataRow['quantity'];
  if (isset($dataRow['amount']) && $dataRow['amount']) {
    $amount = (float) $dataRow['amount'];
    $p = $q > 0 ? $amount / $q : 0.0;
  } else {
    $p = (float) $serviceInfo['price'];
    $amount = $p * $q;
  }

  $prods[$key][] = array(
    'task' => null,
    'prod' => $prod,
    'title' => $dataRow['plan_title'],
    'quantity' => $dataRow['quantity'],
    'price' => $p,
    'date_entry' => $de,
  );

  if (!isset($tasks[$key])) {
    $tasks[$key] = array(
      'service' => $serviceInfo['service'],
      'client' => $client,
      'title' => '',
      'period_start' => $ps,
      'period_end' => $pe,
      'quantity' => $q,
      'price' => $p,
      'amount' => $amount,
      'service_type' => $serviceInfo['service_type'],
    );
  } else {
    $tasks[$key]['quantity'] += $q;
    $tasks[$key]['amount'] += $amount;
  }
}

// saving

$Loger->debugStr('saving...');

foreach ($tasks as $key => $taskData) {
  if ($taskData['quantity'] > 0) {
    $taskData['price'] = $taskData['amount'] / $taskData['quantity'];
  }
  $task = $DB->taskAdd($taskData);
  $taskList[] = $task;
  $color = getNewColor($task);
  $DB->taskUpdate($task, array('color' => $color));
  foreach ($prods[$key] as $prodData) {
    $prodData['task'] = $task;
    $DB->taskPlanAdd($prodData);
  }
}

