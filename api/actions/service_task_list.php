<?php

// Action: service_task_list
// Return task list for service (for every service can be different)
// Input:
//    service - id of service
//    period - str format YYYY-MM (start period)
//    page - page to view
//    count - max count of rows to return
//    sort_col - column index to sort
//    sort_dir - ASC | DESC
//    --- filter:
//    client
//    code
//    title
//    description
// Output:
//    rstart
//    rcount
//    count
//    task_list: array (sorted by title)
//        task
//        client
//        client_title
//        doc_list
//        code
//        color
//        price
//        quantity
//        amount
//        payment
//        period_start
//        period_end
//        period_<i>:
//          isCharged; isChecked
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$service = $Input->getParam('service', true);
if(!$service) {
    throw new Exception('<service> is required', ERR_PARAM_MISSING);
}

$period = $Input->getParam('period', true);
if (!$period) {
    $period = $_SESSION[$sid][$ip]['config']['period_start'] . '-01';
}

$filter = $Input->getParamDataArr(array('client', 'code', 'title', 'description'));
$page = $Input->getParam('page');
$count = $Input->getParam('count');
if(!$count || $count > MAX_ROWS) {
    $count = MAX_ROWS;
}
$start = ($page - 1) * $count;
$sort_col = $Input->getParam('sort_col');
if(!$sort_col) {
    $sort_col = 'client_title';
}
$sort_dir = $Input->getParam('sort_dir');

$res['task_list'] = $DB->serviceTaskList($service, $period, PERIOD_COUNT, $filter, $start, $count, $sort_col, $sort_dir);
if(!$res['task_list']) {
    $res['task_list'] = array();
    $res['count'] = 0;
} else {
    $res['count'] = $DB->foundRows();
}    
   
$res['rstart'] = $start;
$res['rcount'] = $count;

