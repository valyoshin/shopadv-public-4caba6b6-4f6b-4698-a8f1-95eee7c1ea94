<?php

// Action: budget_get
// Input:
//    year (M) - year of the budget
// Output:
//    budget: array
//      year: int
//      data: mixed
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$dataText = $DB->budgetGet($year);
if ($dataText) {
    $data = json_decode($dataText, true) ?: null;
} else {
    $data = null;
}

$res['budget'] = array(
    'year' => $year,
    'data' => $data,
);
