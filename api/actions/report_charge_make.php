<?php

// Action: report_charge_make
// Input:
//    services
//    period_s
//    period_e
//    service_type
//    charge_type
//    client
// Output:
//    doc: str[32]
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
  throw new Exception('Action is not allowed', ERR_USER_DENY);
}

require_once(API_DIR . 'classes/ExcelGeneratorLines.php');
require_once(API_DIR . 'classes/period.php');
require_once(API_DIR . 'lang/uk.php');

$services = $Input->getParam('services', true);
if (!$services || !count($services)) {
  throw new Exception('<services> is required', ERR_PARAM_MISSING);
}

$serviceType = $Input->getParam('service_type', true);

$periodStart = $Input->getParam('period_s', true);
if (!$periodStart) {
  throw new Exception('<period_s> is required', ERR_PARAM_MISSING);
}

$periodEnd = $Input->getParam('period_e', true);
if (!$periodEnd) {
  throw new Exception('<period_e> is required', ERR_PARAM_MISSING);
}

$chargeType = $Input->getParam('charge_type');

$client = $Input->getParam('client');

switch ($chargeType) {
  case 'charge':
    include('_report_charge.php');
    break;
  case 'charge_service':
    include('_report_charge_service.php');
    break;
  case 'charge_client':
    include('_report_charge_client.php');
    break;
  case 'charge_fine':
    include('_report_charge_fine.php');
    break;
  case 'charge_extra':
    include('_report_charge_extra.php');
    break;
  case 'charge_promo':
    include('_report_charge_promo.php');
    break;
  default:
    throw new Exception('<charge_type> is invalid', ERR_PARAM_MISSING);
}
