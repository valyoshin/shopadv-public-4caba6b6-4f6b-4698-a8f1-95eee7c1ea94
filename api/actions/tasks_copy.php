<?php

// Action: tasks_copy
// Input:
//    period_s (M)
//    period_e (M)
//    service (M)
//    period_count (O)
//    task_list (O)
// Output:
//    none
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$data = $Input->getParamValidArr(array('service', 'period_s', 'period_e', 'task_list'));
if (!isset($data['service']) || !$data['service']) {
    throw new Exception('<service> is required', ERR_PARAM_MISSING);
}
if (!isset($data['task_list']) || !$data['task_list']) {
    throw new Exception('<task_list> is required', ERR_PARAM_MISSING);
}

$service = $data['service'];

if (!isset($data['period_s']) || !$data['period_s']) {
    throw new Exception('<period_s> is required', ERR_PARAM_MISSING);
}

$periodStart = $data['period_s'];

if (!isset($data['period_e']) || !$data['period_e']) {
    throw new Exception('<period_e> is required', ERR_PARAM_MISSING);
}

if (!isset($data['period_e']) || !$data['period_e']) {
    throw new Exception('<period_e> is required', ERR_PARAM_MISSING);
}

$periodEnd = $data['period_e'];

$serviceInfo = $DB->serviceInfo($service);
if (!$serviceInfo) {
    throw new Exception('<service> is required (2)', ERR_PARAM_MISSING);
}
if (!$serviceInfo['copy_access'] || $serviceInfo['period_type'] != 'M') {
    throw new Exception('Access denied', ERR_ACTION_WRONG);
}

$periodCount = $Input->getParam('period_count');

$taskList = $DB->taskListByServicePeriodInfo($service, $periodStart, $data['task_list']);

$taskListNew = array();

if ($taskList) {
    foreach ($taskList as $taskData) {
        $task = $taskData['task'];
        $taskData['task'] = null;
        for ($i = 0; $i < $periodCount; $i++) {
            $taskPeriodEnd = Period::incMonth($periodEnd, $i);
            $taskData['period_start'] = $taskPeriodEnd;
            $taskData['period_end'] = $taskPeriodEnd;
            $taskNew = $DB->taskAdd($taskData);
            if ($taskNew) {
                $taskListNew[] = $taskNew;
                $DB->copyTaskShop($task, $taskNew);
                $DB->copyTaskPlan($task, $taskNew);
                $DB->copyTaskDoc($task, $taskNew);
            }
        }
    }
}

$res['task_list'] = implode(',', $taskListNew);
