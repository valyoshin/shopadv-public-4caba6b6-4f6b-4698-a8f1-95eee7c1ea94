<?php

// Action: task_info
// Input:
//    task
// Output:
//    task: array 
//        task - task id
//        service - service id
//        client - client id
//        maket - maket id
//        title
//        description
//        period_start
//        period_end
//        quantity
//        price
//        amount
//        payment
//        have_alcohol
//        color
//        category_list
//        doc_list
//    ts: array indexed by shop
//       q - quantity
//       p - price
//       d - days
//       desc - description
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$task = $Input->getParam('task', true);
if(!$task) {
    throw new Exception('<task> is required', ERR_PARAM_MISSING);
}

require_once('_task_shop.php');
$res['task'] = $DB->taskInfo($task);
if($res['task']) {
    $res['task']['have_alcohol'] = (bool) $res['task']['have_alcohol'];
    $ts = $DB->taskShopInfo($task);
    if($ts) {
        $res['ts'] = prepareTaskShop($ts);
    } else {
        $res['ts'] = array();
    }
    $res['task']['category_list'] = $DB->getTaskCategories($task);
}
