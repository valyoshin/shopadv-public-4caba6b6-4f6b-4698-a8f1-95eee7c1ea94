<?php

// Action: towns_cache
// Input:
//    none
// Output:
//    town_list: array (sorted by title)
//        town
//        title
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$res['town_list'] = $DB->townListCache();

if(!$res['town_list']) {
    $res['town_list'] = array();
}
