<?php

// Action: maket_add
// Input:
//    fdata - file data
// Output:
//    maket: int
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

require_once(API_DIR . 'classes/photomanager.php');

if (!isset($_FILES['fdata']) || !$_FILES['fdata']['tmp_name']) {
    throw new Exception('<fdata> is required', ERR_PARAM_MISSING);
}

// UPLOAD FILE
$src = TEMP_DIR . Utils::uniqid();
if (!@move_uploaded_file($_FILES['fdata']['tmp_name'], $src)) {
    @unlink($src);
    throw new Exception('system error (move_uploaded_file)', ERR_SYSTEM);
}

$PH = new PhotoManager(TEMP_DIR);

$PH->UseScale = true;
$PH->ScaleWidthMin = IMG_W_MIN;
$PH->ScaleWidthMax = IMG_W_MAX;

$PH->SrcFileName = $src;
$PH->DestFileName = $src;
if (!$PH->process()) {
    @unlink($src);
    throw new Exception($PH->error() . ': ' . $PH->errorMsg(), ERR_PHOTO);
}

// PHASH
$filesize = @filesize($src);
if (!$filesize) {
    @unlink($src);
    throw new Exception('system error (filesize)', ERR_SYSTEM);
}
$fhash = Utils::fileHash($src);
if (!$fhash) {
    @unlink($src);
    throw new Exception('system error (fileHash)', ERR_SYSTEM);
}

// SEARCH FHASH
$maket = $DB->maketSearch($fhash);

if (!$maket) {
    $maket = $DB->maketAdd($fhash, $filesize);
    if(!$maket) {
        @unlink($src);
        throw new Exception('Error while adding. Probably, value has already exist.', ERR_ADD);
    }
    $dest = MAKET_DIR . $maket . '.jpg';
    if (!@rename($src, $dest)) {
        @unlink($src);
        throw new Exception('system error (rename)', ERR_SYSTEM);
    }
}

@unlink($src);
$res['maket'] = $maket;

