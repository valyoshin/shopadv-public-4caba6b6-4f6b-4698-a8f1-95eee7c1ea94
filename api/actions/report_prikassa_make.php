<?php

// Action: report_prikassa_make
// Input:
//    period_s (M)
//    period_e (M)
//    client (O)
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

require_once(API_DIR . 'classes/ExcelGeneratorLines.php');
require_once(API_DIR . 'classes/period.php');

$periodStart = $Input->getParam('period_s', true);
if (!$periodStart) {
    throw new Exception('<period_s> is required', ERR_PARAM_MISSING);
}
$periodEnd = $Input->getParam('period_e', true);
if (!$periodEnd) {
    throw new Exception('<period_e> is required', ERR_PARAM_MISSING);
}
if ($periodStart > $periodEnd) {
    throw new Exception('<period_e> must be greater or equal than <period_s>', ERR_PARAM_MISSING);
}

$client = $Input->getParam('client');
$service = $DB->getServiceByCode(12); // ID of THE prikassa service

$template = 'prikassa';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

define('ROW_START', 2);
define('COL_START', 0);
define('COL_CLIENT_TITLE', COL_START + 0);
define('COL_PLAN_TITLE', COL_START + 1);
define('COL_PROD_CODE', COL_START + 2);
define('COL_PROD_TITLE', COL_START + 3);
define('COL_TASK_TITLE', COL_START + 4);
define('COL_PLAN_QUANTITY', COL_START + 5);
define('COL_PLAN_PRICE', COL_START + 6);
define('COL_PLAN_AMOUNT', COL_START + 7);
define('COL_PLAN_DATE_ENTRY', COL_START + 8);

$EG = new ExcelGeneratorLines(TEMP_DIR);

try {

    $EG->open($templateFilename);
    $EG->setSheetName($EG->getSheetIndex(), Period::periodAsWin($periodStart) . '-' . Period::periodAsWin($periodEnd));

    // styles
    $EG->addStyle('text', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11
        ),
        'alignment' => array(
            'horizontal' => 'left'
        )
    ));
    $EG->addStyle('center', array(
        'font' => array(
            'name' => 'Courier New',
            'size' => 10
        ),
        'alignment' => array(
            'horizontal' => 'center'
        )
    ));
    $EG->addStyle('quantity', array(
        'font' => array(
            'name' => 'Courier New',
            'size' => 10,
            'bold' => false
        ),
        'numberformat' => array(
            'code' => '0'
        )
    ));
    $EG->addStyle('money', array(
        'font' => array(
            'name' => 'Courier New',
            'size' => 10,
            'bold' => false
        ),
        'numberformat' => array(
            'code' => '0.00'
        )
    ));
    $EG->addStyle('total', array(
        'font' => array(
            'name' => 'Courier New',
            'size' => 10,
            'bold' => true
        ),
        'numberformat' => array(
            'code' => '0.00'
        )
    ));    
    
    $row = ROW_START;
    $taskPlanList = $DB->reportTaskPlan($service, $periodStart, $periodEnd, $client);
    if ($taskPlanList) {
        foreach ($taskPlanList as $taskPlanData) {
            $quantity = (int) $taskPlanData['quantity'];
            $price = (float) $taskPlanData['price'];
            $amount = $quantity * $price;
            
            $R[COL_CLIENT_TITLE] = array('value' => $taskPlanData['client_title'], 'style' => 'text');
            $R[COL_PLAN_TITLE] = array('value' => $taskPlanData['plan_title'], 'style' => 'text');
            $R[COL_PROD_CODE] = array('value' => $taskPlanData['prod_code'], 'style' => 'center');
            $R[COL_PROD_TITLE] = array('value' => $taskPlanData['prod_title'], 'style' => 'text');
            $R[COL_TASK_TITLE] = array('value' => $taskPlanData['task_title'], 'style' => 'center');
            $R[COL_PLAN_QUANTITY] = array('value' => $quantity, 'style' => 'quantity');
            $R[COL_PLAN_PRICE] = array('value' => $price, 'style' => 'money');
            $R[COL_PLAN_AMOUNT] = array('value' => $amount, 'style' => 'total');
            $R[COL_PLAN_DATE_ENTRY] = array('value' => Period::dateAsWin($taskPlanData['date_entry']), 'style' => 'center');
            
            $EG->writeRow(COL_START, $row, $R);
            $row++;
        } // taskPlanData
    }

    // make borders
    $EG->writeBorders(0, 1, COL_PLAN_DATE_ENTRY, $row - 1);

    $doc = md5(Utils::uniqid());
    $destFilename = DOC_DIR . $doc . '.xls';

    $EG->save($destFilename);
    $EG->close();

    $title = 'Prikassa_' . Period::periodAsWin($periodStart) . '_' . Period::periodAsWin($periodEnd) . '_' . date('Y_m_d') . '.xls';

    if (!$DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
        throw new Exception('Error creating document at DB');
    }
    
} catch (Exception $ex) {
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
