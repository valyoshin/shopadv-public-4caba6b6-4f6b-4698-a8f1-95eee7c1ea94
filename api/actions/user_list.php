<?php

// Action: user_list
// Return user list
// Input:
//    login
//    name
//    email
//    role
//    state
//    count - max count of rows to return
//    sort_col - column index to sort
//    sort_dir - ASC | DESC
// Output:
//    user_list: array (sorted by title)
//        login
//        name
//        email
//        role
//        state
//    rstart
//    rcount
//    count
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$filter = $Input->getParamDataArr(array('login', 'name', 'email', 'role', 'state'));
$page = $Input->getParam('page');
$count = $Input->getParam('count');
if(!$count || $count > MAX_ROWS) {
    $count = MAX_ROWS;
}
$start = ($page - 1) * $count;
$sort_col = $Input->getParam('sort_col');
if(!$sort_col) {
    $sort_col = 'login';
}
$sort_dir = $Input->getParam('sort_dir');

$arr = $DB->userList($filter, $start, $count, $sort_col, $sort_dir);
$res['user_list'] = array();
if ($arr) {
    foreach ($arr as $row) {
        $row['state'] = (bool) $row['state'];
        $res['user_list'][] = $row;
    }
    $res['count'] = $DB->foundRows();
} else {
    $res['count'] = 0;
}

$res['rstart'] = $start;
$res['rcount'] = $count;
        