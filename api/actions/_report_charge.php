<?php

// Action: _report_charge SUB template
// Input:
//    services 
//    period_s
//    period_e
//    client
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

function formatPeriodList($plist) {
    if (!$plist) {
        return '';
    }
    
    $res = array();
    $arr = explode(',', $plist);
    foreach ($arr as $p) {
        $res[] = Period::periodAsWin($p);
    }
    
    return " " . implode(', ', $res);
}

$template = 'charge';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

// COLS
//    'no'
//    'client_title'
//    'client_code'
//    'title'
//    'service_title_buh'
//    'service_point'
//    'price'
//    'quantity'
//    'amount'
//    'comment'
//    'category' list


define('ROW_TOP', 2);

$EG = new ExcelGeneratorLines(TEMP_DIR);

try {

    $EG->open($templateFilename);
    $EG->setSheetName($EG->getSheetIndex(), Period::periodAsWin($periodStart) . '_' . Period::periodAsWin($periodEnd));
    
    // styles
    $EG->addStyle('bold', array(
        'font' => array(
            'name' => 'Courier New',
            'size' => 10,
            'bold' => true
        )
    ));
    $EG->addStyle('quantity', array(
        'font' => array(
            'name' => 'Courier New',
            'size' => 10,
            'bold' => false
        ),
        'numberformat' => array(
            'code' => '0'
        )
    ));
    $EG->addStyle('money', array(
        'font' => array(
            'name' => 'Courier New',
            'size' => 10,
            'bold' => true
        ),
        'numberformat' => array(
            'code' => '0.00'
        )
    ));
    
    $row = ROW_TOP;
    $lastCol = 0;
    foreach ($services as $service) {
        $serviceInfo = $DB->serviceInfo($service);
        if(!$serviceInfo) {
            continue;
        }
        if ($serviceInfo['service_type'] === 'C') {
            $st = $serviceType;
        } else {
            $st = false;
        }
        $tasks = $DB->taskClientListByServicePeriod($periodStart, $periodEnd, $service, $client, $st);
        
        if ($tasks) {
            $RS = array();
            $i = 1;
            $totalQuantity = 0;
            $totalAmount = 0;
            foreach ($tasks as $task) {
                $taskId = (int) $task['task'];
                $chargeCount = (int) $task['charge_count'];
                $price = '';
                $amount = (float) $task['amount'];
                $payment = $amount;
                $quantity = (int) $task['quantity'];
                if ($serviceInfo['have_payment']) {
                    if ($task['payment']) {
                        $payment = (float) $task['payment'];
                    }
                } else {
                    if ($task['price']) {
                        $price = (float) $task['price'];
                    }
                }
                
                $chargeQuantity = $chargeCount * $quantity;
                $chargePayment = $chargeCount * $payment;
                
                $R = array();
                $R[] = $i; // no
                $R[] = $task['client_title']; // client_title
                $R[] = $task['client_code']; // client_code
                $R[] = $task['title']; // task title
                $R[] = $serviceInfo['title_buh']; // service_title_buh
                $R[] = $serviceInfo['service_point']; // service_point
                $R[] = array('value' => $price, 'style' => 'money'); // price
                $R[] = array('value' => $chargeQuantity, 'style' => 'quantity'); // quantity
                $R[] = array('value' => $chargePayment, 'style' => 'money'); // amount
                // comment
                if ($serviceInfo['period_type'] == 'D' && !$serviceInfo['have_shop_list']) {
                    $R[] = formatPeriodList($task['period_list']) . ': ' . Period::dateAsWin($task['period_start']) . '-' . Period::dateAsWin($task['period_end']);
                } else {
                    $R[] = formatPeriodList($task['period_list']);
                }
                // category
                $categoryTitles = $DB->getTaskCategoryTitles($taskId);
                $R[] = implode(', ', $categoryTitles);

                $lastCol = count($R) - 1;

                $RS[] = $R;
                // total
                $totalQuantity += $chargeQuantity;
                $totalAmount += $chargePayment;
                $i++;
            }
            // total row
            $T = array();
            $T[] = ''; // no
            $T[] = array('value' => $serviceInfo['title'], 'style' => 'bold'); 
            $T[] = '';
            $T[] = '';
            $T[] = '';
            $T[] = '';
            $T[] = ''; // price
            $T[] = array('value' => $totalQuantity, 'style' => 'quantity');
            $T[] = array('value' => $totalAmount, 'style' => 'money');
            $T[] = ''; // comment
            // excel
            $EG->writeRow(0, $row, $T);
            $EG->writeRows(0, $row + 1, $RS);
            $row += count($RS) + 1;
        }
    } // services
    // make borders
    $EG->writeBorders(0, ROW_TOP, $lastCol, $row - 1);

    $doc = md5(Utils::uniqid());
    $destFilename = DOC_DIR . $doc . '.xls';

    $EG->save($destFilename);
    $EG->close();
    
    $title = 'НБ_XXX_' . date('Y_m_d') . '.xls';
    
    if (! $DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
        throw new Exception('Error creating document at DB');
    }
    
} catch (Exception $ex) {
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
