<?php

// Action: stat
// статистика продаж за год: stat. Input: year (M), client (O), service (O). Output: stat amounts (price, quantity, amount) by client-service pair.
// Input:
//    year (M)
//    client (O)
//    service (O)
//    service_type: (O)
// Output:
//    stat: array (sorted by service, client)
//        service (id)
//        client (id)
//        stat: array by quarters of the year
//              q - total quantity for quarter i
//              a - total amount for quarter i
//

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$periodStart = $year.'-01-01';
$periodEnd = $year.'-12-31';

$filterClientId = $Input->getParam('client', true);
$filterServiceId = $Input->getParam('service', true);
$serviceType = $Input->getParam('service_type', true);

$stat = array();

$quarter[0] = $DB->taskStat($year.'-01-01', $year.'-03-31', $filterClientId, $filterServiceId, $serviceType);
$quarter[1] = $DB->taskStat($year.'-04-01', $year.'-06-30', $filterClientId, $filterServiceId, $serviceType);
$quarter[2] = $DB->taskStat($year.'-07-01', $year.'-09-30', $filterClientId, $filterServiceId, $serviceType);
$quarter[3] = $DB->taskStat($year.'-10-01', $year.'-12-31', $filterClientId, $filterServiceId, $serviceType);

foreach ($quarter as $index => $rows) {
    if ($rows) {
        foreach ($rows as $row) {
            $service = $row['service'];
            $client = $row['client'];
            $q = (int)$row['total_quantity'];
            $a = (float)$row['total_amount'];

            if (!isset($stat[$service][$client])) {
                $stat[$service][$client] = array(
                    array('q' => 0, 'a' => 0.0),
                    array('q' => 0, 'a' => 0.0),
                    array('q' => 0, 'a' => 0.0),
                    array('q' => 0, 'a' => 0.0),
                );
            }

            $stat[$service][$client][$index]['q'] += $q;
            $stat[$service][$client][$index]['a'] += $a;
        }
    }
}

$res['stat'] = array();
foreach ($stat as $service => $statService) {
    foreach ($statService as $client => $statServiceClient) {
        $res['stat'][] = array(
            'service' => (int) $service,
            'client' => (int) $client,
            'stat' => $statServiceClient
        );
    }
}