<?php

// Action: task_import
// Input:
//    fdata - file data
//    service
//    period
//    
//    ONLY FOR prikassa!!!
// Output:
//    none
//    

defined('IN_SITE') or die();

define('SKIP_CLIENT_TITLE', '=INDEX');

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
  throw new Exception('Action is not allowed', ERR_USER_DENY);
}

//error_log(print_r($_FILES, true));

if (!isset($_FILES['fdata']) || !$_FILES['fdata']['tmp_name']) {
  throw new Exception('<fdata> is required', ERR_PARAM_MISSING);
}

$filename = $_FILES['fdata']['tmp_name'];
if (!$filename) {
  throw new Exception('<fdata> is required: filename', ERR_PARAM_MISSING);
}

$service = $Input->getParam('service', true);
if (!$service) {
  throw new Exception('<service> is required 1', ERR_PARAM_MISSING);
}

$period = $Input->getParam('period', true);
if (!$period) {
  throw new Exception('<period> is required 1', ERR_PARAM_MISSING);
}

$pd = Period::decode($period);
$periodLast = Period::encode($pd['y'], $pd['m'], Period::daysInMonth($pd['m'], $pd['y']));

$Loger->debug($periodLast);

$serviceInfo = $DB->serviceInfo($service);
if (!$serviceInfo || !($serviceInfo['have_plan_list'] || $serviceInfo['import_access'])) {
  throw new Exception('<service> is required 2', ERR_PARAM_MISSING);
}

if ($serviceInfo['service_type'] == 'C') {
  $serviceInfo['service_type'] = 'M';
}

$names = explode('.',$_FILES['fdata']['name']);
$ext = strtolower(array_pop($names));

try {
  $taskList = array();

  if ($ext == 'json') {
    include('_task_import_json.php');
  } else {
    include('_task_import_xls.php');
  }

} catch (Exception $ex) {
  $msg = 'Error: ' . $ex->getMessage();
  $Loger->debug($msg);
  throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$Loger->debug($taskList);

$res['list'] = implode(',', $taskList);