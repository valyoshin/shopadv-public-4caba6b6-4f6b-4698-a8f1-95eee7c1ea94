<?php

// Action: report_budget_plan_make
// Input:
//    budget_plan: int (M)
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

require_once(API_DIR . 'classes/ExcelGeneratorLines.php');

$budgetPlanId = $Input->getParam('budget_plan', true);
if (!$budgetPlanId) {
    throw new Exception('<budget_plan> is required', ERR_PARAM_MISSING);
}

$budgetPlan = $DB->budgetPlan($budgetPlanId);
if (!$budgetPlan) {
    throw new Exception('budget plan is not found', ERR_OBJ_NOT_FOUND);
}

$template = 'budget_plan';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

$budgetPlanData = $DB->budgetPlanData($budgetPlanId);

define('ROW_TOP', 3);
define('COL_RIGHT', 14);

$docTitle = $budgetPlan['year'] . '_' . $budgetPlan['title'];

$EG = new ExcelGeneratorLines(TEMP_DIR);

$EG->open($templateFilename);
$EG->setSheetName($EG->getSheetIndex(), $docTitle);

// styles
//    $EG->addStyle('bold', array(
//        'font' => array(
//            'name' => 'Tahoma',
//            'size' => 9,
//            'bold' => true
//        )
//    ));
$EG->addStyle('quantity', array(
    'font' => array(
        'name' => 'Tahoma',
        'size' => 9,
        'bold' => false
    ),
    'numberformat' => array(
        'code' => '0'
    )
));
$EG->addStyle('money', array(
    'font' => array(
        'name' => 'Tahoma',
        'size' => 9,
        'bold' => false
    ),
    'numberformat' => array(
        'code' => '0.00'
    )
));
$EG->addStyle('money_bold', array(
    'font' => array(
        'name' => 'Tahoma',
        'size' => 9,
        'bold' => true
    ),
    'numberformat' => array(
        'code' => '0.00'
    )
));

$services = array();
$clients = array();

$rows = array();
foreach ($budgetPlanData as $budgetPlanRow) {
    $serviceId = (int)$budgetPlanRow['service'];
    $clientId = (int)$budgetPlanRow['client'];
    $data = json_decode($budgetPlanRow['data'], true) ?: array();

    if (!isset($services[$serviceId])) {
        $serviceTitle = $DB->serviceTitle($serviceId);
        if (!$serviceTitle) {
            continue;
        }
        $services[$serviceId] = $serviceTitle;
    } else {
        $serviceTitle = $services[$serviceId];
    }

    if (!isset($clients[$clientId])) {
        $clientTitle = $DB->clientTitle($clientId);
        if (!$clientTitle) {
            continue;
        }
        $clients[$clientId] = $clientTitle;
    } else {
        $clientTitle = $clients[$clientId];
    }

    $total = 0.0;

    $row = array();

    $row[] = $serviceTitle; // Service title
    $row[] = $clientTitle; // Client title

    for ($q = 0; $q <= 3; $q++) {
        $quantity = isset($data[$q]['q']) ? (int)$data[$q]['q'] : null;
        $price = isset($data[$q]['p']) ? (float)$data[$q]['p'] : null;
        if ($quantity !== null && $price !== null) {
            $amount = $quantity * $price;
            $total += $amount;
        } else {
            $amount = null;
        }

        $row[] = array('value' => $quantity ?: '', 'style' => 'quantity'); // quarter quantity
        $row[] = array('value' => $price ?: '', 'style' => 'money'); // quarter price
        $row[] = array('value' => $amount ?: '', 'style' => 'money'); // quarter amount
    }

    $row[] = array('value' => $total, 'style' => 'money_bold'); // total amount

    $rows[] = $row;
}

if ($rows) {
    $EG->writeRows(0, ROW_TOP, $rows);
    $EG->writeBorders(0, ROW_TOP, COL_RIGHT, ROW_TOP + count($rows) - 1);
}

$doc = md5(Utils::uniqid());
$destFilename = DOC_DIR . $doc . '.xls';

$EG->save($destFilename);
$EG->close();

$title = $docTitle . '_' . date('Y_m_d') . '.xls';

if (!$DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
    throw new Exception('Error creating document at DB');
}

$res['doc'] = $doc;
