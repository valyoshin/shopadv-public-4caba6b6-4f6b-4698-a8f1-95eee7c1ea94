<?php

// Action: client_doc_add
// Input:
//    client: int - id of the client
//    fdata - file data
// Output:
//    doc: str(32)
//    

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

include('doc_add.php');

if (!$DB->clientDocAdd($clientId, $res['doc'])) {
    throw new Exception('Error while adding. Probably, value has already exist.', ERR_ADD);
}