<?php

// Action: connect
// Input:
//    xid - XID of the database (*)
//    key - licence key for the database (*)
//    ip - IP address of the client (system)
//    login - user's login for connect\auth (*)
//    password - user's password for connect\auth (*)
// Output:
//    config: array
//        title - organization title
//    user: array
//        name - user name
//        login - user login
//        role - user role
//        email - user e-mail
//    sid - session hash ID
//    session - session ID
//    period_start - period in format YYYY-MM as a start month

defined('IN_SITE') or die();

$xid = $Input->getParam('xid', true);
if (!$xid) {
    throw new Exception('<xid> is required', ERR_PARAM_MISSING);
}

$key = $Input->getParam('key', true);
if (!$key) {
    throw new Exception('<key> is required', ERR_PARAM_MISSING);
}

$login = $Input->getParam('login', true);
if (!$login) {
    throw new Exception('<login> is required', ERR_PARAM_MISSING);
}

$password = $Input->getParam('password', true);
if (!$password) {
    throw new Exception('<password> is required', ERR_PARAM_MISSING);
}

// TEST XID

$xidData = $DB->testXID($xid);
if (!$xidData || $xidData['key'] != $key) {
    throw new Exception('XID wrong', ERR_XID_WRONG);
}

$xid = $xidData['xid'];
$country = $xidData['country'];

$DB->XID = $xid;
$DB->Country = $country;

// TEST USER

$user = $DB->getUserAuth($login, Utils::encode($password, MD5_SALT));
if($user) {
    $res['user'] = $user;
} else {
    throw new Exception('Auth fail (63)', ERR_AUTH);
}

$config = array(
    'title' => $xidData['title'],
    'country' => $country,
    'period_start' => date('Y-m')
);
$res['config'] = $config;

$sid = Utils::encode(Utils::uniqid(), MD5_SALT);
$session = $DB->sessionAdd($sid, $user['login'], $ip, $xid);

if($session) {
    $res['session'] = $session;
    $res['sid'] = $sid;
} else {
    throw new Exception('System error', ERR_SYSTEM);
}

// SET SESSION

$_SESSION[$sid][$ip] = array(
    'session' => $session,
    'xid' => $xid,
    'config'  => $config,
    'user'    => $user
);
       
