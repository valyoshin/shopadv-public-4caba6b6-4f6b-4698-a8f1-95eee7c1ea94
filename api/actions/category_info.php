<?php

// Action: category_info
// Input:
//    category
// Output:
//    category: array
//        category
//        category_group
//        title
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
  throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$category = $Input->getParam('category', true);
if (!$category) {
  throw new Exception('<category> is required', ERR_PARAM_MISSING);
}

$res['category'] = $DB->categoryInfo($category);
