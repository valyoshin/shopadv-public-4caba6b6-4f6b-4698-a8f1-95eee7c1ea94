<?php

// Action: client_update
// Input:
//    client
//    title
//    code
//    jur_name
//    email 
//    phone
//    contact_name
//    address
//    inn
//    state
// Output:
//    client: int
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}
$data = $Input->getParamDataArr(array('title', 'code', 'jur_name', 'email', 'phone', 'contact_name', 'address', 'inn', 'state', 'use_in_list'));

if ($DB->clientUpdate($clientId, $data)) {
    $res['client'] = $clientId;
} else {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}
