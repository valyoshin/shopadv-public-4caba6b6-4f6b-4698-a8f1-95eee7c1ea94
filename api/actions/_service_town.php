<?php

function prepareServiceTown($st) {
    $r = array();
    foreach($st as $row) {
        $r[ $row['town'] ] = array(
          'p' => (float) $row['price'],
        );
    }
    return $r;
}

function processServiceTown($Input, &$ST) {
    if ($ST) {
        foreach ($ST as $key => $row) {
            $ST[$key]['p'] = $Input->getParamCustom('price', $row['p']);
        }
    }
}
