<?php
// Action: budget_plan_import
// Input:
//    budget_plan: int (M)
//    year: int (M)
//    service_type: (O)
//    qAmounts: array (M)
// Output:
//    budget_plan: int
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$qAmounts = $Input->getParam('qAmounts', true);
if (count($qAmounts) !== 4) {
    throw new Exception('<qAmounts> is required as array', ERR_PARAM_MISSING);
}

$budgetPlanId = $Input->getParam('budget_plan', true);
if (!$budgetPlanId) {
    throw new Exception('<budget_plan> is required as array', ERR_PARAM_MISSING);
}

$budgetPlanInfo = $DB->budgetPlan($budgetPlanId);
if (!$budgetPlanInfo) {
    throw new Exception('Not found', ERR_OBJ_NOT_FOUND);
}

$serviceType = $Input->getParam('service_type', true);

$data = array();

$DB->startTransaction();

$DB->budgetPlanDataClear($budgetPlanId);

// Make stat!

$periodStart = $year.'-01-01';
$periodEnd = $year.'-12-31';

$stat = array();

$quarter[0] = $DB->taskStat($year.'-01-01', $year.'-03-31', null, null, $serviceType);
$quarter[1] = $DB->taskStat($year.'-04-01', $year.'-06-30', null, null, $serviceType);
$quarter[2] = $DB->taskStat($year.'-07-01', $year.'-09-30', null, null, $serviceType);
$quarter[3] = $DB->taskStat($year.'-10-01', $year.'-12-31', null, null, $serviceType);

$totalsByQuarter = array();

foreach ($quarter as $index => $rows) {
    $totalsByQuarter[$index] = 0.0;
    if ($rows) {
        foreach ($rows as $row) {
            $service = (int)$row['service'];
            $client = (int)$row['client'];
            $q = (int)$row['total_quantity'];
            $a = (float)$row['total_amount'];

            if (!isset($stat[$service][$client])) {
                $stat[$service][$client] = array(
                    array('q' => 0, 'a' => 0.0),
                    array('q' => 0, 'a' => 0.0),
                    array('q' => 0, 'a' => 0.0),
                    array('q' => 0, 'a' => 0.0),
                );
            }

            $stat[$service][$client][$index]['q'] += $q;
            $stat[$service][$client][$index]['a'] += $a;

            $totalsByQuarter[$index] += $a;
        }
    }
}

foreach ($stat as $service => $statService) {
    foreach ($statService as $client => $statServiceClient) {
        $data = array();

        foreach ($statServiceClient as $index => $statServiceClientQuarter) {
            $quantity = $statServiceClientQuarter['q'];
            $amount = $statServiceClientQuarter['a'];
            $price = $quantity > 0 ? round($amount / $quantity, 2) : 0.0;
            if ($totalsByQuarter[$index] > 0) {
                $price = round($price * $qAmounts[$index] / $totalsByQuarter[$index], 2);
            }

            $quarterData = array(
                'q' => $quantity,
                'p' => $price,
            );

            $data[$index] = $quarterData;
        }

        $rawData = json_encode($data);

        if (!$DB->budgetPlanDataAdd($budgetPlanId, $service, $client, $rawData)) {
            $DB->rollback();
            throw new Exception('Can not add new budget plan data row', ERR_ADD);
        }
    }
}

$DB->commit();

$res['budget_plan'] = array(
    'budget_plan' => $budgetPlanId,
    'year' => (int) $budgetPlanInfo['year'],
    'title' => $budgetPlanInfo['title']
);
