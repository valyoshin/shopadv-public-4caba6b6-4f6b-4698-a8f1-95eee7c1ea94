<?php
// Action: client_add
// Input:
//    title
//    code
//    jur_name
//    email 
//    phone
//    contact_name
//    address
//    inn
//    state: bool
// Output:
//    client: int
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$data = $Input->getParamValidArr(array('title', 'code', 'jur_name', 'email', 'phone', 'contact_name', 'address', 'inn', 'state', 'use_in_list'));
$client = $DB->clientAdd($data);

if ($client) {
    $res['client'] = $client;
} else {
    throw new Exception('Error while adding. Probably, value has already exist.', ERR_ADD);
}
