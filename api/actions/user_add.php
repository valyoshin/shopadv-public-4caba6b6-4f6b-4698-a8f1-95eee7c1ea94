<?php

// Action: user_add
// Input:
//    login
//    name
//    email
//    state
//    password
//    role
// Output:
//    login
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$data = $Input->getParamValidArr(array('login', 'name', 'email', 'state', 'role', 'password'));
if(!isset($data['login']) || !$data['login']) {
    throw new Exception('<login> is required', ERR_PARAM_MISSING);
}
if(isset($data['password'])) {
    $data['password_hash'] = Utils::encode($data['password'], MD5_SALT);
    unset($data['password']);
}

if($DB->userAdd($data)) {
    $res['login'] = $data['login'];
} else {
    throw new Exception('Error while adding. Probably, value has already exist.', ERR_ADD);
}
