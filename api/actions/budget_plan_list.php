<?php
// Action: budget_plan_list
// Input:
//    year (M) - year of the budget
// Output:
//    budget_plan_list: array of object
//      budget_plan: int
//      year: int
//      title: string
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$year = $Input->getParam('year', true);
if (!$year) {
    throw new Exception('<year> is required', ERR_PARAM_MISSING);
}

$rows = $DB->budgetPlanList($year);
$res['budget_plan_list'] = array();
if ($rows) {
    foreach ($rows as $row) {
        $res['budget_plan_list'][] = array(
            'budget_plan' => (int) $row['budget_plan'],
            'year' => (int) $row['year'],
            'title' => $row['title']
        );
    }
}