<?php

// Action: client_dialog_delete
// Input:
//    client_dialog: int
// Output:
//    none
//

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientDialogId = $Input->getParam('client_dialog', true);
if(!$clientDialogId) {
    throw new Exception('<client_dialog> is required', ERR_PARAM_MISSING);
}

$clientDialogInfo = $DB->clientDialog($clientDialogId);
if(!$clientDialogInfo) {
    throw new Exception('Not found.', ERR_OBJ_NOT_FOUND);
}

if(!$DB->clientDialogDelete($clientDialogId)) {
    throw new Exception('Error while deleting. Probably, some record could not to be deleted.', ERR_DELETE);
}

