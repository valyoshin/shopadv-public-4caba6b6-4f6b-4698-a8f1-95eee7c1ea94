<?php

// Action: category_update
// Input:
//    category
//    category_group
//    title
// Output:
//    category: int
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
  throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$category = $Input->getParam('category', true);
if (!$category) {
  throw new Exception('<category> is required', ERR_PARAM_MISSING);
}
$data = $Input->getParamDataArr(array('title', 'category_group'));

if ($DB->categoryUpdate($category, $data)) {
  $res['category'] = $category;
} else {
  throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}
