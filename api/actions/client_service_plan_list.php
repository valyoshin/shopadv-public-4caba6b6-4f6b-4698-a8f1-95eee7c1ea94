<?php
// Action: client_service_plan_list
// Input:
//    client: int (M) - client ID
// Output:
//    client_service_plan_list: array of object
//      client_service_plan: int
//      client: int
//      service: int
//      active: bool
//      description: string
//      created: datetime
//      quantity: int|null
//      period_count: int|null
//      price: float|null
//      amount: float
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$rows = $DB->clientServicePlanList($clientId);
$res['client_service_plan_list'] = array();
if ($rows) {
    foreach ($rows as $row) {
        $row['client_service_plan'] = (int) $row['client_service_plan'];
        $row['client'] = (int) $row['client'];
        $row['service'] = (int) $row['service'];
        $row['active'] = (bool) $row['active'];
        $row['removed'] = (bool) $row['removed'];

        $row['period_count'] = is_numeric($row['period_count']) ? (int) $row['period_count'] : null;
        $row['quantity'] = is_numeric($row['quantity']) ? (int) $row['quantity'] : null;
        $row['price'] = is_numeric($row['price']) ? (float) $row['price'] : null;
        $row['amount'] = (float) $row['amount'];
        $row['total'] = $row['amount'] * $row['period_count'];

        $res['client_service_plan_list'][] = $row;
    }
}