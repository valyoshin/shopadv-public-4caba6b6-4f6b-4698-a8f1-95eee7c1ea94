<?php

// Action: shops_cache
// Input:
//    none
// Output:
//    shop_list: array (sorted by title)
//        shop
//        town
//        title
//        district
//        address
//        date_open
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}


$res['shop_list'] = $DB->shopListCache();

if(!$res['shop_list']) {
    $res['shop_list'] = array();
}
