<?php

// Action: _report_charge_client SUB template
// Input:
//    services 
//    period_s
//    period_e
//    client
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

$template = 'charge_client';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

// COLS
//    'client_title'
//    'task_title'
//    'period_0' ... period_N-1
//    'total'

define('ROW_TOP', 3);
define('COL_START', 3);
define('COL_TOTAL', 15);

$EG = new ExcelGeneratorLines(TEMP_DIR);

$periodCount = Period::between($periodStart, $periodEnd);

if ($periodCount > PERIOD_COUNT) {
    $periodCount = PERIOD_COUNT;
}

$lastCol = COL_START + PERIOD_COUNT;

function writeClientTitle($row, $title) {
    global $EG;
    global $lastCol;

    $T[0] = array('value' => $title, 'style' => 'yellow');
    for ($i = 1; $i <= $lastCol; $i++) {
        $T[$i] = array('value' => '', 'style' => 'yellow');
    }
    $EG->writeRow(0, $row, $T);
}

function writeMonths($periodStart, $periodCount) {
    global $EG;
    global $LANG;

    $M = array();
    for ($i = 0; $i < $periodCount; $i++) {
        $p = Period::incMonth($periodStart, $i);
        $pd = Period::decode($p);
        $M[] = array('value' => $LANG['month_names'][$pd['m'] - 1] . ' ' . $pd['y'], 'style' => 'bold');
    }

    $EG->writeRow(COL_START, ROW_TOP - 1, $M);
}

$Totals = array();

function totalsClear($periodCount) {
    global $Totals;

    for ($i = 0; $i < $periodCount; $i++) {
        $Totals[$i] = 0;
    }
}

function totalsAdd($index, $value) {
    global $Totals;

    $Totals[$index] += $value;
}

function totalsGet($index) {
    global $Totals;

    return $Totals[$index];
}

function totalsTotal($periodCount) {
    global $Totals;

    $t = 0;
    for ($i = 0; $i < $periodCount; $i++) {
        $t += $Totals[$i];
    }

    return $t;
}

function writeTotals(&$row, $periodCount) {
    global $EG;
    
    $R = array();
    $R[] = array('value' => 'общая стоимость за период', 'style' => 'bold');
    for ($i = 0; $i < $periodCount; $i++) {
        $R[] = array('value' => totalsGet($i), 'style' => 'total');
    }
    
    $EG->writeRow(COL_START - 1, $row, $R);
    
    $totalCell = array('value' => totalsTotal($periodCount), 'style' => 'total');
    $EG->writeRow(COL_TOTAL, $row, array($totalCell));
    
    $row++;
}

try {

    $EG->open($templateFilename);

    // styles
    $EG->addStyle('bold', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11,
            'bold' => true
        )
    ));
    $EG->addStyle('money', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11,
            'bold' => false
        ),
        'numberformat' => array(
            'code' => '0.00'
        )
    ));
    $EG->addStyle('total', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11,
            'bold' => true
        ),
        'numberformat' => array(
            'code' => '0.00'
        )
    ));
    $EG->addStyle('yellow', array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array(
                'rgb' => 'FFFF00'
            )
        )
    ));

    $row = ROW_TOP;

    writeMonths($periodStart, $periodCount);

    if ($client) {
        $clients = array($client);
    } else {
        $clients = null;
    }
    
    $tasks = $DB->clientServiceList($clients, $services, $periodStart, $periodCount, false, $serviceType);

    if ($tasks) {

        $client = null;

        foreach ($tasks as $task) {

            if ($task['payment']) {
                $payment = (float) $task['payment'];
            } else {
                $payment = false;
            }
            
            if ($client != $task['client']) {

                if ($client != null) {
                    writeTotals($row, $periodCount);
                }

                $client = $task['client'];
                writeClientTitle($row, $task['client_title']);
                $row++;
                totalsClear($periodCount);
            }

            $R = array();
            $R[] = $task['client_title'];
            $R[] = $task['service_title'];
            $R[] = $task['title'];

            $totalTask = 0;
            for ($i = 0; $i < $periodCount; $i++) {
                $amount = (float) $task['period_' . $i];
                if ($amount > 0) {
                    $p = $payment ? $payment : $amount;
                    $R[] = array('value' => $p, 'style' => 'money');
                    $totalTask += $p;
                    totalsAdd($i, $p);
                } else {
                    $R[] = '';
                }
            }
            
            $EG->writeRow(0, $row, $R);
            
            $totalCell = array('value' => $totalTask, 'style' => 'total');
            $EG->writeRow(COL_TOTAL, $row, array($totalCell));
            
            $row++;
        } // foreach tasks
        
        writeTotals($row, $periodCount);
        
    } // if tasks

    // make borders
    $EG->writeBorders(0, ROW_TOP, $lastCol, $row - 1);

    $doc = md5(Utils::uniqid());
    $destFilename = DOC_DIR . $doc . '.xls';

    $EG->save($destFilename);
    $EG->close();
    
    $title = 'НК_XXX_' . date('Y_m_d') . '.xls';
    
    if (! $DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
        throw new Exception('Error creating document at DB');
    }
    
} catch (Exception $ex) {
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
