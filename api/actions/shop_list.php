<?php

// Action: shop_list
// Return shop list
// Input:
//    shop
//    town
//    state
//    page - page to view
//    count - max count of rows to return
//    sort_col - column index to sort
//    sort_dir - ASC | DESC
// Output:
//    shop_list: array (sorted by shop)
//        shop - shop id
//        town
//        town_title
//        address
//        district
//        date_open
//        state
//    rstart
//    rcount
//    count
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$filter = $Input->getParamDataArr(array('shop', 'town', 'state'));
$page = $Input->getParam('page');
$count = $Input->getParam('count');
if(!$count || $count > MAX_ROWS) {
    $count = MAX_ROWS;
}
$start = ($page - 1) * $count;
$sort_col = $Input->getParam('sort_col');
if(!$sort_col) {
    $sort_col = 'shop';
}
$sort_dir = $Input->getParam('sort_dir');

$arr = $DB->shopListFilter($filter, $start, $count, $sort_col, $sort_dir);
$res['shop_list'] = array();
if ($arr) {
    foreach ($arr as $row) {
        $row['state'] = (bool) $row['state'];
        $res['shop_list'][] = $row;
    }
    $res['count'] = $DB->foundRows();
} else {
    $res['count'] = 0;
}

$res['rstart'] = $start;
$res['rcount'] = $count;
