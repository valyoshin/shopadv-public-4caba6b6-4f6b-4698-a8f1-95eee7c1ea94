<?php

// Action: client_task_history
// Input:
//     client: int
//     period_start: datetime
//     period_end: datetime
//     service_type: string (O)
// Output:
//     client_task_history: array of
//         period: string|date
//         task: int
//         service: int
//         client: int
//         title: string
//         period_start: string|date
//         period_end: string|date
//         description: string
//         quantity: int
//         price: float|null
//         amount: float
//         payment: float|null
//         task_period_check: bool
//

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$periodStart = $Input->getParam('period_start', true);
if (!$periodStart) {
    throw new Exception('<period_start> is required', ERR_PARAM_MISSING);
}

$periodEnd = $Input->getParam('period_end', true);
if (!$periodEnd) {
    throw new Exception('<period_end> is required', ERR_PARAM_MISSING);
}

$serviceType = $Input->getParam('service_type', true);

$data = $DB->taskHistory($periodStart, $periodEnd, $clientId, null, $serviceType);

$res['client_task_history'] = array();
if ($data) {
    foreach ($data as $row) {
        $row['task'] = (int) $row['task'];
        $row['service'] = (int) $row['service'];
        $row['client'] = (int) $row['client'];
        $row['quantity'] = (int) $row['quantity'];
        $row['price'] = is_numeric($row['price']) ? (float) $row['price'] : null;
        $row['amount'] = (float) $row['amount'];
        $row['payment'] = is_numeric($row['payment']) ? (float) $row['payment'] : null;
        $row['task_period_check'] = (bool) $row['task_period_check'];

        $res['client_task_history'][] = $row;
    }
}