<?php

// Action: services_cache
// Return service list (only active) for cache
// Input:
//    none
// Output:
//    service_list: array (sorted by service ID)
//        service : int - service id
//        code : int
//        title : str - service title
//        period_type : D,W,M,Q
//        service_type : M,A,C
//        min_period_count : int
//        price : currency
//        price_alcohol : currency
//        have_alcohol : true | false
//        have_payment : true | false
//        have_fine : true | false
//        have_shop_list : true | false
//        have_plan_list : true | false
//        have_maket : true | false
//        have_days : true | false
//        have_doc : true | false
//        have_description : true | false
//        town_price : array indexed by town ID, value - price for town
//        shop_access : true | false
//        copy_access : true | false
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$arr = $DB->serviceListCache();
$res['service_list'] = array();
if($arr) {
    foreach($arr as $row) {
        $row['service'] = (int) $row['service'];
        $row['code'] = (int) $row['code'];
        $row['min_period_count'] = (int) $row['min_period_count'];

        $row['state'] = (bool) $row['state'];
        $row['have_alcohol'] = (bool) $row['have_alcohol'];
        $row['have_shop_list'] = (bool) $row['have_shop_list'];
        $row['have_plan_list'] = (bool) $row['have_plan_list'];
        $row['have_maket'] = (bool) $row['have_maket'];
        $row['have_doc'] = (bool) $row['have_doc'];
        $row['have_description'] = (bool) $row['have_description'];
        $row['have_payment'] = (bool) $row['have_payment'];
        $row['have_fine'] = (bool) $row['have_fine'];
        $row['have_days'] = (bool) $row['have_days'];
        $row['shop_access'] = (bool) $row['shop_access'];
        $row['copy_access'] = (bool) $row['copy_access'];

        // rebuild town_price
        $tp = array();
        if($row['town_price']) {
            $sa = explode(';', $row['town_price']);
            foreach($sa as $s) {
                $sb = explode(':', $s);
                if(count($sb) == 2) {
                    $tp[ (int) $sb[0] ] = (float) $sb[1];
                }
            }
            $row['town_price'] = $tp;
        } else {
            $row['town_price'] = array();
        }

        $res['service_list'][] = $row;
    }
}

