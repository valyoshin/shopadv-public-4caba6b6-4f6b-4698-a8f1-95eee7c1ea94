<?php
//$shopList = $DB->shopList();
//$serviceList = $DB->serviceShortList();

$shop = $Input->getParam('shop', true);
$service = $Input->getParam('service', true);
$period = $Input->getParam('period', true);

if(!$period) {
    $period = date('Y-m-01');
    $Input->setParam('period', $period);
}

$xid = $DB->XID;

$res = array();
if ($shop) {
    if (isset($_GET['report_shop_task'])) {
        define('MAX_ROWS_SKIP', true);
        include('report_shop_task.php');
    } elseif (isset($_GET['report_promo_make'])) {
        include('report_promo_make.php');
        header('Location: doc/' . $res['doc'] . '.xls');
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js not-ie"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ShopADV: shop report</title>
        <meta name="keywords" content="shopadv">
        <meta name="description" content="shopadv">
        <meta name="robots" content="noindex,nofollow">
        <style>
            html,
            body {
                margin: 0;
                padding: 0;
                color: #111;
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size: 14px;
                line-height: 1.42857;
            }
            select,option,input {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size: 14px;
                line-height: 1.42857;
                color: #111;
                min-width: 100px;
            }
            h2 {
                font-size: 18px;
                line-height: 30px;
            }
            #wrap {
                width: 70%;
                padding: 10px 10px;
                margin: 0 auto;
            }
            #filter {
                margin-bottom: 10px;
            }
            #report table {
                width: 100%;
                margin: 0;
                padding: 0;
            }
            #report table tr.odd {
                background-color: #eee;
            }
            #report table tr.header {
                background-color: #ddd;
                text-align: center;
                font-weight: bold;
            }
            #report table td {
                padding: 1px 4px;
            }

            @media print {
                #filter {
                    display: none;
                }
            }

        </style>
    </head>
    <body>
        <div id="wrap">
            <div id="filter">
                <form action="" method="GET">
                    <input type="hidden" name="xid" value="<?php echo $xid; ?>"/>
                    Магазин: <input name="shop" type="text" value="<?php echo $shop ? $shop : ''; ?>" autofocus="true">

<?php //                    Магазин: <select name="shop">
      //                  <option value="0">-- выберите магазин --</option> ?>
                        <?php
//                        if (is_array($shopList)) {
//                            foreach ($shopList as $shopInfo) {
//                                $s = ($shop && $shop == $shopInfo['shop']) ? ' selected="selected"' : '';
//                                echo '<option value="' . $shopInfo['shop'] . '"' . $s . '>' . $shopInfo['shop'] . '</option>';
//                            }
//                        }
                        ?>

<?php //                    </select> ?>
<?php //                    Услуга: <select name="service"><option value="0">-- все --</option> ?>
                        <?php
//                        if (is_array($serviceList)) {
//                            foreach ($serviceList as $serviceInfo) {
//                                if ($serviceInfo['shop_access'] == 'Y') {
//                                    $s = ($service && $service == $serviceInfo['service']) ? ' selected="selected"' : '';
//                                    echo '<option value="' . $serviceInfo['service'] . '"' . $s . '>' . $serviceInfo['title'] . '</option>';
//                                }
//                            }
//                        }
                        ?>
<?php //                    </select> ?>
<?php //                    Период: <select name="period"> ?>
                        <?php
//                        $ps = date('Y-m-01');
//                        for ($i = -PERIOD_COUNT; $i < PERIOD_COUNT; $i++) {
//                            $p = Period::incMonth($ps, $i);
//                            $d = Period::decode($p);
//                            $title = $LANG['month_names'][$d['m'] - 1] . ' ' . $d['y'];
//
//                            $s = ($period && $period == $p) ? ' selected="selected"' : '';
//                            echo '<option value="' . $p . '"' . $s . '>' . $title . '</option>';
//                        }
                        ?>
<?php //                    </select> ?>
                    <input type="submit" name="report_shop_task" value="Отчёт">
                    <input type="submit" name="report_promo_make" value="График промо">
                    <a href="#" onclick="window.print();">ПЕЧАТЬ</a>
                </form>
            </div>

            <div id="report">
                <h2>Актуальные рекламные материалы поставщиков</h2>
<?php
if (isset($res['report_shop_task_list'])) {
?>
                <h3>Магазин №<?php echo $shop; ?> <?php echo date('d.m.Y'); ?></h3>
                <table border="1" cellpadding="0" cellspacing="0">
                    <tr class="header">
                        <td>№п/п</td>
                        <td>Наименование рекламного материала</td>
                        <td>ТМ</td>
                        <td>Макет</td>
                        <td>Дата начала</td>
                        <td>Дата окончания</td>
                    </tr>
<?php
    $i = 0;
    foreach ($res['report_shop_task_list'] as $row) {
        $row['period_start'] = Period::dateAsWin($row['period_start']);
        $row['period_end'] = Period::dateAsWin($row['period_end']);

        if ($row['maket']) {
            $row['maket'] = '<a href="maket/'.$row['maket'].'.jpg" target="_blank"><img height="120" src="maket/'.$row['maket'].'.jpg" alt="'.$row['maket'].'"></a>';
        } else {
            $row['maket'] = '&nbsp;';
        }

        $trclass = ($i % 2) ? 'even' : 'odd';

        $serviceTitle = $row['service_title'];
        if ($row['description']) {
            $serviceTitle .= ' / ' . $row['description'];
        }

        echo '<tr class="'.$trclass.'">';
        echo '<td>' . ($i+1) . '</td>';
        echo '<td>' . $serviceTitle . '</td>';
        echo '<td>' . $row['title'] . '</td>';
        echo '<td>' . $row['maket'] . '</td>';
        echo '<td>' . $row['period_start'] . '</td>';
        echo '<td>' . $row['period_end'] . '</td>';
        echo '</tr>';

        $i++;
    }
?>
                </table>
<?php
}
?>
            </div>
        </div>
    </body>
</html>
