<?php
// Action: client_dialog_add
// Input:
//    client: int (M)
//    description: string
//    active: int|bool (0|1)
// Output:
//      client_dialog: int
//      client: int
//      active: bool
//      description: string
//      created: datetime
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$data = $Input->getParamValidArr(array('active', 'description'));
$data['client'] = $clientId;
$data['created'] = date('Y-m-d H:i:s');

$clientDialogId = $DB->clientDialogAdd($data);

if ($clientDialogId) {
    $res['client_dialog'] = array(
        'client_dialog' => $clientDialogId,
        'client' => $clientId,
        'active' => (bool) $data['active'],
        'description' => $data['description'],
        'created' => $data['created'],
    );
} else {
    throw new Exception('Error while adding.', ERR_ADD);
}
