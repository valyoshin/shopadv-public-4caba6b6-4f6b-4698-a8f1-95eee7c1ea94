<?php

// Action: category_groups_cache
// Input:
//    none
// Output:
//    category_group_list: array (sorted by title)
//        category_group
//        title
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$res['category_group_list'] = $DB->categoryGroupListCache();

if(!$res['category_group_list']) {
    $res['category_group_list'] = array();
}
