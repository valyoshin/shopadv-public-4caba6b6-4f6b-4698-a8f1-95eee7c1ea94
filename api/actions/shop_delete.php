<?php

// Action: shop_delete
// Input:
//    shop
// Output:
//    none
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$shop = $Input->getParam('shop', true);
if(!$shop) {
    throw new Exception('<shop> is required', ERR_PARAM_MISSING);
}

if(!$DB->shopDelete($shop)) {
    throw new Exception('Error while deleting. Probably, some record could not to be deleted.', ERR_DELETE);
}

