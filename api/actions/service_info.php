<?php

// Action: service_info
// Input:
//    service
// Output:
//    service: array 
//        service
//        code
//        title
//        title_buh,
//        description,
//        service_point,
//        price,
//        price_alcohol,
//        have_alcohol,
//        have_plan_list
//        have_shop_list
//        copy_access
//        have_payment
//        have_fine
//        state
//        service_type
//        
//        
//    st: array indexed by town
//       p - price
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$service = $Input->getParam('service', true);
if(!$service) {
    throw new Exception('<service> is required', ERR_PARAM_MISSING);
}

$res['service'] = $DB->serviceInfo($service);

if($res['service']) {
    $res['service']['have_alcohol'] = (bool) $res['service']['have_alcohol'];
    $res['service']['have_plan_list'] = (bool) $res['service']['have_plan_list'];
    $res['service']['have_shop_list'] = (bool) $res['service']['have_shop_list'];
    $res['service']['copy_access'] = (bool) $res['service']['copy_access'];
    $res['service']['have_payment'] = (bool) $res['service']['have_payment'];
    $res['service']['have_fine'] = (bool) $res['service']['have_fine'];
    $res['service']['state'] = (bool) $res['service']['state'];
    $st = $DB->serviceTownInfo($service);
    if(!$st) {
        $res['st'] = array();
    } else {
        require_once '_service_town.php';
        $res['st'] = prepareServiceTown($st);
    }
}
        