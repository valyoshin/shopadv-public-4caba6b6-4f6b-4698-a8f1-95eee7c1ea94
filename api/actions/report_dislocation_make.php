<?php

// Action: report_dislocation_make
// Input:
//    services 
//    period
//    full
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

require_once(API_DIR . 'classes/ExcelGeneratorLines.php');

$services = $Input->getParam('services', true);
if (!$services || !count($services)) {
    throw new Exception('<services> is required', ERR_PARAM_MISSING);
}

$periodStart = $Input->getParam('period', true);
if (!$periodStart) {
    throw new Exception('<period> is required', ERR_PARAM_MISSING);
}

$full = $Input->getParam('full');

$template = 'base';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

define('COL_TOP', 3);
define('COL_SHOP', 0);
define('ROW_TOP', 1);
define('ROW_SHOP', 4);

$EG = new ExcelGeneratorLines(TEMP_DIR);

try {

    $EG->open($templateFilename);

    $col = COL_TOP;
    $V = array();
    $j = COL_TOP;
    foreach($services as $service) {
        $serviceTitle = $DB->serviceTitle($service);
        
        $b = false;
        $row1 = array(); // periods
        $row2 = array(); // tm's
        $row3 = array(); // supplier
        for($i = 0; $i < PERIOD_COUNT; $i++) {
            $p = Period::incMonth($periodStart, $i);
            $tasks = $DB->taskListByServicePeriod($service, $p);
            if($tasks) {
                foreach($tasks as $task) {
                    $row1[] = Period::periodAsWin($p);
                    $row2[] = $task['title'];
                    $row3[] = $task['client_title'];
                    $V[$j] = explode(',', $task['shops']);
                    $j++;
                }
                $b = true;
            }
        } // period
        if($b) {
            $EG->writeRow($col, ROW_TOP, array($serviceTitle));
            $EG->writeRow($col, ROW_TOP + 1, $row1);
            $EG->writeRow($col, ROW_TOP + 2, $row2);
            $EG->writeRow($col, ROW_TOP + 3, $row3);
            $col += count($row1);
        }
    } // services
    // shops
    $shops = $DB->shopList();
    if(empty($shops)) {
        throw new Exception('No shops', ERR_SYSTEM);
    }

    $i = 1;
    $r = 0;
    foreach($shops as $shop) {
        $row = array();
        $row[] = $shop['shop'];
        $row[] = $shop['town_title'];
        $row[] = $shop['address'];
        $b = $full;
        for($c = COL_TOP; $c < $col; $c++) {
            if(isset($V[$c]) && in_array($shop['shop'], $V[$c])) {
                $row[] = 1;
                $b = true;
            } else {
                $row[] = '';
            }
        }
        if($b) {
            $EG->writeRow(COL_SHOP, ROW_SHOP + $i, $row);
            $r++;
            $i++;
        }
    } 
    
    // make borders
    $EG->writeBorders(COL_SHOP, ROW_TOP, $col - 1, ROW_SHOP + $r);
    
    $doc = md5(Utils::uniqid());
    $destFilename = DOC_DIR . $doc . '.xls';

    $EG->save($destFilename);
    $EG->close();
    
    $title = 'РД_XXX_' . date('Y_m_d') . '.xls';
    
    if (! $DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
        throw new Exception('Error creating document at DB');
    }
    
} catch (Exception $ex) {
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
