<?php

function prepareTaskShop($ts)
{
    $r = array();
    foreach($ts as $row) {
        $r[ $row['shop'] ] = array(
          'p' => (float) $row['price'],
          'q' => (int) $row['quantity'],
          'd' => $row['days'],
          'desc' => $row['description']
        );
    }

    return $r;
}

function processTaskShop($Input, &$TS, &$data)
{
    if ($TS) {
        $data['quantity'] = 0;
        $data['amount'] = 0;
        $price = false;
        foreach ($TS as $key => $row) {
            $TS[$key]['desc'] = $Input->getParamCustom('description', $row['desc']);
            $TS[$key]['p'] = $Input->getParamCustom('price', $row['p']);
            if ($price === false) {
                $price = $TS[$key]['p'];
            } elseif ($price != $TS[$key]['p']) {
                $price = null; // different prices
            }
            if (isset($row['d'])) {
                $TS[$key]['d'] = $Input->getParamCustom('days', $row['d']);
                if (is_array($TS[$key]['d'])) {
                    $TS[$key]['q'] = count($TS[$key]['d']);
                } else {
                    $TS[$key]['q'] = 0;
                }
            } else {
                $TS[$key]['d'] = null;
                $TS[$key]['q'] = $Input->getParamCustom('quantity', $row['q']);
            }
            $data['quantity'] += $TS[$key]['q'];
            $data['amount'] += $TS[$key]['p'] * $TS[$key]['q'];
        }
        if ($data['quantity'] > 0) {
            $data['price'] = $price;
        } else {
            $data['price'] = 0;
        }
    } elseif (isset($data['quantity']) && isset($data['price'])) {
        $data['amount'] = $data['quantity'] * $data['price'];
    }
}
