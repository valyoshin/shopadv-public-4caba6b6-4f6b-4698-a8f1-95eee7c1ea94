<?php

// Action: shop_town_list
// Return town list where exists if only one shop
// Input:
//    none  
// Output:
//    town_list: array (sorted by title)
//        town - town id
//        region - region id
//        title - town title
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$res['town_list'] = $DB->townListWithShop();
if(!$res['town_list']) {
    $res['town_list'] = array();
}
       
