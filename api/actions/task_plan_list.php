<?php

// Action: task_plan_list
// Input:
//    task
// Output:
//    task: array 
//        task - task id
//        service - service id
//        client - client id
//        maket - maket id
//        title
//        description
//        period_start
//        period_end
//        quantity
//        price
//        amount
//        payment
//        have_alcohol
//        color
//        doc_list
//    ts: array indexed by shop
//       q - quantity
//       p - price
//       d - days
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$task = $Input->getParam('task', true);
if(!$task) {
    throw new Exception('<task> is required', ERR_PARAM_MISSING);
}

$res['task_plan_list'] = $DB->taskPlanList($task);
