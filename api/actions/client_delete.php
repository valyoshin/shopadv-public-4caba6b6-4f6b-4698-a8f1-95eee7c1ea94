<?php

// Action: client_delete
// Input:
//    client
// Output:
//    none
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if(!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

if(!$DB->clientDelete($clientId)) {
    throw new Exception('Error while deleting. Probably, some record could not to be deleted.', ERR_DELETE);
}

