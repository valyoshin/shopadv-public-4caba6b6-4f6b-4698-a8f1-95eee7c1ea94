<?php
// Action: budget_plan_delete
// Input:
//    budget_plan: int (M) - id
// Output:
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$budgetPlanId = $Input->getParam('budget_plan', true);
if (!$budgetPlanId) {
    throw new Exception('<budget_plan> is required', ERR_PARAM_MISSING);
}

$budgetPlan = $DB->budgetPlan($budgetPlanId);
if (!$budgetPlan) {
    throw new Exception('budget plan is not found', ERR_OBJ_NOT_FOUND);
}

if (!$DB->budgetPlanDelete($budgetPlanId)) {
    throw new Exception('budget plan is not found', ERR_DELETE);
}
