<?php

// Action: user_update
// Input:
//    login
//    name
//    email
//    role
//    state
//    password
// Output:
//    login
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$login = $Input->getParam('login', true);
if(!$login) {
    throw new Exception('<login> is required', ERR_PARAM_MISSING);
}
$data = $Input->getParamDataArr(array('name', 'role', 'state', 'email', 'password'));
if(isset($data['password'])) {
    if($login != USER_SHOP && $data['password']) {
        $data['password_hash'] = Utils::encode($data['password'], MD5_SALT);
    }
    unset($data['password']);
}

if($DB->userUpdate($login, $data)) {
    $res['login'] = $login;
} else {
    throw new Exception('Error while updating. Probably, some value is incorrect.', ERR_UPDATE);
}
