<?php

$Loger->timestamp('open gen: '.$filename);

$json = file_get_contents($filename);

$data = json_decode($json, true);
if (!$data) {
  throw new \Exception('Bad format: not JSON');
}

$Loger->timestamp('open done');

if (empty($data['type']) || !isset($data['sale_boards'])) {
  throw new \Exception('Unsupported type of json file');
}

if ($data['type'] != 'report_sale_boards') {
  throw new \Exception('Unsupported type of json file: ' . $data['type']);
}

$saleBoards = $data['sale_boards'];

// processing

$Loger->debugStr('processing...');

$tasks = array();
$taskShops = array();

//{
//  "amount_cash":0,
//  "amount_bank":0,
//  "price_cash":0,
//  "price_bank":0,
//  "period":"2016-05-01",
//  "client_name":"\u0412\u0438\u0442\u043c\u0430\u0440\u043a-\u0423\u043a\u0440\u0430\u0438\u043d\u0430 \u0421\u041f \u041e\u041e\u041e (22480087)",
//  "operator":1,
//  "client_code":"22480087",
//  "sale_title":"Jaffa",
//  "i":1,
//  "operator_id":"C\u041f.4-6",
//  "zone":"4",
//  "side":"C\u041f-6",
//  "sale":11,
//  "client":5,
//  "board":2159,
//  "sale_days":"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31"
//}

foreach ($saleBoards as $dataRow) {
  $shopId = $dataRow['zone'];
  $shop = $shopId ? $DB->shopInfo($shopId) : null;
  if (!$shop) {
    $Loger->debugStr('Shop is not found: ' . $dataRow['zone']);
    continue;
  }

  $client = $DB->clientByCode($dataRow['client_code']);
  if (!$client) {
    $client = $DB->clientAdd(array(
      'title' => $dataRow['client_name'],
      'code'  => $dataRow['client_code'],
    ));
  }
  if (!$client) {
    $Loger->debugStr('Client is not found and created: ' . $dataRow['client_name'] . ' / ' . $dataRow['client_code']);
    continue;
  }

  $pd = Period::decode($dataRow['period']);
  $ps = Period::encode($pd['y'], $pd['m'], 1);
  $pe = Period::encode($pd['y'], $pd['m'], Period::daysInMonth($pd['m'], $pd['y']));

  $q = 1;
  $p = (float) $dataRow['amount_bank'];
  $amount = $p * $q;

  $key = $dataRow['sale'] . ':' . $dataRow['period'];

  if (!isset($tasks[$key])) {
    $tasks[$key] = array(
      'service' => $serviceInfo['service'],
      'client' => $client,
      'title' => $dataRow['sale_title'],
      'description' => $dataRow['sale'],
      'period_start' => $ps,
      'period_end' => $pe,
      'quantity' => $q,
      'price' => $p,
      'amount' => $amount,
      'service_type' => $serviceInfo['service_type'],
    );
  } else {
    $tasks[$key]['quantity'] += $q;
    $tasks[$key]['amount'] += $amount;
    if ($tasks[$key]['price'] != $p) {
      $tasks[$key]['price'] = null;
    }
  }

  if (isset($taskShops[$key][$shop['shop']])) {
      $totalQuantity = $taskShops[$key][$shop['shop']]['q'] + $q;
      $totalAmount = $taskShops[$key][$shop['shop']]['a'] + $amount;
      $newPrice = round($totalAmount / $totalQuantity, 2);

      $taskShops[$key][$shop['shop']]['d'] .= ', '.$dataRow['operator_id'];
      $taskShops[$key][$shop['shop']]['q'] = $totalQuantity;
      $taskShops[$key][$shop['shop']]['p'] = $newPrice;
      $taskShops[$key][$shop['shop']]['a'] = $totalAmount;
  } else {
      $taskShops[$key][$shop['shop']] = array(
          'd' => $dataRow['operator_id'],
          'q' => $q,
          'p' => $p,
          'a' => $amount
      );
  }
}

// saving

foreach ($tasks as $key => $taskData) {
  $task = $DB->taskAdd($taskData);
  if ($task) {
    $taskList[] = $task;
    $color = getNewColor($task);
    $DB->taskUpdate($task, array('color' => $color));

    foreach ($taskShops[$key] as $shopId => $taskShop) {
      $DB->taskShopAdd($task, $shopId, $taskShop['q'], $taskShop['p'], null, $taskShop['d']);
    }
  }
}

