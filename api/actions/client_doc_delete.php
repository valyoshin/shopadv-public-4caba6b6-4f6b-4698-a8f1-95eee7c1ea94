<?php

// Action: client_doc_delete
// Input:
//    client_doc: int
// Output:
//    none
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientDocId = $Input->getParam('client_doc', true);
if(!$clientDocId) {
    throw new Exception('<client_doc> is required', ERR_PARAM_MISSING);
}

$clientDocInfo = $DB->clientDocInfo($clientDocId);
if(!$clientDocInfo) {
    throw new Exception('Not found.', ERR_OBJ_NOT_FOUND);
}

if(!$DB->clientDocDelete($clientDocId)) {
    throw new Exception('Error while deleting. Probably, some record could not to be deleted.', ERR_DELETE);
}

