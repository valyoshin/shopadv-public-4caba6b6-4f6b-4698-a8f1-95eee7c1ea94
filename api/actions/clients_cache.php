<?php

// Action: clients_cache
// Input:
//    none
// Output:
//    client_list: array (sorted by title)
//        client
//        title
//        use_in_list
//        service_prices: array indexed by service id
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER, ROLE_SHOP))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$data = $DB->clientListCache();

$res['client_list'] = array();
if ($data) {
    foreach ($data as $row) {
        $row['client'] = (int) $row['client'];
        $row['use_in_list'] = (bool) $row['use_in_list'];
        $row['service_prices'] = array();

//        $clientServicePlanList = $DB->clientServicePlanList($row['client'], true);
//        if ($clientServicePlanList) {
//            foreach ($clientServicePlanList as $clientServicePlan) {
//                $service = (int) $clientServicePlan['service'];
//                $price = is_numeric($clientServicePlan['price']) ? (float) $clientServicePlan['price'] : null;
//                if ($price) {
//                    $row['service_prices'][$service] = array('price' => $price);
//                }
//            }
//        }

        $res['client_list'][] = $row;
    }
}
