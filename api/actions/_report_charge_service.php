<?php

// Action: _report_charge_service SUB template
// Input:
//    services 
//    period_s
//    period_e   
//    client 
// Output:
//    doc: str[32]
//    

defined('IN_SITE') or die();

$template = 'charge_service';
$templateFilename = TEMPLATE_DIR . $template . '.xls';
if (!$template || !file_exists($templateFilename)) {
    throw new Exception('<template> is wrong', ERR_PARAM_MISSING);
}

// COLS
//    'service_title'
//    'client_title'
//    'period_0' ... period_N-1
//    'total'

define('ROW_TOP', 3);
define('COL_START', 2);
define('COL_TOTAL', 14);

$EG = new ExcelGeneratorLines(TEMP_DIR);

$periodCount = Period::between($periodStart, $periodEnd);

if ($periodCount > PERIOD_COUNT) {
    $periodCount = PERIOD_COUNT;
}

$lastCol = 2 + PERIOD_COUNT;

function writeServiceTitle($row, $title) {
    global $EG;
    global $lastCol;
    
    $T[0] = array('value' => $title, 'style' => 'yellow');
    for ($i = 1; $i <= $lastCol; $i++) {
        $T[$i] = array('value' => '', 'style' => 'yellow');
    }
    $EG->writeRow(0, $row, $T);
}

function writeMonths($periodStart, $periodCount) {
    global $EG;
    global $LANG;
    
    $M = array();
    for ($i = 0; $i < $periodCount; $i++) {
        $p = Period::incMonth($periodStart, $i);
        $pd = Period::decode($p);
        $M[] = array('value' => $LANG['month_names'][$pd['m'] - 1] . ' ' . $pd['y'], 'style' => 'bold');
    }
    
    $EG->writeRow(2, ROW_TOP - 1, $M);
}

$Totals = array();
$allTotals = array();

function totalsClear($periodCount) {
    global $Totals;
    
    for ($i = 0; $i < $periodCount; $i++) {
        $Totals[$i] = 0;
    }
}

function allTotalsClear($periodCount) {
    global $allTotals;

    for ($i = 0; $i < $periodCount; $i++) {
        $allTotals[$i] = 0;
    }
}

function totalsAdd($index, $value) {
    global $Totals;
    global $allTotals;
    
    $Totals[$index] += $value;
    $allTotals[$index] += $value;
}

function totalsGet($index) {
    global $Totals;
    
    return $Totals[$index];
}

function allTotalsGet($index) {
    global $allTotals;

    return $allTotals[$index];
}

function totalsTotal($periodCount) {
    global $Totals;
    
    $t = 0;
    for ($i = 0; $i < $periodCount; $i++) {
        $t += $Totals[$i];
    }
    
    return $t;
}

function allTotalsTotal($periodCount) {
    global $allTotals;

    $t = 0;
    for ($i = 0; $i < $periodCount; $i++) {
        $t += $allTotals[$i];
    }

    return $t;
}

try {

    $EG->open($templateFilename);
    
    // styles
    $EG->addStyle('bold', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11,
            'bold' => true
        )
    ));
    $EG->addStyle('money', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11,
            'bold' => false
        ),
        'numberformat' => array(
            'code' => '0.00'
        )
    ));
    $EG->addStyle('total', array(
        'font' => array(
            'name' => 'Calibri',
            'size' => 11,
            'bold' => true
        ),
        'numberformat' => array(
            'code' => '0.00'
        )
    ));    
    $EG->addStyle('yellow', array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array(
                'rgb' => 'FFFF00'
            )
        )
    ));    
    
    $row = ROW_TOP;
    
    writeMonths($periodStart, $periodCount);

    allTotalsClear($periodCount);
    
    foreach ($services as $service) {
        $serviceInfo = $DB->serviceInfo($service);
        if(!$serviceInfo) {
            continue;
        }

        if ($serviceInfo['service_type'] === 'C') {
            $st = $serviceType;
        } else {
            $st = false;
        }
        
        writeServiceTitle($row, $serviceInfo['title']);
        $row++;
        
        totalsClear($periodCount);
        
        $clients = $DB->serviceClientList($periodStart, $periodCount, $service, $client, $st);
        
        if ($clients) {
            $RS = array();
            foreach ($clients as $clientData) {
                $R = array();
                
                $R[] = ''; // empty
                $R[] = $clientData['title'];
                
                $totalClient = 0;
                for ($i = 0; $i < $periodCount; $i++) {
                    $R[] = array('value' => $clientData['period_' . $i], 'style' => 'money');
                    totalsAdd($i, $clientData['period_' . $i]);
                    $totalClient += $clientData['period_' . $i];
                }
                for ($i = $periodCount; $i < PERIOD_COUNT; $i++) {
                    $R[] = '';
                }
                
                $R[] = array('value' => $totalClient, 'style' => 'total');
                
                $RS[] = $R;
            }
            // total row
            $R = array();
            $R[] = ''; 
            $R[] = array('value' => 'общая стоимость за период', 'style' => 'bold'); 
            for ($i = 0; $i < $periodCount; $i++) {
                $R[] = array('value' => totalsGet($i), 'style' => 'total');
            }
            for ($i = $periodCount; $i < PERIOD_COUNT; $i++) {
                $R[] = '';
            }
            
            $R[] = array('value' => totalsTotal($periodCount), 'style' => 'total');
            $RS[] = $R;
            
            // excel
            $EG->writeRows(0, $row, $RS);
            
            $row += count($RS);
        } // clients
    } // services

    // total row
    $R = array();
    $R[] = '';
    $R[] = array('value' => 'общая стоимость', 'style' => 'bold');
    for ($i = 0; $i < $periodCount; $i++) {
        $R[] = array('value' => allTotalsGet($i), 'style' => 'total');
    }
    for ($i = $periodCount; $i < PERIOD_COUNT; $i++) {
        $R[] = '';
    }
    $R[] = array('value' => allTotalsTotal($periodCount), 'style' => 'total');
    $EG->writeRow(0, $row, $R);
    $row++;

    // make borders
    $EG->writeBorders(0, ROW_TOP, $lastCol, $row - 1);

    $doc = md5(Utils::uniqid());
    $destFilename = DOC_DIR . $doc . '.xls';

    $EG->save($destFilename);
    $EG->close();
    
    $title = 'НУ_XXX_' . date('Y_m_d') . '.xls';
    
    if (! $DB->docAdd($doc, 'xls', Utils::asFilename($title))) {
        throw new Exception('Error creating document at DB');
    }
    
} catch (Exception $ex) {
    throw new Exception($ex->getMessage(), ERR_SYSTEM);
}

$res['doc'] = $doc;
