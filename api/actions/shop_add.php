<?php

// Action: shop_add
// Input:
//    shop
//    town
//    address
//    district
//    date_open 
//    state
// Output:
//    shop: int
//    

defined('IN_SITE') or die();

if(!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$data = $Input->getParamValidArr(array('shop', 'town', 'address', 'district', 'date_open', 'state'));

if (!isset($data['shop']) || !$data['shop']) {
    throw new Exception('<shop> is required', ERR_PARAM_MISSING);
}
if (!isset($data['town']) || !$data['town']) {
    throw new Exception('<town> is required', ERR_PARAM_MISSING);
}

$data['title'] = $data['shop'];

if( $DB->shopAdd($data) ) {
    $res['shop'] = $data['shop'];
} else {
    throw new Exception('Error while adding. Probably, value has already exist.', ERR_ADD);
}
