<?php

// Action: client_list
// Return client list
// Input:
//    client_title - string for search (by title "LIKE style")
//    code - int for search by code
//    jur_name - string for search (by jur_name "LIKE style")
//    page - page to view
//    count - max count of rows to return
//    service_type: string (O)
//    sort_col - column index to sort
//    sort_dir - ASC | DESC
// Output:
//    client_list: array (sorted by title)
//        client - client id
//        code - client's code
//        client_title - client title
//        jur_name
//        client_email
//        client_phone
//        contact_name
//        client_address
//        inn
//        state
//        plan_year_amount
//        plan_total_current_month_amount
//        total_period_charge
//        current_month_debt_amount
//        current_month_debt_percent
//    rstart
//    rcount
//    count
//    

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$filter = $Input->getParamDataArr(array('client_title', 'code', 'jur_name', 'state', 'current_month_debt_percent', 'use_in_list'));
$page = $Input->getParam('page');
$count = $Input->getParam('count');
if (!$count || $count > MAX_ROWS) {
    $count = MAX_ROWS;
}
$start = ($page - 1) * $count;
$sort_col = $Input->getParam('sort_col');
if (!$sort_col) {
    $sort_col = 'client_title';
}
$sort_dir = $Input->getParam('sort_dir');

$currentYear = (int) date('Y');
$currentMonth = (int) date('m');

if ($currentMonth > 1) {
    $currentDay = (int) date('d');
    if ($currentDay <= 15) {
        $currentMonth--; // prev month
        if ($currentMonth <= 0) {
            $currentMonth = 12;
            $currentYear--;
        }
    }

    $currentYearStr = str_pad($currentYear, 4, '0', STR_PAD_LEFT);
    $currentMonthStr = str_pad($currentMonth, 2, '0', STR_PAD_LEFT);

    $periodChargeStart = $currentYearStr . '-01-01';
    $periodChargeEnd = $currentYearStr . '-' . $currentMonthStr . '-01';
} else {
    $periodChargeStart = null;
    $periodChargeEnd = null;
}

$serviceType = $Input->getParam('service_type', true);

$data = $DB->clientList($filter, $start, $count, $sort_col, $sort_dir, $currentMonth, $periodChargeStart, $periodChargeEnd, $serviceType);

$res['client_list'] = array();
if ($data) {
    $res['count'] = $DB->foundRows();

    foreach ($data as $row) {
        $row['client'] = (int)$row['client'];
        $row['state'] = (bool)$row['state'];
        $row['use_in_list'] = (bool)$row['use_in_list'];

        if (is_numeric($row['total_period_charge'])) {
            $row['total_period_charge'] = (float)$row['total_period_charge'];
        } else {
            $row['total_period_charge'] = null;
        }

        if (is_numeric($row['plan_year_amount'])) {
            $row['plan_year_amount'] = (float)$row['plan_year_amount'];
        } else {
            $row['plan_year_amount'] = null;
        }

        if (is_numeric($row['plan_total_current_month_amount'])) {
            $row['plan_total_current_month_amount'] = (float)$row['plan_total_current_month_amount'];
        } else {
            $row['plan_total_current_month_amount'] = null;
        }

        if (is_numeric($row['current_month_debt_amount'])) {
            $row['current_month_debt_amount'] = (float)$row['current_month_debt_amount'];
        } else {
            $row['current_month_debt_amount'] = null;
        }

        if (is_numeric($row['current_month_debt_percent'])) {
            $row['current_month_debt_percent'] = (float)$row['current_month_debt_percent'];
        } else {
            $row['current_month_debt_percent'] = null;
        }

        $res['client_list'][] = $row;
    }
} else {
    $res['count'] = 0;
}

$res['rstart'] = $start;
$res['rcount'] = $count;