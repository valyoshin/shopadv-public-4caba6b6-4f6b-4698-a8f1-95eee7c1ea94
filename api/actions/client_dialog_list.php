<?php
// Action: client_dialog_list
// Input:
//    client: int (M) - client ID
// Output:
//    client_dialog_list: array of object
//      client_dialog: int
//      active: bool
//      description: string
//      created: datetime
//

defined('IN_SITE') or die();

if (!testRole(array(ROLE_ADMIN, ROLE_MANAGER))) {
    throw new Exception('Action is not allowed', ERR_USER_DENY);
}

$clientId = $Input->getParam('client', true);
if (!$clientId) {
    throw new Exception('<client> is required', ERR_PARAM_MISSING);
}

$rows = $DB->clientDialogList($clientId);
$res['client_dialog_list'] = array();
if ($rows) {
    foreach ($rows as $row) {
        $row['client_dialog'] = (int) $row['client_dialog'];
        $row['active'] = (bool) $row['active'];

        $res['client_dialog_list'][] = $row;
    }
}