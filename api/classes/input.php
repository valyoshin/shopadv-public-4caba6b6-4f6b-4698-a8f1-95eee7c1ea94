<?php

//
// ShopADV
// ShopADV API
// работа с входящими параметрами (по-умолчанию, параметры берутся из $_POST)
// class: Input
// Copyright (c) 2014 Alyoshin Vitaliy <master@ap-soft.com>
//

defined('IN_SITE') or die();

require_once('utils.php');

class Input
{

    // параметр, вызвавший последнюю ошибку
    private $ErrorParam = '';
    // сами данные
    private $Data;
    // объект данных - необязательно
    public $Object = null;

    function __construct($data = false)
    {
        if (!$this->set($data)) {
            $this->Data = $_POST;
        }
        mb_internal_encoding('UTF-8');
    }

    function __destruct()
    {

    }

    /**
     * Возвращает входные данные в виде массива.
     * @return array Входные данные.
     */
    public function get()
    {
        return $this->Data;
    }

    /**
     * Переопределяет входные данные собственными
     * @return bool TRUE в случае успеха
     */
    public function set($data)
    {
        if ($data && is_array($data)) {
            $this->Data = $data;
            return true;
        }
        return false;
    }

    /**
     * Тестирует входные данные на обязательные параметры
     * @param array $data [required] <p>
     * Массив наименований параметров, обязательных на наличие в массиве входных данных
     * </p>
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function required($data)
    {
        $this->errorClear();
        foreach ($data as $value) {
            if (!isset($this->Data[$value])) {
                $this->ErrorParam = $value;
                return false;
            }
        }
        return true;
    }

    private function errorClear()
    {
        $this->ErrorParam = '';
    }

    /**
     * Возвращает параметр, вызвавший последнюю ошибку
     * </p>
     * @return string Идентификатор параметра
     */
    public function errorParam()
    {
        return $this->ErrorParam;
    }

    public function getIsSet($p)
    {
        return isset($this->Data[$p]);
    }

    /**
     * Возвращает значение по-умолчанию для параметра
     * При неверном параметре - возвращается NULL
     * @param string $p <p>
     * Идентификатор параметра
     * </p>
     * @return mixed
     */
    public function getParamDefault($p)
    {
        switch ($p) {
            case 'callback':
            case 'key':
            case 'action':
            case 'hash':
            case 'sid':
            case 'login':
            case 'password':
            case 'client_title':
            case 'category_title':
            case 'category_group_title':
            case 'service_title':
            case 'jur_name':
            case 'title':
            case 'email':
            case 'phone':
            case 'contact_name':
            case 'address':
            case 'district':
            case 'template':
            case 'name':
            case 'charge_type':
            case 'code':
            case 'inn':
            case 'description':
            case 'title_buh':
            case 'service_point':
                return '';
            case 'page':
            case 'active':
            case 'period_count':
                return 1;
            case 'count':
            case 'quantity':
            case 'price':
            case 'price_alcohol':
            case 'removed':
                return 0;
            case 'sort_dir':
                return 'ASC';
            case 'services':
            case 'doc_list':
            case 'category_list':
            case 'task_list':
            case 'plan_list':
            case 'qAmounts':
                return array();
            case 'full':
                return false;
            case 'state':
            case 'have_alcohol':
            case 'check':
            case 'use_in_list':
                return 0;
            case 'lang':
                return LANG_RU;
        }
        return null;
    }

    private function getParamDefaultOrNull($p, $strict)
    {
        return $strict ? null : $this->getParamDefault($p);
    }

    /**
     * Возвращает безопасно параметр (выполняются все проверки, очистки и т.п.)
     * При неверном параметре - возвращается значение параметра по-умолчанию или null (если strict = true)
     * @param string $p <p>
     * Идентификатор параметра
     * </p>
     * @param bool $strict [optional] <p>
     * Если = TRUE, то при ошибке работе с параметром возвращается NULL в любом случае.
     * Если параметр отсутствует во входных данных, то возвращается его значение по-умолчанию или <b>NULL</b>
     * Если параметр неизвестный, то возвращается <b>NULL</b>
     * </p>
     * @return mixed
     */
    public function getParam($p, $strict = false)
    {
        $this->errorClear();

        if (isset($this->Data[$p])) {
            $value = $this->Data[$p];
        } else {
            $this->ErrorParam = $p;
            return $this->getParamDefaultOrNull($p, $strict);
        }

        return $this->getParamCustom($p, $value, $strict);
    }

    public function setParam($key, $value)
    {
        $this->Data[$key] = $value;
    }

    public function getParamCustom($p, $value, $strict = false)
    {
        $this->errorClear();
        switch ($p) {
            case 'data':
                return $value;

            case 'action':
            case 'template':
                $value = Utils::asStr($value, 32);
                if (!$value) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'code':
            case 'inn':
                $value = Utils::asStr($value, 64);
                if (!$value) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'login':
                $value = mb_strtoupper(Utils::asStr($value, 32));
                if (!$value) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'callback':
            case 'password':
            case 'client_title':
            case 'category_title':
            case 'category_group_title':
            case 'service_title':
            case 'jur_name':
            case 'title':
            case 'email':
            case 'phone':
            case 'contact_name':
            case 'address':
            case 'district':
            case 'name':
            case 'charge_type':
            case 'title_buh':
            case 'service_point':
                $value = Utils::asStr($value, 255);
                break;

            case 'description':
                $value = Utils::asStr($value);
                break;

            case 'redirect':
                $value = stripslashes($value);
                break;

            case 'sort_col':
                $value = Utils::asStr($value, 32);
                if (!$value) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'hash':
            case 'sid':
            case 'key':
            case 'doc':
                $value = mb_strtolower(Utils::asHexStr($value, 32));
                if (mb_strlen($value) != 32) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'date_open':
            case 'period_start':
            case 'period_end':
                $value = Utils::asPeriodDate($value);
                if (!$value) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'period':
            case 'period_s':
            case 'period_e':
                $value = Utils::asPeriodStartDate($value);
                if (!$value) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'service':
            case 'shop':
            case 'town':
            case 'count':
            case 'client':
            case 'client_dialog':
            case 'client_doc':
            case 'client_service_plan':
            case 'task':
            case 'maket':
            case 'quantity':
            case 'category':
            case 'version':
            case 'xid':
            case 'plan':
            case 'category_group':
            case 'year':
            case 'budget_plan':
            case 'period_count':
                $value = Utils::asInt($value);
                if (!$value || $value < 0) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'price':
            case 'price_alcohol':
            case 'payment':
            case 'amount':
            case 'turnover':
            case 'turnoverRate':
            case 'current_month_debt_percent':
                $value = Utils::asFloat($value);
                if (!$value || $value < 0) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'page':
                $value = Utils::asInt($value);
                if (!$value || $value < 1) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'sort_dir':
                $value = mb_strtoupper(Utils::asStr($value, 4));
                if (!in_array($value, array('ASC', 'DESC'))) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'role':
                $value = mb_strtoupper(Utils::asStr($value));
                if (!in_array($value, roleArr())) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'lang':
                $value = mb_strtolower(Utils::asStr($value));
                if (!in_array($value, langArr())) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'state':
            case 'active':
            case 'have_alcohol':
            case 'check':
            case 'use_in_list':
            case 'removed':
                if ($value === true || $value === 'true') {
                    $value = 1;
                } elseif ($value === false || $value === 'false') {
                    $value = 0;
                } else {
                    $value = (int)$value;
                    if ($value > 1 || $value < 0) {
                        $value = 1;
                    }
                }
                break;

            case 'period_type':
                $value = mb_strtoupper(Utils::asStr($value));
                if (!in_array($value, ptArr())) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'service_type':
                $value = mb_strtoupper(Utils::asStr($value));
                if (!in_array($value, stArr())) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'task_shop':
            case 'ts':
            case 'st':
                if (!is_array($value)) {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'days':
                if ($value) {
                    if (is_array($value)) {
                        $value = implode(',', $value);
                    }
                    $value = Utils::asIntSetArrayRange($value, 1, 31, ',');
                    if (!count($value)) {
                        $value = $this->getParamDefaultOrNull($p, $strict);
                    }
                } else {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'services':
                if ($value) {
                    if (is_array($value)) {
                        $value = implode(',', $value);
                    }
                    $value = Utils::asIntSetArray($value);
                } else {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'doc_list':
                if ($value) {
                    if (is_array($value)) {
                        $value = implode(',', $value);
                    }
                    $value = Utils::asStrSetArray($value, ',', 32);
                } else {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'task_list':
            case 'category_list':
                if ($value) {
                    if (is_array($value)) {
                        $value = implode(',', $value);
                    }
                    $value = Utils::asIntSetArray($value, ',');
                } else {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'plan_list':
                if ($value) {
                    if (!is_array($value)) {
                        $value = $this->getParamDefaultOrNull($p, $strict);
                    }
                }
                break;

            case 'qAmounts':
                if ($value) {
                    if (is_array($value)) {
                        $value = implode(',', $value);
                    }
                    $value = Utils::asFloatSetArray($value, ',');
                } else {
                    $value = $this->getParamDefaultOrNull($p, $strict);
                }
                break;

            case 'full':
                $value = true;
                break;

            default:
                $this->ErrorParam = $p;
                $value = null;
        } // switch($p)

        return $value;
    }

    /**
     * Возвращает безопасно параметры в виде массива, индексированного по идентификаторам параметров (выполняются все проверки, очистки и т.п.)
     * При неверном параметре - возвращается значение параметра по-умолчанию или null (если strict = true)
     * @param array $pArr <p>
     * Список идентификаторов параметров
     * </p>
     * @param bool $exists [optional] <p>
     * Если = TRUE, то будут возвращены только параметры, которые существуют во входных данных.
     * Если FALSE, то для тех параметров, что не существуют будет возвращено значение по-умолчанию.
     * </p>
     * @param bool $strict [optional] <p>
     * Если = TRUE, то при ошибке работе с параметром возвращается NULL в любом случае.
     * Если параметр отсутствует во входных данных, то возвращается его значение по-умолчанию или <b>NULL</b>
     * Если параметр неизвестный, то возвращается <b>NULL</b>
     * </p>
     * @return array of mixed
     */
    public function getParamArr($pArr, $exists = true, $strict = false)
    {
        $res = array();
        foreach ($pArr as $p) {
            if (!$exists || isset($this->Data[$p])) {
                $pn = $this->getParamDatabaseIndex($p);
                $res[$pn] = $this->getParam($p, $strict);
            }
        }
        return $res;
    }

    // using for ADD
    public function getParamValidArr($pArr)
    {
        $res = array();
        foreach ($pArr as $p) {
            if (isset($this->Data[$p])) {
                $pn = $this->getParamDatabaseIndex($p);
                $res[$pn] = $this->getParam($p, true);
                if (is_null($res[$pn])) {
                    unset($res[$pn]);
                }
            }
        }
        return $res;
    }

    public function getParamAll($pArrRequired, $pArrValid, $exists = true, $strict = false)
    {
        return array_merge(
            $this->getParamArr($pArrRequired, $exists, $strict), $this->getParamValidArr($pArrValid)
        );
    }

    // required ids
    // using for INFO, UPDATE (for IDS)
    public function getParamRequiredArr($pArr)
    {
        $res = array();
        foreach ($pArr as $p) {
            $pn = $this->getParamDatabaseIndex($p);
            if (isset($this->Data[$p])) {
                $res[$pn] = $this->getParam($p, true);
            } else {
                $res[$pn] = null;
            }
        }
        return $res;
    }

    // data exists
    // using for UPDATE (not IDS)
    public function getParamDataArr($pArr)
    {
        return $this->getParamArr($pArr, true, false);
    }

    private function getParamDatabaseIndex($p)
    {
        return $p;
    }
}

