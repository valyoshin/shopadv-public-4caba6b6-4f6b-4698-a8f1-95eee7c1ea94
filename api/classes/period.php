<?php

//
// Class: Period
// working with MySQL-like-periods: YYYY-MM-DD [hh:mm:ss]
// Copyright (c) 2012-2013 Alyoshin Vitaliy <master@ap-soft.com>
//

defined('IN_SITE') or die();

final class Period {

    static public function date() {
        return date('Y-m-d');
    }

    static public function time() {
        return date('H:i:s');
    }

    static public function curPeriod() {
        return date('Y-m-01');
    }

    static public function timestamp() {
        return self::date() . ' ' . self::time();
    }

    static public function dayOfWeek($y, $m, $d = 1) {
        return (date("w", mktime(0, 0, 0, (int) $m, (int) $d, (int) $y)) + 6) % 7;
    }

    static public function decode($period) {
        $r = array();
        // yyyy-mm-dd HH:MM:SS
        $r['y'] = (int) substr($period, 0, 4);
        $r['m'] = (int) substr($period, 5, 2);
        $r['d'] = (int) substr($period, 8, 2);
        $r['H'] = (int) substr($period, 11, 2);
        $r['M'] = (int) substr($period, 14, 2);
        $r['S'] = (int) substr($period, 17, 2);
        return $r;
    }

    static public function decodeWin($period) {
        $r = array();
        // dd.mm.yyyy HH:MM:SS
        $r['d'] = (int) substr($period, 0, 2);
        $r['m'] = (int) substr($period, 3, 2);
        $r['y'] = (int) substr($period, 6, 4);
        $r['H'] = (int) substr($period, 11, 2);
        $r['M'] = (int) substr($period, 14, 2);
        $r['S'] = (int) substr($period, 17, 2);
        return $r;
    }
    
    static public function encode($y, $m, $d = 1) {
        return date('Y-m-d', mktime(0, 0, 0, (int) $m, (int) $d, (int) $y));
    }
    
    static public function daysInMonth($month, $year) {
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

    static public function between($p1, $p2) {
        $pd1 = self::decode($p1);
        $pd2 = self::decode($p2);
        return 12 * ($pd2['y'] - $pd1['y']) + ($pd2['m'] - $pd1['m']) + 1;
    }

    static public
            function test($period) {
        $p = self::decode($period);
        return (is_array($p) && !!$p['y'] && !!$p['m']);
    }

    static public
            function incMonth($period, $mcount = 1) {
        $p = self::decode($period);
        return date('Y-m-d', mktime(0, 0, 0, $p['m'] + $mcount, $p['d'], $p['y']));
    }

    static public
            function startOfTheMonth($period) {
        $p = self::decode($period);
        return date('Y-m-d', mktime(0, 0, 0, $p['m'], 1, $p['y']));
    }

    static public
            function endOfTheMonth($period) {
        $p = self::decode($period);
        return date('Y-m-d', mktime(0, 0, 0, $p['m'], self::daysInMonth($p['m'], $p['y']), $p['y']));
    }

    static public
            function incDay($period, $dcount = 1) {
        $p = self::decode($period);
        return date('Y-m-d', mktime(0, 0, 0, $p['m'], $p['d'] + $dcount, $p['y']));
    }

    static public
            function short($period) {
        return substr($period, 0, 7);
    }

    static public
            function long($period) {
        return self::short($period) . '-01';
    }

    static public
            function onlyDay($period) {
        return substr($period, 0, 10);
    }

    static public
            function isBetween($period, $start, $end) {
        if (self::between($start, $period) < 1) {
            return false;
        }
        if (self::between($period, $end) < 1) {
            return false;
        }
        return true;
    }

    static public
            function isSameDay($p1, $p2) {
        $p1 = self::onlyDay($p1);
        $p2 = self::onlyDay($p2);
        return ($p1 == $p2);
    }

    static public
            function dateAsWin($period) {
        $p = self::decode($period);
        return date('d.m.Y', mktime(0, 0, 0, $p['m'], $p['d'], $p['y']));
    }

    static public
            function periodAsWin($period) {
        $p = self::decode($period);
        return date('m.Y', mktime(0, 0, 0, $p['m'], $p['d'], $p['y']));
    }

    static public
            function getWinDate($d) {
        $p = self::decode($d);
        if ($p['H']) {
            return $p['d'] . '.' . $p['m'] . '.' . $p['y'] . ' ' . $p['H'] . ':' . $p['M'] . ':' . $p['S'];
        } else {
            return $p['d'] . '.' . $p['m'] . '.' . $p['y'];
        }
    }

}
