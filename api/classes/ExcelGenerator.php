<?php

/**
 * ExcelNetGenerator
 * class: ExcelNetGenerator
 *
 * Adapter based on PHPExcel library for creating excel nets (incl. period states).
 * @copyright  Copyright (c) 2011 Alyoshin V.A. (admin@spyoutdoor.com) SPYOUTDOOR
 */
require_once('phpexcel/PHPExcel.php');

class ExcelGenerator {

    // Error constants
    const ERR_NOT_OPENED = 'Excel file is not opened...';
    const ERR_WRONG_CACHE_METHOD = 'Wrong cache method';
    const ERR_ARG_MUST_BE_SET = 'Argument must be set';
    // Excel writer format and filename extension
    const EX_WRITER_FORMAT = 'Excel5';
    const EXT = '.xls';
    // cache size for temp. file
    const CACHE_SIZE = 128; // Mb

    // Current Excel object
    // @var PHPExcel
    // NULL means that file is not opened now

    protected $_ex = NULL;
    // Last error description
    // @var string
    protected $_error = '';


    // Column for search keys
    // @var integer (>=0)
    public $ColumnKeys = 0;
    // Column for write values
    // @var integer (>=0)
    public $ColumnValues = 1;
    // Sheet name with template data
    // @var string
    public $DataSheet = '_data';
    // Pairs: key : value
    // @var array
    public $Data = array();

    public $BordersColor = '000000';

    // Create a new ExcelGenerator with temp. file in directory $tempDir
    // @param string $tempDir Path for storing temp. files.
    // @throw Exception
    public function __construct($tempDir) {
        $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => self::CACHE_SIZE . 'MB', 'dir' => $tempDir);

        if (!PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings)) {
            throw new Exception(self::ERR_WRONG_CACHE_METHOD);
        }
        $this->_ex = new PHPExcel(); // empty workbook
    }

    // Close the file and destroy the object
    public function __destruct() {
        $this->close();
    }

    // Real close all worksheets
    private function doClose() {
        $this->_ex->disconnectWorksheets();
        $this->_ex = NULL;
    }

    // Write real data to cell
    // @param PHPExcel_Worksheet $sheet Worksheet
    // @params integer $col, $row Cell coordinates
    // @param string data Data
    protected function writeCellValue($sheet, $col, $row, $data) {
        $sheet->setCellValueByColumnAndRow($col, $row, $data);
    }

    // Read real data from cell
    // @param PHPExcel_Worksheet $sheet Worksheet
    // @params integer $col, $row Cell coordinates
    // @return string data Data
    protected function readCellValue($sheet, $col, $row) {
        try {
            return $sheet->getCellByColumnAndRow($col, $row)->getValue();
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            return '';
        }
    }

    // @return PHPExcel_Cell
    public function getCell($col, $row) {
        try {
            return $this->getSheet()->getCellByColumnAndRow($col, $row);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            return null;
        }
    }

    public function getCellValue($col, $row) {
        return $this->readCellValue($this->getSheet(), $col, $row);
    }

    // Open file by $filename as template
    // @param string $filename
    // @throw Exception
    public function open($filename) {
        try {
            if ($this->isOpened()) {
                $this->doClose();
            }
            $this->_ex = PHPExcel_IOFactory::load($filename);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('open: ' . $this->_error);
        }
    }

    // Is file opened?
    // @return boolean TRUE - if opened
    public function isOpened() {
        return !!($this->_ex);
    }

    // Save file to $filename
    // @param string $filename
    // @throw Exception
    public function save($filename) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $objWriter = PHPExcel_IOFactory::createWriter($this->_ex, self::EX_WRITER_FORMAT);
            $objWriter->save($filename);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('save: ' . $this->_error);
        }
    }

    // Close file
    // @throw Exception
    public function close() {
        try {
            if ($this->isOpened()) {
                $this->doClose();
            }
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('close: ' . $this->_error);
        }
    }

    // Add new empty sheet
    // @param string $title Title for a new sheet
    // @param integer|null $index Index of sheet after which will be add new
    // @return integer Index of a new sheet
    // @throw Exception
    public function sheetAdd($title, $index = NULL) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $sheet = new PHPExcel_Worksheet($this->_ex, $title);
            $this->_ex->addSheet($sheet, $index);
            return $this->_ex->getIndex($sheet);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('sheetAdd: ' . $this->_error);
        }
    }

    // Delete sheet from file
    // @param integer $index Index of a sheet for delete
    public function sheetDelete($index) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $this->_ex->removeSheetByIndex($index);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('sheetDelete: ' . $this->_error);
        }
    }

    // Return sheet title(name) by index or title of the current sheet
    // @param integer|null $index Sheet index
    // @return string Sheet title (name)
    // @throw Exception
    public function getSheetName($index = NULL) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            if ($index === NULL) {
                return $this->_ex->getActiveSheet()->getTitle();
            } else {
                return $this->_ex->getSheet($index)->getTitle();
            }
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('getSheetName: ' . $this->_error);
        }
    }

    // Set sheet title(name) by index
    // @param integer $index Sheet index
    // @param string $title Sheet title (name)
    // @throw Exception
    public function setSheetName($index, $title) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $this->_ex->getSheet($index)->setTitle($title);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('setSheetName: ' . $this->_error);
        }
    }

    // Return current sheet index
    // @return integer Current sheet index
    // @throw Exception
    public function getSheetIndex() {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            return $this->_ex->getIndex($this->_ex->getActiveSheet());
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('getSheetIndex: ' . $this->_error);
        }
    }

    // Set current sheet index
    // @param integer $index Sheet index
    // @throw Exception
    public function setSheetIndex($index) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $this->_ex->setActiveSheetIndex($index);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('setSheetIndex: ' . $this->_error);
        }
    }

    // Set current sheet index by name
    // @param string $sheetName Sheet name
    // @throw Exception
    public function setSheetByName($sheetName) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $this->_ex->setActiveSheetIndexByName($sheetName);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('setSheetByName: ' . $this->_error);
        }
    }

    // apply data to the opened file
    public function apply() {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $this->setSheetByName($this->DataSheet);
            $sheet = $this->_ex->getActiveSheet();
            for($row = 1; $row <= $sheet->getHighestRow(); $row++) {
                $key = (string) $this->readCellValue($sheet, $this->ColumnKeys, $row);
                $key = strtolower( trim($key) );
                if(!empty($key) && isset($this->Data[$key])) {
                    $this->writeCellValue($sheet, $this->ColumnValues, $row, $this->Data[$key]);
                }
            }
            $this->setSheetIndex(0);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('apply: ' . $this->_error);
        }
    }

    // Get last error
    // @return string Last error description (msg)
    public function getError() {
        return $this->_error;
    }

    protected function getRange($left, $top, $right, $bottom) {
        $lt = PHPExcel_Cell::stringFromColumnIndex($left) . $top;
        $rb = PHPExcel_Cell::stringFromColumnIndex($right) . $bottom;
        return $lt . ':' . $rb;
    }

    // Add borders
    public function writeBorders($colStart, $rowStart, $colLast, $rowLast, $borderStyle = PHPExcel_Style_Border::BORDER_THIN) {
        $styleArray = array(
            'allborders' => array(
                'style' => $borderStyle,
                'color' => array('rgb' => $this->BordersColor),
            ),
        );
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $sheet = $this->_ex->getActiveSheet();
            $sheet->getStyle($this->getRange($colStart, $rowStart, $colLast, $rowLast))->getBorders()->applyFromArray($styleArray);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('writeBorders: ' . $this->_error);
        }
    }

    public function getLastColumn($sheetIndex = null) {
        if($sheetIndex == null) {
            $sheetIndex = $this->getSheetIndex();
        }
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            return PHPExcel_Cell::columnIndexFromString($this->_ex->getSheet($sheetIndex)->getHighestDataColumn());
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('getLastColumn: ' . $this->_error);
        }
    }

    public function getLastRow($sheetIndex = null) {
        if($sheetIndex == null) {
            $sheetIndex = $this->getSheetIndex();
        }
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            return $this->_ex->getSheet($sheetIndex)->getHighestDataRow();
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('getLastRow: ' . $this->_error);
        }
    }

    public function columnsAdd($beforeCol, $numberOfCols = 1, $sheetIndex = null) {
        if($sheetIndex == null) {
            $sheetIndex = $this->getSheetIndex();
        }
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $this->_ex->getSheet($sheetIndex)->insertNewColumnBeforeByIndex($beforeCol, $numberOfCols);
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('columnAdd: ' . $this->_error);
        }
    }

    public function getSheet() {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            return $this->_ex->getSheet($this->getSheetIndex());
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('getSheet: ' . $this->_error);
        }
    }

}
