<?php

//
// class: Loger
// Copyright (c) 2012-2013 Alyoshin Vitaliy <master@ap-soft.com>
//
// Loger line format:
// DATE (IP) [MODULE_NAME] MSG_TYPE {SESSION} DESC\n
// MSG_TYPE one of: info | warn | error
//

defined('IN_SITE') or die();

require_once('utils.php');

class Loger {

    const INFO = 'info';
    const WARN = 'warn';
    const ERROR = 'error';

    public $LogFile = '';
    public $Module = '';
    public $MailTypes = array('warn', 'error');
    public $Session = array();
    public $DateFormat = 'Y-m-d H:i:s';
    private $TimerStart;
    
    public $Mailer = null; // SMTP mailer
    public $Email = '';

    function __construct($logFile, $moduleName) {
        $this->LogFile = $logFile;
        $this->Module = trim($moduleName);
        $this->timerFlush();
    }

    function __destruct() {

    }

    public function getTimerStamp($flush = true) {
        $time = microtime(true) - $this->TimerStart;
        if($flush) {
            $this->timerFlush();
        }
        return $time;
    }

    public function timerFlush() {
        $this->TimerStart = microtime(true);
    }

    private function linear($ar) {
        $str = '';
        foreach ($ar as $k => $v) {
            if (is_array($v)) {
                $v = print_r($v, true);
            }
            $str .= $k . ': ' . $v . '; ';
        }
        return $str;
    }

    private function msg($msgType, $desc, $incPost = false) {
        $msgType = strtolower($msgType);
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = 'localhost';
        }
        $r = date($this->DateFormat) . ' (' . $ip . ') [' . $this->Module . '] ' . $msgType . ' {' . $this->linear($this->Session) . '}';
        if ($incPost) {
            $r .= ' P{' . $this->linear($_POST) . '}';
        }
        return $r . ' ' . $desc;
    }

    private function subject($msgType) {
        return $this->Module . ' ' . $msgType;
    }

    private function send($msg, $msgType) {
        if($this->Mailer && $this->Email) {
            $this->Mailer->setMessage($msg);
            $this->Mailer->send($this->Email, $this->subject($msgType));
        }
    }

    public function log($msgType, $desc = '', $incPost = false) {
        $msg = $this->msg($msgType, $desc, $incPost);
        Utils::logFile($msg, $this->LogFile);
        if (in_array($msgType, $this->MailTypes)) {
            $this->send($msg, $msgType);
        }
    }

    public function debug(&$msg) {
        $this->debugStr(print_r($msg, true));
    }

    public function debugStr($msg) {
        $this->log(self::WARN, $msg);
    }
    
    public function timestamp($msg) {
        Utils::logFile('TIMESTAMP [' . date('Y-m-d H:i:s u') . '] ' . $msg, $this->LogFile);
    }

}
