<?php

defined('IN_SITE') or die();

require_once('utils.php');

class PhotoManager {

    public $SrcFileName = ''; // must be already UPLOADED (move_uploaded_file)
    public $DestFileName = '';
    public $ScaleWidthMin = 0;
    public $ScaleWidthMax = 0;
    public $ScaleHeightMin = 0;
    public $ScaleHeightMax = 0;
    public $UseScale = false;
    public $TempDir = '';
    private $MaxImageWidth = 20480;
    private $MaxImageHeight = 15360;
    private $MaxImageSize = 20971520; // 20*1024*1024; // 20Mb
    private $Error = 0;
    public $DestFileFormat = 'jpeg'; // or 'png'

    function __construct($temp = '') {
        $this->TempDir = $temp;
    }

    public function process() {
        $this->Error = 0;

        $src = $this->SrcFileName;

        if (!file_exists($src)) {
            $this->Error = 1;
            return false;
        }

        if (filesize($src) > $this->MaxImageSize) {
            $this->Error = 9;
            return false;
        }

        if (($size = @getimagesize($src)) === false) {
            $this->Error = 2;
            return false;
        }

        if (($size[0] > $this->MaxImageWidth) || ($size[1] > $this->MaxImageHeight)) {
            $this->Error = 10;
            return false;
        }

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));

        $tmp1 = '';
        $tmp2 = '';

        if ($format != $this->DestFileFormat) {
            $tmp1 = $this->TempDir . '/' . Utils::uniqid();

            switch ($this->DestFileFormat) {
                case 'jpeg':
                    if (!$this->makeJPEG($src, $tmp1, $format)) {
                        $this->clear($tmp1);
                        return false;
                    }
                    break;
                case 'png':
                    if (!$this->makePNG($src, $tmp1, $format)) {
                        $this->clear($tmp1);
                        return false;
                    }
                    break;
                default:
                    return false;
            }

            $src = $tmp1;
            $format = $this->DestFileFormat;
            $size = @getimagesize($src);
            if (!$size) {
                $this->Error = 4;
                $this->clear($tmp1);
                return false;
            }
        }

        if (($this->DestFileFormat == 'jpeg') && $this->UseScale && $this->ScaleWidthMin && $this->ScaleWidthMax && $this->ScaleHeightMin && $this->ScaleHeightMax) {
            $tmp2 = $this->TempDir . '/' . Utils::uniqid();
            if (!$this->resize($src, $tmp2, $size, $format)) {
                $this->clear($tmp1);
                $this->clear($tmp2);
                $this->Error = 11;
                return false;
            }
            $src = $tmp2;
        }

        // copying src to dest
        $r = $this->makeCopy($src, $this->DestFileName);

        $this->clear($tmp1);
        $this->clear($tmp2);

        return $r;
    }

    public function error() {
        return $this->Error;
    }

    public function errorMsg() {
        switch ($this->Error) {
            case 0: return '';
            case 1: return 'source file does not exists';
            case 2: return 'source file is not an image';
            case 3: return 'unknown image type';
            case 4: return 'error reading image';
            case 5: return 'create new image error';
            case 6: return 'not enough memory';
            case 7: return 'resampling error';
            case 8: return 'error creating jpg';
            case 9: return 'file size too big';
            case 10: return 'image sizes too big';
            case 11: return 'resize error';
            case 12: return 'can not copy to destination file';
            default: return 'unknown error';
        }
    }

    // private

    private function makeCopy($src, $dest) {
        if (($src != $dest) && !@copy($src, $dest)) {
            $this->Error = 12;
            return false;
        }
        return true;
    }

    private function makeImg($src, $format) {
        $icfunc = 'imagecreatefrom' . $format;
        if (!function_exists($icfunc)) {
            $this->Error = 3;
            return false;
        }

        $isrc = @$icfunc($src);
        if (!$isrc) {
            $this->Error = 4;
            return false;
        }
        return $isrc;
    }

    private function makeJPEG($src, $dest, $format) {
        if ($format == 'jpeg') {
            return $this->makeCopy($src, $dest);
        }

        $isrc = $this->makeImg($src, $format);
        if(!$isrc) {
            return false;
        }

        $r = @imagejpeg($isrc, $dest, 100);
        if (!$r) {
            $this->Error = 8;
            return false;
        }
        return true;
    }

    private function makePNG($src, $dest, $format) {
        if ($format == 'png') {
            return $this->makeCopy($src, $dest);
        }

        $isrc = $this->makeImg($src, $format);
        if(!$isrc) {
            return false;
        }

        $r = @imagepng($isrc, $dest);
        if (!$r) {
            $this->Error = 8;
            return false;
        }
        return true;
    }

    private function resize($src, $dest, $size, $format = 'jpeg') {
        $icfunc = 'imagecreatefrom' . $format;
        if (!function_exists($icfunc)) {
            return false;
        }

        $x_ratio_min = $this->ScaleWidthMin / $size[0];
        $y_ratio_min = $this->ScaleHeightMin / $size[1];
        $x_ratio_max = $this->ScaleWidthMax / $size[0];
        $y_ratio_max = $this->ScaleHeightMax / $size[1];

        $ratio_min = min($x_ratio_min, $y_ratio_min);
        $ratio_max = min($x_ratio_max, $y_ratio_max);

        if ($ratio_min < 1 && $ratio_max < 1) {
            $use_x_ratio = ($x_ratio_max == $ratio_max);
            $new_width = $use_x_ratio ? $this->ScaleWidthMax : floor($size[0] * $ratio_max);
            $new_height = !$use_x_ratio ? $this->ScaleHeightMax : floor($size[1] * $ratio_max);
        } else if ($ratio_min > 1 && $ratio_max > 1) {
            $use_x_ratio = ($x_ratio_min == $ratio_min);
            $new_width = $use_x_ratio ? $this->ScaleWidthMin : floor($size[0] * $ratio_min);
            $new_height = !$use_x_ratio ? $this->ScaleHeightMin : floor($size[1] * $ratio_min);
        } else {
            $ratio = $this->ScaleWidthMin / $this->ScaleHeightMin;
            $ratio_img = $size[0] / $size[1];
            if ($ratio < $ratio_img) {
                $new_width = floor($ratio * $size[1]);
                $new_height = $size[1];
            } else {
                $new_width = $size[0];
                $new_height = floor($size[0] / $ratio);
            }
        }

        $isrc = @$icfunc($src);
        if (!$isrc) {
            return false;
        }

        $idest = @imagecreatetruecolor($new_width, $new_height);
        if (!$idest) {
            @imagedestroy($isrc);
            return false;
        }

        if (!@imagecopyresampled($idest, $isrc, 0, 0, 0, 0, $new_width, $new_height, $size[0], $size[1])) {
            @imagedestroy($isrc);
            @imagedestroy($idest);
            return false;
        }

        $r = @imagejpeg($idest, $dest, 100);

        @imagedestroy($isrc);
        @imagedestroy($idest);

        return !!$r;
    }

    private function clear($fname) {
        if ($fname) {
            @unlink($fname);
        }
    }

}

?>