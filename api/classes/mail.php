<?php

//
// class: SMTPMailer
// class for send e-mail's (only in UTF-8 encoding with base64)
// Copyright (c) 2014 Alyoshin Vitaliy <master@ap-soft.com>
// base on SMTP.php,Socket.php,PEAR.php
//

defined('IN_SITE') or die();

require_once('pear/smtp.php');

class SMTPMailer {

    private $error = '';
    private $att = array(); // attachments array
    private $msg = '';
    public $Host = 'localhost';
    public $Auth = true;
    public $User = '';
    public $Password = '';
    public $Port = 25;
    public $SMTPType = 'LOGIN';
    public $SMTPFrom = '';
    public $From = '';
    public $FromName = '';
    public $Mailer = 'SMTPMailer';
    public $ErrorsTo = '';
    public $MailHost = ''; // use Host if blank - need to create Message-Id field
    public $Charset = 'UTF-8'; //'windows-1251';
    public $IsHTMLContent = false;
    public $AutoRCPTReplace = '%RCPT%';
    public $EncodeFromName = true;

    function __construct() {

    }

    function __destruct() {

    }

    private function isError($smtpResult) {

    }

    /*
      Make real send e-mail
      @param string $rcpt       E-mail of rcpt
      @param string $message    Message itself
      @return boolean           true if sending success
      @update
      $error    last error of sending
     */

    private function sendReal($rcpt, $message) {
        if (!($SMTP = new Net_SMTP($this->Host, $this->Port))) {
            $this->error = 'error creating SMTP object';
            return false;
        }
        if ($SMTP->isError($SMTP->connect())) {
            $this->error = 'SMTP connect error';
            return false;
        }
        if ($this->Auth) {
            if ($SMTP->isError($SMTP->auth($this->User, $this->Password, $this->SMTPType))) {
                $this->error = 'SMTP auth error';
                return false;
            }
        }
        if ($SMTP->isError($SMTP->mailFrom($this->SMTPFrom))) {
            $this->error = 'SMTP mail from error';
            return false;
        }

        $rcpt = str_replace(';', ',', $rcpt);
        $RCPT = explode(',', $rcpt);

        foreach ($RCPT as $r) {
            if ($SMTP->isError($SMTP->rcptTo($r))) {
                $this->error = 'SMTP rcpt error';
                return false;
            }
        }

        if ($SMTP->isError($SMTP->data($message))) {
            $this->error = 'SMTP data error';
            return false;
        }
        $SMTP->disconnect();
        $this->error = '';
        return true;
    }

    private function getFromWithName() {
        if ($this->FromName) {
            if ($this->EncodeFromName) {
                return $this->encode($this->FromName) . ' <' . $this->getFrom() . '>';
            }
            return $this->FromName . ' <' . $this->getFrom() . '>';
        }
        return $this->getFrom();
    }

    private function getFrom() {
        if ($this->From) {
            return $this->From;
        }
        return $this->SMTPFrom;
    }

    private function getErrorsTo() {
        if ($this->ErrorsTo) {
            return $this->ErrorsTo;
        }
        return $this->getFrom();
    }

    private function getMsgToSend($rcpt) {
        if ($this->AutoRCPTReplace) {
            return str_replace($this->AutoRCPTReplace, $rcpt, $this->msg);
        }
        return $this->msg;
    }

    private function getMID() {
        $mid = uniqid(time());
        if ($this->MailHost) {
            $mid .= '@' . $this->MailHost;
        } else {
            $mid .= '@' . $this->Host;
        }
        return '<' . $mid . '>';
    }

    private function haveAttachment() {
        return !!count($this->att);
    }

    private function encode($msg) {
        return '=?' . $this->Charset . '?B?' . base64_encode($msg) . '?=';
    }

    private function getOnlyText($msg) {
        return strip_tags($msg);
    }

    private function getFileType($filename) {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        if (!$finfo) {
            return 'application/octet-stream';
        }
        $r = finfo_file($finfo, $filename);
        finfo_close($finfo);
        if (!$r) {
            return 'application/octet-stream';
        }
        return $r;
    }

    private function getBasename($file) {
        $array = explode('/', $file);
        return end($array);
    }


    // PUBLIC

    public function addAttachment($file) {
        $this->att[] = $file;
    }

    public function clearAttachment() {
        $this->att = array();
    }

    /*
      Set message as a simple text
      @param string $msg    Message text
      @return none
      @update $msg
     */

    public function setMessage($msg) {
        $this->msg = $msg;
    }

    /*
      Set message with template text
      @param string $msg    Message text
      @param string $template    Template text
      @param array|string $search    Template search-params
      @param array|string $replace    Template replace-params
      @return none
      @update $msg
     */

    public function setMessageWithTemplate($template, $search = '', $replace = '') {
        $this->setMessage(str_replace($search, $replace, $template));
    }

    /*
      Set message with template text loaded from file
      @param string $msg    Message text
      @param string $filename    Template file name
      @param array|string $search    Template search-params
      @param array|string $replace    Template replace-params
      @return boolean    true if success
      @update $msg (only if success)
     */

    public function setMessageWithTemplateFile($filename, $search = '', $replace = '') {
        $tpl = @file_get_contents($filename);
        if (!$tpl)
            return false;
        $this->setMessageWithTemplate($tpl, $search, $replace);
        return true;
    }

    /*
      Send an e-mail
      @param string $rcpt       E-mail of rcpt
      @param string $subject    Subject of the message
      @return boolean           true if sending success
      @update
      $error    last error of sending
     */

    /*

      1. HTML

      MIME-Version: 1.0
      Content-Type: multipart/alternative; boundary="BOUNDARY"
      \n
      --BOUNDARY
      Content-Type: text/plain; charset="UTF-8"
      Content-Transfer-Encoding: base64
      \n
      ... TEXT\n
      \n
      --BOUNDARY
      Content-Type: text/html; charset="UTF-8"
      Content-Transfer-Encoding: base64
      \n
      ... HTML\n
      \n
      --BOUNDARY--
      \n
      \n

      2. TEXT

      MIME-Version: 1.0
      Content-Type: text/plain; charset=UTF-8;
      Content-Transfer-Encoding: base64
      \n
      ... TEXT\n
      \n
      \n

      3. TEXT + ATT

      MIME-Version: 1.0
      Content-Type: multipart/mixed; boundary="BOUNDARY"
      \n
      --BOUNDARY
      Content-Type: text/plain; charset="UTF-8"
      Content-Transfer-Encoding: base64
      \n
      ... TEXT\n
      \n
      --BOUNDARY
      Content-Type: application/octet-stream
      Content-Transfer-Encoding: base64
      Content-Disposition: attachment; filename = ...
      \n
      ... FILE
      \n
      --BOUNDARY--
      \n
      \n

      4. HTML + ATT

      MIME-Version: 1.0
      Content-Type: multipart/mixed; boundary="BOUNDARY_1"
      \n
      --BOUNDARY_1
      Content-Type: multipart/alternative; boundary="BOUNDARY_2"
      \n
      --BOUNDARY_2
      Content-Type: text/plain; charset="UTF-8"
      Content-Transfer-Encoding: base64
      \n
      ... TEXT\n
      \n
      --BOUNDARY_2
      Content-Type: text/html; charset="UTF-8"
      Content-Transfer-Encoding: base64
      \n
      ... HTML\n
      \n
      --BOUNDARY_2--
      \n
      --BOUNDARY_1
      Content-Type: application/octet-stream
      Content-Transfer-Encoding: base64
      Content-Disposition: attachment; filename = ...
      \n
      ... FILE
      \n
      --BOUNDARY_1--
      \n
      \n

     */

    public function send($rcpt, $subject) {
        $subject = $this->encode($subject);
        $xfrom = $this->getFromWithName();
        $from = $this->getFrom();
        $errors = $this->getErrorsTo();
        $msg = $this->getMsgToSend($rcpt);
        $mid = $this->getMID();

        $boundary1 = "b1" . md5(uniqid(time()));
        $boundary2 = "b2" . md5(uniqid(time()));

        $headers = "From: $xfrom\n";
        $headers .= "To: $rcpt\n";
        $headers .= "Subject: $subject\n";
        $headers .= "Reply-To: $xfrom\n";
        $headers .= "Return-Path: $from\n";
        $headers .= "Errors-To: $errors\n";
        $headers .= "Message-ID: $mid\n";
        $headers .= "X-Sender: $from\n";
        $headers .= "X-Mailer: $this->Mailer\n";
        $headers .= "X-Priority: 3\n";
        $headers .= "Date: " . date("r") . "\n";
        $headers .= "MIME-Version: 1.0\n";

        $multipart = '';

        if ($this->haveAttachment()) {
            $headers .= "Content-Type: multipart/mixed; boundary=\"$boundary1\"\n";

            if ($this->IsHTMLContent) {
                // HTML + ATT
                $multipart .= "\n";
                $multipart .= "--$boundary1\n";
                $multipart .= "Content-Type: multipart/alternative; boundary=\"$boundary2\"\n";
                $multipart .= "\n";

                $multipart .= "--$boundary2\n";
                $multipart .= "Content-Type: text/plain; charset=\"$this->Charset\"\n";
                $multipart .= "Content-Transfer-Encoding: base64\n";
                $multipart .= "\n";
                $multipart .= chunk_split(base64_encode($this->getOnlyText($msg))) . "\n";
                //$multipart .= "\n";

                $multipart .= "--$boundary2\n";
                $multipart .= "Content-Type: text/html; charset=\"$this->Charset\"\n";
                $multipart .= "Content-Transfer-Encoding: base64\n";
                $multipart .= "\n";
                $multipart .= chunk_split(base64_encode($msg)) . "\n";
                //$multipart .= "\n";

                $multipart .= "--$boundary2--\n";
                $multipart .= "\n";
            } else {
                // TEXT + ATT
                $multipart .= "\n";
                $multipart .= "--$boundary1\n";
                $multipart .= "Content-Type: text/plain; charset=\"$this->Charset\"\n";
                $multipart .= "Content-Transfer-Encoding: base64\n";
                $multipart .= "\n";
                $multipart .= chunk_split(base64_encode($msg)) . "\n";
                //$multipart .= "\n";
            }

            foreach ($this->att as $row) {
                if (is_array($row)) {
                    $a = $row;
                } else {
                    $a['name'] = $row;
                    $a['tmp_name'] = $row;
                }

                if (isset($a['tmp_name']) && ($a['tmp_name'] != '')) {
                    $fp = @fopen($a['tmp_name'], "r");
                    if (!$fp) {
                        $this->error = 'Attachment file was not found: ' . $a['tmp_name'];
                        return false;
                    }
                    $file = @fread($fp, filesize($a['tmp_name']));
                    @fclose($fp);
                    if ($file !== false) {
                        $filename = $this->encode($this->getBasename($a['name']));
                        $multipart .= "--$boundary1\n";
                        $multipart .= "Content-Type: " . $this->getFileType($a['tmp_name']) . "; name=\"" . $filename . "\"\n";
                        $multipart .= "Content-Transfer-Encoding: base64\n";
                        $multipart .= "Content-Disposition: attachment; filename = \"" . $filename . "\"\n";
                        $multipart .= "\n";
                        $multipart .= chunk_split(base64_encode($file)) . "\n";
                        $multipart .= "\n";
                    }
                }
            }; // foreach attachment
            $multipart .= "--$boundary1--\n";
            $multipart .= "\n";
        } elseif ($this->IsHTMLContent) {
            // HTML
            $headers .= "Content-Type: multipart/alternative; boundary=\"$boundary1\"\n";

            $multipart .= "\n";
            $multipart .= "--$boundary1\n";
            $multipart .= "Content-Type: text/plain; charset=\"$this->Charset\"\n";
            $multipart .= "Content-Transfer-Encoding: base64\n";
            $multipart .= "\n";
            $multipart .= chunk_split(base64_encode($this->getOnlyText($msg))) . "\n";
            //$multipart .= "\n";

            $multipart .= "--$boundary1\n";
            $multipart .= "Content-Type: text/html; charset=\"$this->Charset\"\n";
            $multipart .= "Content-Transfer-Encoding: base64\n";
            $multipart .= "\n";
            $multipart .= chunk_split(base64_encode($msg)) . "\n";
            //$multipart .= "\n";

            $multipart .= "--$boundary1--\n";
            $multipart .= "\n";
        } else {
            // TEXT
            $headers .= "Content-Type: text/plain; charset=\"$this->Charset\"\n";
            $headers .= "Content-Transfer-Encoding: base64\n";
            $headers .= "\n";
            $headers .= chunk_split(base64_encode($msg)) . "\n";
            $headers .= "\n";
            $headers .= "\n";
        }

        return $this->sendReal($rcpt, $headers . "\r\n" . $multipart);
    }

    public function getError() {
        return $this->error;
    }

}

?>
