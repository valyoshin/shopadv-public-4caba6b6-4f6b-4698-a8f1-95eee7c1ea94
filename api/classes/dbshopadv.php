<?php

//
// ShopADV
// ShopADV API
// Class: DatabaseShopADV (extends Database)
// common MySQL database for ShopADV project
// Copyright (c) 2014 Alyoshin Vitaliy <master@ap-soft.com>
//

defined('IN_SITE') or die();

require_once('Database.php');
require_once('loger.php');
require_once('period.php');

class DatabaseShopADV extends Database {

    public $Loger = null;
    public $LogLevel = Loger::ERROR;
    public $XID = 0;
    public $Country = '';

    public function __construct($dbname, $dbhost, $dbuser, $dbpassword, $dbport = 3306) {
        parent::__construct($dbname, $dbhost, $dbuser, $dbpassword, $dbport);
    }

    public function __destruct() {
        parent::__destruct();
    }

    public function connect() {
        if (parent::connect()) {
            parent::query('SET SESSION group_concat_max_len = 100000');
            return true;
        }
        return false;
    }

    public function query($sql) {
        $r = parent::query($sql);
        if ($this->Loger) {
            if ($this->failure()) {
                $this->Loger->log($this->LogLevel, 'query: ' . $this->fullError());
            }
            //$this->Loger->log(Loger::INFO, 'SQL: ' . $sql);
        }
        return $r;
    }

    // COMMON

    private function qLimit($start, $count) {
        if (!$start && !$count) {
            return "";
        }
        $sql = "
            LIMIT $start, $count
        ";
        return $sql;
    }

    private function qWhere($whereStart, $filterArr, $aliases = array()) {
        $sql = '';
        $w = $whereStart;
        foreach ($filterArr as $k => $v) {
            switch ($k) {
                case 'client':
                    if ($v) {
                        $sql .= $w . "
                           c.client = $v
                        ";
                        $w = 'AND';
                    }
                    break;
                case 'period_type':
                    if ($v) {
                        $sql .= $w . "
                           s.period_type = '$v'
                        ";
                        $w = 'AND';
                    }
                    break;
                case 'client_title':
                    $v = '%' . $this->like($v) . '%';
                    $sql .= $w . "
                       c.title LIKE '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'category_title':
                    $v = '%' . $this->like($v) . '%';
                    $sql .= $w . "
                       c.title LIKE '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'category_group_title':
                    $v = '%' . $this->like($v) . '%';
                    $sql .= $w . "
                       cg.title LIKE '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'service_title':
                    $v = '%' . $this->like($v) . '%';
                    $sql .= $w . "
                       s.title LIKE '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'shop':
                    if ($v) {
                        $sql .= $w . "
                           s.shop = $v
                        ";
                        $w = 'AND';
                    }
                    break;
                case 'town':
                    if ($v) {
                        $sql .= $w . "
                           t.town = $v
                        ";
                        $w = 'AND';
                    }
                    break;
                case 'code':
                    if ($v) {
                        $sql .= $w . "
                           c.code = $v
                        ";
                        $w = 'AND';
                    }
                    break;
                case 'jur_name':
                    $v = '%' . $this->like($v) . '%';
                    $sql .= $w . "
                       c.jur_name LIKE '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'title':
                    $v = '%' . $this->like($v) . '%';
                    $sql .= $w . "
                       t.title LIKE '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'description':
                    $v = '%' . $this->like($v) . '%';
                    $sql .= $w . "
                       t.description LIKE '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'service':
                    if ($v) {
                        $a = isset($aliases['service']) ? $aliases['service'] : 't';
                        $sql .= $w . "
                            $a.service = $v
                       ";
                        $w = 'AND';
                    }
                    break;
                case 'login':
                    $sql .= $w . "
                       u.login = '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'name':
                    $sql .= $w . "
                       u.name LIKE '%$v%'
                    ";
                    $w = 'AND';
                    break;
                case 'email':
                    $sql .= $w . "
                       u.email LIKE '%$v%'
                    ";
                    $w = 'AND';
                    break;
                case 'role':
                    $sql .= $w . "
                       u.role = '$v'
                    ";
                    $w = 'AND';
                    break;
                case 'state':
                    $sql .= $w . "
                       state = $v
                    ";
                    $w = 'AND';
                    break;
                case 'use_in_list':
                    $sql .= $w . "
                       use_in_list = $v
                    ";
                    $w = 'AND';
                    break;
            }
        }
        return $sql;
    }

    private function qOrder($sort_col, $sort_dir) {
        if (!$sort_col) {
            return "";
        }
        $sql = "
            ORDER BY $sort_col $sort_dir
        ";
        return $sql;
    }

    // SESSION

    public function testXID($xid) {
        $sql = "
            SELECT
                `xid`,
                `title`,
                `key`,
                `country`
            FROM `xid`
            WHERE `xid` = $xid
                AND `state` = 1
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function sessionAdd($sid, $login, $ip, $xid) {
        $sql = "
            INSERT
            INTO session
            SET
                sid = '$sid',
                login = '$login',
                ip = '$ip',
                xid = $xid,
                date_action = NOW()
        ";
        $this->query($sql);
        if ($this->success()) {
            return $this->lastid();
        }
        return false;
    }

    public function sessionTest($sid, $ip) {
        $sql = "
            SELECT
                s.session,
                s.login,
                s.xid,
                x.title,
                x.country
            FROM session s
            INNER JOIN xid x ON(x.xid = s.xid)
            WHERE s.sid = '$sid'
                AND s.ip = '$ip'
                AND x.state = 1
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function sessionDo($session) {
        $sql = "
            UPDATE session
            SET date_action = NOW(), action_count = action_count + 1
            WHERE session = $session
        ";
        $this->query($sql);
        return $this->success();
    }

    // >>> users

    public function getUserAuth($login, $passwordHash) {
        $sql = "
            SELECT
                login,
                name,
                email,
                role
            FROM users
            WHERE login = '$login'
                AND password_hash = '$passwordHash'
                AND state = 1
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function getUser($login) {
        $sql = "
            SELECT
                login,
                name,
                email,
                role
            FROM users
            WHERE login = '$login'
                AND state = 1
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function userAdd($data) {
        $data['xid'] = $this->XID;
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO users
            SET $set
        ";
        $this->query($sql);
        return $this->success();
    }

    public function userUpdate($login, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE users
            SET $set
            WHERE login = '$login'
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    public function userDelete($login) {
        $sql = "
            DELETE
            FROM users
            WHERE login = '$login'
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    public function userList($filter, $start, $count, $sort_col, $sort_dir) {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
                u.login,
                u.name,
                u.role,
                u.state,
                u.email
            FROM users u
            WHERE u.xid = $this->XID
        ";
        $sql .= $this->qWhere(' AND', $filter);
        $sql .= $this->qOrder($sort_col, $sort_dir);
        $sql .= $this->qLimit($start, $count);

        $this->query($sql);
        return $this->fetchAll();
    }

    public function userInfo($login) {
        $sql = "
            SELECT
                login,
                name,
                role,
                state,
                email
            FROM users
            WHERE login = '$login'
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetch();
    }

    // >>> doc

    public function docAdd($docHash, $ext, $title) {
        $ext = (string) $ext;
        $sql = "
            INSERT INTO doc
            SET
                doc = '$docHash',
                ext = '$ext',    
                title = '$title'
        ";

        $this->query($sql);

        return $this->success();
    }

    public function getDocInfo($doc) {
        $sql = "
            SELECT 
                title,
                ext
            FROM doc
            WHERE doc = '$doc'
        ";
        $this->query($sql);

        return $this->fetch();
    }

    // >>> service, service_town_price

    public function serviceListCache() {
        $sql = "
            SELECT
                s.service,
                s.code,
                s.title,
                s.period_type,
                s.service_type,
                s.min_period_count,
                s.price,
                s.price_alcohol,
                s.have_alcohol,
                s.have_shop_list,
                s.have_plan_list,
                s.have_maket,
                s.have_doc,
                s.have_description,
                s.have_payment,
                s.have_fine,
                s.have_days,
                s.shop_access,
                s.copy_access,
                s.import_access,
                s.state,
                (SELECT GROUP_CONCAT(CONCAT_WS(':', p.town, p.price) SEPARATOR ';') FROM service_town_price p WHERE p.service = s.service) AS town_price
            FROM service s
            WHERE s.xid = $this->XID
            ORDER BY s.service
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function serviceList($filter, $start, $count, $sort_col, $sort_dir) {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
                s.service,
                s.title AS service_title,
                s.period_type,
                s.service_type,
                s.min_period_count,
                s.price,
                s.price_alcohol,
                s.have_alcohol,
                s.have_shop_list,
                s.have_plan_list,
                s.have_maket,
                s.have_doc,
                s.have_description,
                s.have_payment,
                s.have_fine,
                s.have_days,
                s.shop_access,
                s.copy_access,
                s.import_access,
                s.state,
                IFNULL((SELECT 1 FROM service_town_price p WHERE p.service = s.service LIMIT 1), 0) AS have_town_price
            FROM service s
                WHERE s.xid = $this->XID
        ";
        $sql .= $this->qWhere(' AND', $filter, array('service' => 's'));
        $sql .= $this->qOrder($sort_col, $sort_dir);
        $sql .= $this->qLimit($start, $count);

        $this->query($sql);
        return $this->fetchAll();
    }

    public function serviceShortList() {
        $sql = "
            SELECT
                s.service,
                s.title,
                s.shop_access
            FROM service s
            WHERE s.state = 1
                AND s.xid = $this->XID
            ORDER BY s.service
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function serviceTitle($service) {
        $sql = "
            SELECT `title`
            FROM `service`
            WHERE `service` = $service
                AND `xid` = $this->XID
        ";
        $this->query($sql);

        return $this->fetchValue();
    }

    public function serviceInfo($service) {
        $sql = "
            SELECT
                service,
                code,
                title,
                title_buh,
                period_type,
                service_type,
                description,
                service_point,
                price,
                price_alcohol,
                have_alcohol,
                have_plan_list,
                have_shop_list,
                copy_access,
                import_access,
                have_payment,
                have_fine,
                state
            FROM service
            WHERE service = $service
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function getServiceByCode($code) {
        $sql = "
            SELECT
                service
            FROM service
            WHERE code = $code
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetchValue();
    }

    public function serviceUpdate($service, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE service
            SET $set
            WHERE service = $service
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    public function serviceTownInfo($service) {
        $sql = "
            SELECT
                town,
                price
            FROM service_town_price
            WHERE service = $service
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function serviceTownClear($service) {
        $sql = "
            DELETE
            FROM service_town_price
            WHERE service = $service
        ";
        $this->query($sql);
        return $this->success();
    }

    public function serviceTownAdd($service, $town, $price) {
        $sql = "
            INSERT
            INTO service_town_price
            SET
                service = $service,
                town = $town,
                price = $price
        ";
        $this->query($sql);
        return $this->success();
    }

    // >>> shop

    public function shopListCache() {
        $sql = "
            SELECT
                shop,
                town,
                title,
                district,
                address,
                date_open
            FROM shop
            WHERE state = 1
                AND xid = $this->XID
            ORDER BY shop
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function shopList() {
        $sql = "
            SELECT
                s.shop,
                t.title AS town_title,
                s.address
            FROM shop s
            INNER JOIN town t ON(t.town = s.town)
            WHERE s.state = 1
                AND s.xid = $this->XID
            ORDER BY s.shop
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function shopListFilter($filter, $start, $count, $sort_col, $sort_dir) {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
                s.shop,
                s.town,
                t.title AS town_title,
                s.address,
                s.district,
                s.date_open,
                s.state
            FROM shop s
            INNER JOIN town t ON(t.town = s.town)
            WHERE s.xid = $this->XID
        ";
        $sql .= $this->qWhere(' AND', $filter);
        $sql .= $this->qOrder($sort_col, $sort_dir);
        $sql .= $this->qLimit($start, $count);

        $this->query($sql);
        return $this->fetchAll();
    }

    public function shopDelete($shop) {
        $sql = "
            DELETE
            FROM shop
            WHERE shop = $shop
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    public function shopInfo($shop) {
        $sql = "
            SELECT
                shop,
                town,
                address,
                state,
                district,
                date_open
            FROM shop
            WHERE shop = $shop
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function shopAdd($data) {
        $data['xid'] = $this->XID;
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO shop
            SET $set
        ";
        $this->query($sql);
        return $this->success();
    }

    public function shopUpdate($shop, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE shop
            SET $set
            WHERE shop = $shop
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    // >>> town

    public function townListWithShop() {
        $sql = "
            SELECT
                t.town,
                t.region,
                t.title
            FROM town t
            WHERE EXISTS (
                SELECT 1
                FROM shop s
                WHERE s.town = t.town
                    AND s.xid = $this->XID
            )
            ORDER BY t.title
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function townListCache() {
        $sql = "
            SELECT
                t.town,
                t.title
            FROM town t
            WHERE t.title <> ''
                AND EXISTS (
                SELECT 1
                FROM region r
                WHERE r.region = t.region
                    AND r.country = '$this->Country'
            )
            ORDER BY t.title
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    // >>> client

    public function clientList($filter, $start, $count, $sort_col, $sort_dir, $currentMonth, $periodChargeStart, $periodChargeEnd, $serviceType = null) {
        $sqlClientWhere = $this->qWhere(' AND', $filter);
        $currentMonth = (int) $currentMonth;

        if ($serviceType) {
            $w = "
                AND s.`service_type` = '$serviceType'
            ";
        } else {
            $w = '';
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
                t.`client`,
                t.`code`,
                t.`title` AS client_title,
                t.`email` AS client_email,
                t.`jur_name`,
                t.`phone` AS client_phone,
                t.`contact_name`,
                t.`address` AS client_address,
                t.`inn`,
                t.`state`,
                t.`use_in_list`,   
                t.`plan_year_amount`,
                t.`total_period_charge`,
                (t.`plan_year_amount` * $currentMonth / 12) AS `plan_total_current_month_amount`,
                ((t.`plan_year_amount` * $currentMonth / 12) - t.`total_period_charge`) AS `current_month_debt_amount`,
                100 * (1 - 12 * t.`total_period_charge` / $currentMonth / t.`plan_year_amount`) AS `current_month_debt_percent`
            FROM (    
                SELECT 
                    c.`client`,
                    c.`code`,
                    c.`title`,
                    c.`email`,
                    c.`jur_name`,
                    c.`phone`,
                    c.`contact_name`,
                    c.`address`,
                    c.`inn`,
                    c.`state`,
                    c.`use_in_list`,
                    (SELECT cp.`amount` FROM `client_plan` cp WHERE cp.`client` = c.`client` AND cp.`year` = YEAR(NOW())) AS `plan_year_amount`,
                    (
                        SELECT SUM(IFNULL(t.`payment`, t.`amount`)) 
                        FROM `task` t 
                        INNER JOIN `task_period_charge` tc ON(tc.`task` = t.`task` AND tc.`period` BETWEEN '$periodChargeStart' AND '$periodChargeEnd') 
                        INNER JOIN `service` s ON(s.`service` = t.`service`)
                        WHERE t.`client` = c.`client`
                        $w
                    ) AS `total_period_charge`
                FROM client c
                WHERE c.xid = $this->XID
                $sqlClientWhere
            ) AS t
        ";

        if (isset($filter['current_month_debt_percent'])) {
            $sql .= "\n HAVING `current_month_debt_percent` >= " . $filter['current_month_debt_percent'] . "\n";
        }

        $sql .= $this->qOrder($sort_col, $sort_dir);
        $sql .= $this->qLimit($start, $count);

        $this->query($sql);
        return $this->fetchAll();
    }

    public function clientListCache() {
        $sql = "
            SELECT
                `client`,
                `title`,
                `use_in_list`
            FROM `client`
            WHERE `state` = 1
                AND `xid` = $this->XID
            ORDER BY `title`
        ";
        $this->query($sql);

        return $this->fetchAll();
    }

    public function clientByTitle($title) {
        $sql = "
            SELECT `client`
            FROM `client`
            WHERE `title` = '$title'
                AND `xid` = $this->XID
        ";
        $this->query($sql);

        return $this->fetchValue();
    }

    public function clientTitle($clientId) {
        $sql = "
            SELECT `title`
            FROM `client`
            WHERE `client` = $clientId
                AND `xid` = $this->XID
        ";
        $this->query($sql);

        return $this->fetchValue();
    }

    public function clientByCode($code) {
        $sql = "
            SELECT `client`
            FROM `client`
            WHERE `code` = '$code'
                AND xid = $this->XID
            ORDER BY `client` DESC
            LIMIT 1
        ";
        $this->query($sql);
        return $this->fetchValue();
    }

    public function clientInfo($client) {
        $sql = "
            SELECT
                `client`,
                `state`,
                `use_in_list`,   
                `title`,
                `jur_name`,
                `code`,
                `email`,
                `phone`,
                `contact_name`,
                `address`,
                `inn`
            FROM `client`
            WHERE `client` = $client
                AND `xid` = $this->XID
        ";
        $this->query($sql);

        return $this->fetch();
    }

    public function clientAdd($data) {
        $data['xid'] = $this->XID;
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO client
            SET $set
        ";
        $this->query($sql);

        if ($this->success()) {
            return (int) $this->lastid();
        }

        return null;
    }

    public function clientUpdate($client, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE client
            SET $set
            WHERE client = $client
                AND xid = $this->XID
        ";
        $this->query($sql);

        return $this->success();
    }

    public function clientDelete($client) {
        $sql = "
            DELETE
            FROM client
            WHERE client = $client
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    // Client plan

    public function clientPlan($client, $year) {
        $sql = "
            SELECT
                cp.`turnover`,
                cp.`turnoverRate`,
                cp.`amount`
            FROM `client_plan` cp
            WHERE cp.`client` = $client
              AND cp.`year` = $year
        ";
        $this->query($sql);

        return $this->fetch();
    }

    public function clientPlanSet($data) {
        $set = $this->makeSet($data);
        $sql = "
            REPLACE INTO `client_plan`
            SET $set
        ";

        $this->query($sql);

        return $this->success();
    }

    // Client dialog

    public function clientDialogList($client) {
        $sql = "
            SELECT
                cd.`client_dialog`,
                cd.`active`,
                cd.`description`,
                cd.`created`
            FROM `client_dialog` cd
              INNER JOIN `client` c ON (c.`client` = cd.`client`)
            WHERE cd.`client` = $client
              AND c.`xid` = $this->XID
            ORDER BY cd.`client_dialog` DESC 
        ";
        $this->query($sql);

        return $this->fetchAll();
    }

    public function clientDialog($clientDialogId) {
        $sql = "
            SELECT
                cd.`client_dialog`,
                cd.`client`,
                cd.`active`,
                cd.`description`,
                cd.`created`
            FROM `client_dialog` cd
              INNER JOIN `client` c ON (c.`client` = cd.`client`)
            WHERE cd.`client_dialog` = $clientDialogId
              AND c.`xid` = $this->XID

        ";
        $this->query($sql);

        return $this->fetch();
    }

    public function clientDialogAdd($data) {
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO `client_dialog`
            SET $set
        ";

        $this->query($sql);
        if ($this->success()) {
            return (int) $this->lastid();
        }

        return null;
    }

    public function clientDialogUpdate($clientDialogId, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE `client_dialog`
            SET $set
            WHERE `client_dialog` = $clientDialogId
        ";

        $this->query($sql);
        return $this->success();
    }

    public function clientDialogDelete($clientDialogId) {
        $sql = "
            DELETE 
            FROM `client_dialog` 
            WHERE `client_dialog` = $clientDialogId            
        ";

        $this->query($sql);

        return $this->success();
    }

    // Client service plan

    public function clientServicePlanList($client = null, $service = null, $active = false) {
        if ($client) {
            $whereClient = "
                AND csp.`client` = $client
            ";
        } else {
            $whereClient = '';
        }

        if ($service) {
            $whereService = "
                AND csp.`service` = $service
            ";
        } else {
            $whereService = '';
        }

        if ($active) {
            $whereActive = "
                AND csp.`active` = 1
            ";
        } else {
            $whereActive = '';
        }

        $sql = "
            SELECT
                csp.`client_service_plan`,
                csp.`client`,
                csp.`service`,
                csp.`active`,
                csp.`removed`,   
                csp.`description`,
                csp.`created`,
                csp.`period_count`,
                csp.`quantity`,
                csp.`price`,
                csp.`amount`
            FROM `client_service_plan` csp
              INNER JOIN `client` c ON (c.`client` = csp.`client`)
            WHERE c.`xid` = $this->XID
              AND csp.`removed` = 0
              $whereClient
              $whereService
              $whereActive
            ORDER BY csp.`client_service_plan` DESC
        ";
        $this->query($sql);

        return $this->fetchAll();
    }

    public function clientServicePlan($clientServicePlanId) {
        $sql = "
            SELECT
                csp.`client_service_plan`,
                csp.`client`,
                csp.`service`,
                csp.`active`,
                csp.`description`,
                csp.`created`,
                csp.`quantity`,
                csp.`price`,
                csp.`amount`
            FROM `client_service_plan` csp
              INNER JOIN `client` c ON (c.`client` = csp.`client`)
            WHERE csp.`client_service_plan` = $clientServicePlanId
              AND c.`xid` = $this->XID

        ";
        $this->query($sql);

        return $this->fetch();
    }

    public function clientServicePlanAdd($data) {
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO `client_service_plan`
            SET $set
        ";

        $this->query($sql);
        if ($this->success()) {
            return (int) $this->lastid();
        }

        return null;
    }

    public function clientServicePlanUpdate($clientServicePlanId, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE `client_service_plan`
            SET $set
            WHERE `client_service_plan` = $clientServicePlanId
        ";

        $this->query($sql);
        return $this->success();
    }

    // Client doc

    public function clientDocList($client) {
        $sql = "
            SELECT
                cd.`client_doc`,
                cd.`doc`,
                d.`title`,
                d.`ext`,
                d.`created`
            FROM `client_doc` cd
              INNER JOIN `client` c ON (c.`client` = cd.`client`)
              INNER JOIN `doc` d ON (d.`doc` = cd.`doc`)
            WHERE cd.`client` = $client
              AND c.`xid` = $this->XID
            ORDER BY cd.`client_doc` DESC  
        ";
        $this->query($sql);

        return $this->fetchAll();
    }

    public function clientDocInfo($clientDocId) {
        $sql = "
            SELECT
                cd.`client_doc`,
                cd.`client`,   
                cd.`doc`
            FROM `client_doc` cd
              INNER JOIN `client` c ON (c.`client` = cd.`client`)
            WHERE cd.`client_doc` = $clientDocId
              AND c.`xid` = $this->XID
        ";
        $this->query($sql);

        return $this->fetch();
    }

    public function clientDocAdd($client, $doc) {
        $sql = "
            INSERT INTO `client_doc` SET
              `client` = $client,
              `doc` = '$doc' 
        ";

        $this->query($sql);
        if ($this->success()) {
            return (int) $this->lastid();
        }

        return null;
    }

    public function clientDocDelete($clientDocId) {
        $sql = "
            DELETE 
            FROM `client_doc` 
            WHERE `client_doc` = $clientDocId            
        ";

        $this->query($sql);

        return $this->success();
    }

    // >>> prod

    public function prodByCode($code) {
        $sql = "
            SELECT prod
            FROM prod
            WHERE code = '$code'
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetchValue();
    }

    public function prodAdd($data) {
        $data['xid'] = $this->XID;
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO prod
            SET $set
        ";
        $this->query($sql);
        if ($this->success()) {
            return $this->lastid();
        }
        return false;
    }

    public function prodUpdate($prod, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE prod
            SET $set
            WHERE prod = $prod
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    // >>> category

    public function categoryListCache() {
        $sql = "
            SELECT
                c.category,
                c.category_group,
                CONCAT(cg.title, ': ', c.title) AS title
            FROM category c
            INNER JOIN category_group cg ON(cg.category_group = c.category_group)
            ORDER BY title
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function categoryGroupListCache() {
        $sql = "
            SELECT
                cg.category_group,
                cg.title
            FROM category_group cg
            ORDER BY cg.title
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function categoryList($filter, $start, $count, $sort_col, $sort_dir) {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
                c.category,
                c.category_group,
                c.title AS category_title,
                cg.title AS category_group_title
            FROM category c
            INNER JOIN category_group cg ON(cg.category_group = c.category_group)
        ";
        $sql .= $this->qWhere(' AND', $filter);
        $sql .= $this->qOrder($sort_col, $sort_dir);
        $sql .= $this->qLimit($start, $count);

        $this->query($sql);
        return $this->fetchAll();
    }

    public function categoryAdd($data) {
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO `category`
            SET $set
        ";
        $this->query($sql);
        if ($this->success()) {
            return $this->lastid();
        }
        return false;
    }

    public function categoryInfo($category) {
        $sql = "
            SELECT
                category,
                category_group,
                title
            FROM `category`
            WHERE category = $category
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function categoryUpdate($category, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE `category`
            SET $set
            WHERE category = $category
        ";
        $this->query($sql);
        return $this->success();
    }

    // >>> task

    public function taskAdd($data) {
        $data['xid'] = $this->XID;
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO task
            SET $set
        ";
        $this->query($sql);
        if ($this->success()) {
            return $this->lastid();
        }
        return false;
    }

    public function taskUpdate($task, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE task
            SET $set
            WHERE task = $task
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskDelete($taskList) {
        $taskListStr = implode(',', $taskList);
        $sql = "
            DELETE
            FROM task
            WHERE task IN ($taskListStr)
                AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskShopClear($task) {
        $sql = "
            DELETE
            FROM task_shop
            WHERE task = $task
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskShopAdd($task, $shop, $quantity, $price, $days, $description = null) {
        $days = $this->valueOf($days);
        $description = $this->valueOf($description);
        $sql = "
            INSERT
            INTO task_shop
            SET
                task = $task,
                shop = $shop,
                quantity = $quantity,
                price = $price,
                days = $days,
                description = $description
            ";
        $this->query($sql);
        return $this->success();
    }

    public function taskPlanAdd($data) {
        $set = $this->makeSet($data);
        $sql = "
            INSERT
            INTO task_plan
            SET $set
        ";
        $this->query($sql);
        if ($this->success()) {
            return $this->lastid();
        }
        return false;
    }

    public function taskPlanList($task) {
        $sql = "
          SELECT
            p.`plan`,
            p.`prod`,
            p.`title`,
            p.`quantity`,
            p.`price`,
            r.`tm`,
            (
                SELECT GROUP_CONCAT(pc.`category` SEPARATOR ',') 
                FROM `plan_category` pc
                WHERE pc.`plan` = p.`plan` 
            ) AS category_list
          FROM `task_plan` p
          INNER JOIN `prod` r ON(r.`prod` = p.`prod`)
          WHERE p.`task` = $task
            AND r.`xid` = $this->XID
          ORDER BY r.`tm`, p.`plan`
        ";

        $this->query($sql);
        return $this->fetchAll();
    }

    public function taskPlanUpdate($task, $plan, $data) {
        $set = $this->makeSet($data);
        $sql = "
            UPDATE task_plan
            SET $set
            WHERE task = $task
                AND plan = $plan
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskPlanCategoryClear($plan) {
        $plan = (int) $plan;
        $sql = "
            DELETE 
            FROM `plan_category`
            WHERE `plan` = $plan
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskPlanCategoryAdd($plan, $category) {
        $plan = (int) $plan;
        $category = (int) $category;
        $sql = "
            INSERT IGNORE INTO `plan_category` SET
              `plan` = $plan,
              `category` = $category
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskUpdateByPlan($task) {
        $sql = "
            UPDATE task
            SET
              price = GetTaskPlanPrice(task),
              quantity = GetTaskPlanQuantity(task),
              amount = GetTaskPlanAmount(task)
            WHERE task = $task
              AND xid = $this->XID
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskInfo($task) {
        $sql = "
            SELECT
                t.task,
                t.service,
                t.client,
                t.title,
                t.description,
                t.maket,
                t.period_start,
                t.period_end,
                t.quantity,
                t.price,
                t.amount,
                t.payment,
                t.service_type,
                t.have_alcohol,
                t.color,
                GetTaskDocList(t.task) AS doc_list
            FROM task t
            WHERE t.task = $task
                AND t.xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function taskFullInfo($task) {
        $sql = "
            SELECT
                t.task,
                t.service,
                t.client,
                t.title,
                t.maket,
                t.period_start,
                t.period_end,
                t.quantity,
                t.price,
                t.amount,
                t.payment,
                t.have_alcohol,
                c.title AS client_title,
                IF(c.jur_name <> '', c.jur_name, c.title) AS client_jur_name,
                s.period_type
            FROM task t
            INNER JOIN client c ON(c.client = t.client)
            INNER JOIN service s ON(s.service = t.service)
            WHERE t.task = $task
                AND t.xid = $this->XID
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function taskShopInfo($task) {
        $sql = "
            SELECT
                shop,
                quantity,
                price,
                days,
                description
            FROM task_shop
            WHERE task = $task
            ORDER BY shop
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function taskPeriodCheck($task, $period) {
        $sql = "
            INSERT IGNORE
            INTO task_period_check
            SET
                task = $task,
                period = '$period'
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskPeriodUncheck($task, $period) {
        $sql = "
            DELETE
            FROM task_period_check
            WHERE task = $task
                AND period = '$period'
        ";
        $this->query($sql);
        return $this->success();
    }

    public function taskPeriodInfo($task) {
        $sql = "
            SELECT
                GROUP_CONCAT(ts.shop ORDER BY ts.shop SEPARATOR ',') AS shop_list,
                GROUP_CONCAT(DISTINCT s.district ORDER BY s.district SEPARATOR ',') AS district_list,
                GROUP_CONCAT(DISTINCT r.title ORDER BY r.title SEPARATOR ',') AS region_list
            FROM task_shop ts
            INNER JOIN shop s ON(s.shop = ts.shop)
            INNER JOIN town t ON(t.town = s.town)
            INNER JOIN region r ON(r.region = t.region)
            WHERE ts.task = $task
        ";
        $this->query($sql);
        return $this->fetch();
    }

    public function periodClientList($period, $onlyCurPeriod = false) {
        $sql = "
            SELECT DISTINCT
                t.client,
                c.title
            FROM task t
            INNER JOIN client c ON(c.client = t.client)
            WHERE t.period_end >= '$period'
                AND t.xid = $this->XID

        ";
        if ($onlyCurPeriod) {
            $sql .= "
                AND t.period_start >= '$period'
            ";
        }
        $sql .= "
            ORDER BY c.title
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function serviceTaskList($service, $period, $periodCount, $filter, $start, $count, $sort_col, $sort_dir) {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
                c.client,
                c.title AS client_title,
                c.code,
                t.task,
                t.title,
                t.description,
                t.maket,
                t.period_start,
                t.period_end,
                t.color,
                t.price,
                t.quantity,
                t.amount,
                t.payment,
                GetTaskDocList(t.task) AS doc_list
        ";
        $period = Period::startOfTheMonth($period);
        for ($i = 0; $i < $periodCount; $i++) {
            $p = Period::incMonth($period, $i);
            $sql .= "
                ,CONCAT_WS(_utf8mb4';', GetTaskPeriodChargeCount(t.task, '$p', '$p'), GetTaskPeriodCheck(t.task, '$p')) AS period_{$i}
            ";
        }

        $periodEnd = Period::incMonth($period, $periodCount - 1);

        $sql .= "
            FROM task t
            INNER JOIN client c ON(c.client = t.client)
            WHERE t.service = $service
                AND t.xid = $this->XID
                AND t.period_start <= '$periodEnd'
                AND t.period_end > '$period'
        ";
        $sql .= $this->qWhere(' AND', $filter);
        $sql .= $this->qOrder($sort_col, $sort_dir);
        $sql .= $this->qLimit($start, $count);

        $this->query($sql);

        $this->Loger->log($this->LogLevel, 'query: ' . $sql);

        return $this->fetchAll();
    }

    // >>> task category

    public function getTaskCategories($task) {
        $sql = "
            SELECT tc.category
            FROM task_category tc 
            WHERE tc.task = $task
        ";
        $this->query($sql);

        if ($value = $this->fetchAllRec()) {
            return $value;
        }

        return array();
    }

    public function taskCategoryAdd($task, $category) {
        $sql = "
            INSERT IGNORE INTO task_category SET
                task = $task,
                category = $category    
        ";
        $this->query($sql);

        return $this->success();
    }

    public function taskCategoryClear($task) {
        $sql = "
            DELETE 
            FROM task_category
            WHERE task = $task
        ";
        $this->query($sql);

        return $this->success();
    }

    public function getTaskCategoryTitles($task) {
        $sql = "
            SELECT c.`title`
            FROM `task_category` tc
            INNER JOIN `category` c ON(c.`category` = tc.`category`) 
            WHERE tc.`task` = $task
        ";
        $this->query($sql);

        if ($value = $this->fetchAllRec()) {
            return $value;
        }

        return array();
    }

    // >>> task_doc

    public function taskDocAdd($task, $doc) {
        $sql = "
            INSERT IGNORE INTO task_doc SET
                task = $task,
                doc = '$doc'    
        ";
        $this->query($sql);

        return $this->success();
    }

    public function taskDocClear($task) {
        $sql = "
            DELETE 
            FROM task_doc
            WHERE task = $task
        ";
        $this->query($sql);

        return $this->success();
    }

    // >>> maket

    public function maketSearch($fhash) {
        $sql = "
            SELECT
                maket
            FROM maket
            WHERE fhash = '$fhash'
        ";
        $this->query($sql);
        return $this->fetchValue();
    }

    public function maketAdd($fhash, $fsize) {
        $sql = "
            INSERT
            INTO maket
            SET fhash = '$fhash',
                fsize = $fsize
        ";
        $this->query($sql);
        if ($this->success()) {
            return $this->lastid();
        }

        return false;
    }

    // REPORTS

    public function taskClientListByServicePeriod($periodStart, $periodEnd, $service = null, $client = null, $serviceType = null) {
        $w = '';

        if ($client) {
            $w .= "
                AND t.client = $client
            ";
        }

        if ($service) {
            $w .= "
                AND t.service = $service
            ";
        }

        if ($serviceType) {
            $w .= "
                AND t.service_type = '$serviceType'
            ";
        }

        $sql = "
            SELECT
                t.task,
                t.client,
                t.service,
                t.title,
                COUNT(tc.period) AS charge_count,                
                t.price,
                t.quantity,
                t.amount,
                t.payment,
                c.title AS client_title,
                c.code AS client_code,
                c.inn AS client_inn,
                GROUP_CONCAT(tc.period ORDER BY tc.period SEPARATOR ',') AS period_list,
                t.period_start,
                t.period_end
            FROM task_period_charge tc
                INNER JOIN task t ON(t.task = tc.task)
                INNER JOIN client c ON(c.client = t.client)
            WHERE t.xid = $this->XID
                AND tc.period BETWEEN '$periodStart' AND '$periodEnd'
                $w
            GROUP BY tc.task
            ORDER BY client_title, t.task
        ";
        $this->query($sql);
        return $this->fetchAll();
    }

    public function clientServiceList($clients, $services, $periodStart, $periodCount, $havePayment = false, $serviceType = false) {
        $sql = "
            SELECT
                c.client,
                c.title AS client_title,
                t.task,
                t.title,
                t.service,
                t.payment,
                s.title AS service_title
        ";
        $period = Period::startOfTheMonth($periodStart);
        for ($i = 0; $i < $periodCount; $i++) {
            $p = Period::incMonth($period, $i);
            $sql .= "
                ,GetTaskPeriodChargeCount(t.task, '$p', '$p') * t.amount AS period_{$i}
            ";
        }

        $periodEnd = Period::incMonth($period, $periodCount - 1);

        $sql .= "
            FROM task_period_charge tc
            INNER JOIN task t ON(t.task = tc.task)
            INNER JOIN client c ON(c.client = t.client)
            INNER JOIN service s ON(s.service = t.service)
            WHERE tc.period BETWEEN '$period' AND '$periodEnd'
                AND t.xid = $this->XID
        ";

        if ($services) {
            $sql .= "
                AND t.service IN (" . implode(',', $services) . ")
            ";
        }
        if ($havePayment) {
            $sql .= "
                AND s.have_payment = 1
            ";
        }
        if ($clients) {
            $sql .= "
                AND t.client IN (" . implode(',', $clients) . ")
            ";
        }
        if ($serviceType) {
            $sql .= "
                AND (s.service_type <> 'C' OR t.service_type = '$serviceType')
            ";
        }

        $sql .= "
            GROUP BY client_title, t.service, t.task
        ";

        $this->query($sql);
        return $this->fetchAll();
    }

    public function serviceClientList($periodStart, $periodCount, $service = null, $client = null, $serviceType = null) {

        $period = Period::startOfTheMonth($periodStart);
        $periodEnd = Period::incMonth($period, $periodCount - 1);

        $w = '';

        if ($service) {
            $w .= "
                AND t.service = $service
            ";
        }

        if ($serviceType) {
            $w .= "
                AND t.service_type = '$serviceType'
            ";
        }

        $sql = "
            SELECT
                c.client,
                c.title
        ";

        for ($i = 0; $i < $periodCount; $i++) {
            $p = Period::incMonth($period, $i);
            $sql .= "
                ,(
                  SELECT SUM(IFNULL(t.payment, t.amount))
                  FROM task t
                  INNER JOIN task_period_charge _tc ON(_tc.task = t.task AND _tc.period = '$p')
                  WHERE t.client = c.client
                  $w
                ) AS period_{$i}
            ";
        }

        $sql .= "
            FROM client c
        ";

        if ($client) {
            $sql .= "
                WHERE c.client = $client
                    AND c.xid = $this->XID
            ";
        } else {
            $sql .= "
                WHERE c.client IN (
                  SELECT t.client
                    FROM task_period_charge tc
                    INNER JOIN task t ON(t.task = tc.task)
                    WHERE t.service = $service
                        AND t.xid = $this->XID
                        $w    
                        AND tc.period BETWEEN '$period' AND '$periodEnd'
                )
                ORDER BY c.title
            ";
        }

        $this->query($sql);

        return $this->fetchAll();
    }

    public function taskListByServicePeriod($service, $period) {
        $sql = "
            SELECT
                t.task,
                t.client,
                t.title,
                c.title AS client_title,
                GROUP_CONCAT(ts.shop ORDER BY ts.shop SEPARATOR ',') AS shops
            FROM task t
                INNER JOIN task_shop ts ON(ts.task = t.task)
                INNER JOIN service s ON(s.service = t.service)
                INNER JOIN client c ON(c.client = t.client)
            WHERE t.service = $service
                AND t.xid = $this->XID
                AND DATE_FORMAT(t.period_start, '%Y-%m-01') <= '$period'
                AND t.period_end >= '$period'
            GROUP BY ts.task
        ";

/*
 *
 *
 *                 AND EXISTS (
                  SELECT 1
                  FROM task_period_charge tc
                  WHERE tc.task = t.task
                    AND tc.period = '$period'
                )
 *
 */

        $this->query($sql);
        return $this->fetchAll();
    }

    public function taskListByServicePeriodInfo($service, $period, $taskList = null) {
        if ($taskList) {
            $whereTaskList = "
                AND t.`task` IN (".implode(',', $taskList).")
            ";
        } else {
            $whereTaskList = '';
        }

        $sql = "
            SELECT t.*
            FROM task t
            WHERE t.service = $service
                AND t.xid = $this->XID
                $whereTaskList
                AND EXISTS (
                  SELECT 1
                  FROM task_period_charge tc
                  WHERE tc.task = t.task
                    AND tc.period = '$period'
                )
        ";
        $this->query($sql);

        return $this->fetchAll();
    }

    public function taskShopListServicePeriod($period, $service, $client, $shop) {
        $sql = "
            SELECT
                ts.task,
                ts.shop,
                ts.days,
                t.title,
                t.color
            FROM task_shop ts
            INNER JOIN task t ON(t.task = ts.task)
            WHERE
                '$period' BETWEEN t.period_start AND t.period_end
                AND t.xid = $this->XID
        ";
        if ($service) {
            $sql .= "
                AND t.service = $service
            ";
        }
        if ($client) {
            $sql .= "
                AND t.client = $client
            ";
        }
        if ($shop) {
            $sql .= "
                AND ts.shop = $shop
            ";
        }
        $sql .= "
            ORDER BY ts.shop, ts.task
        ";

        $this->query($sql);
        return $this->fetchAll();
    }

    public function taskShopListPromoPeriod($period, $service, $client, $shop) {
        $sql = "
            SELECT
                ts.task,
                ts.shop,
                ts.days,
                t.title,
                t.color,
                tw.title AS town_title,
                s.address AS shop_address
            FROM task_shop ts
            INNER JOIN task t ON(t.task = ts.task)
            INNER JOIN shop s ON(ts.shop = s.shop)
            INNER JOIN town tw ON(tw.town = s.town)
            WHERE
                '$period' BETWEEN t.period_start AND t.period_end
                AND t.xid = $this->XID
        ";
        if ($service) {
            $sql .= "
                AND t.service = $service
            ";
        }
        if ($client) {
            $sql .= "
                AND t.client = $client
            ";
        }
        if ($shop) {
            $sql .= "
                AND ts.shop = $shop
            ";
        }
        $sql .= "
            ORDER BY ts.shop, ts.task
        ";

        $this->query($sql);
        return $this->fetchAll();
    }

    public function reportShopTaskList($shop, $period, $filter, $start, $count, $sort_col, $sort_dir) {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
                s.service,
                s.title AS service_title,
                c.client,
                c.title AS client_title,
                c.code,
                t.task,
                t.title,
                t.maket,
                t.period_start,
                t.period_end,
                ts.quantity,
                ts.description
            FROM task_shop ts
            INNER JOIN task t ON(t.task = ts.task)
            INNER JOIN client c ON(c.client = t.client)
            INNER JOIN service s ON(s.service = t.service)
            WHERE ts.shop = $shop
                AND t.xid = $this->XID
                AND DATE_FORMAT(t.period_start, '%Y-%m-01') <= '$period'
                AND t.period_end >= '$period'
                AND s.shop_access = 1
        ";
        $sql .= $this->qWhere(' AND', $filter);
        $sql .= $this->qOrder($sort_col, $sort_dir);
        $sql .= $this->qLimit($start, $count);

        $this->query($sql);

        return $this->fetchAll();
    }

    public function reportTaskPlan($service, $periodStart, $periodEnd, $client) {
        $sql = "
            SELECT
                tp.plan,
                tp.task,
                tp.prod,
                tp.title AS plan_title,
                tp.quantity,
                tp.price,
                tp.date_entry,
                c.title AS client_title,
                t.title AS task_title,
                p.code AS prod_code,
                p.title AS prod_title
            FROM task_plan tp
            INNER JOIN task t ON(t.task = tp.task)
            INNER JOIN prod p ON(p.prod = tp.prod)
            INNER JOIN client c ON(c.client = t.client)
            WHERE t.service = $service    
                AND DATE_FORMAT(t.period_start, '%Y-%m-01') <= '$periodEnd'
                AND t.period_end >= '$periodStart'

        ";
        if ($client) {
            $sql .= "
                AND t.client = $client
            ";
        }
        $this->query($sql);

        return $this->fetchAll();
    }

    // >>> copy task

    public function copyTaskShop($task, $taskNew) {
        $sql = "
            INSERT INTO task_shop(task, shop, quantity, price, days, description) (
                SELECT $taskNew, shop, quantity, price, days, description
                FROM task_shop
                WHERE task = $task
            )
        ";
        $this->query($sql);

        return $this->success();
    }

    public function copyTaskPlan($task, $taskNew) {
        $sql = "
            INSERT INTO task_plan(task, prod, title, quantity, price, date_entry) (
                SELECT $taskNew, prod, title, quantity, price, date_entry
                FROM task_plan
                WHERE task = $task
            )
        ";
        $this->query($sql);

        return $this->success();
    }

    public function copyTaskDoc($task, $taskNew) {
        $sql = "
            INSERT INTO task_doc(task, doc) (
                SELECT $taskNew, doc
                FROM task_doc
                WHERE task = $task
            )
        ";
        $this->query($sql);

        return $this->success();
    }

    // Stat

    public function taskStat($periodStart, $periodEnd, $client = null, $service = null, $serviceType = null) {
        if ($client) {
            $whereClient = "
                AND t.`client` = $client
            ";
        } else {
            $whereClient = '';
        }

        if ($service) {
            $whereService = "
                AND t.`service` = $service
            ";
        } else {
            $whereService = '';
        }

        if ($serviceType) {
            $whereServiceType = "
                AND s.`service_type` = '$serviceType'
            ";
        } else {
            $whereServiceType = '';
        }

        $sql = "
            SELECT 
              t.`service`, 
              t.`client`, 
              SUM(t.`quantity`) AS total_quantity, 
              SUM(IFNULL(t.`payment`, t.`amount`)) AS total_amount
            FROM `task` t
            INNER JOIN `service` s ON (s.`service` = t.`service`)
            WHERE t.`xid` = $this->XID 
              AND t.`period_start` >= '$periodStart' 
              AND t.`period_end` <= '$periodEnd'
              $whereClient
              $whereService
              $whereServiceType
            GROUP BY t.`service`, t.`client`
        ";

        $this->query($sql);
        return $this->fetchAll();
    }

    public function taskHistory($periodStart, $periodEnd, $client = null, $service = null, $serviceType = null) {
        $where = '';
        if ($client) {
            $where .= "
                AND t.`client` = $client
            ";
        }
        if ($service) {
            $where .= "
                AND t.`service` = $service
            ";
        }

        if ($serviceType) {
            $where .= "
                AND s.`service_type` = '$serviceType'
            ";
        }

        $sql = "
            SELECT
              tc.`period`, 
              tc.`task`,
              t.`service`, 
              t.`client`,
              t.`title`,
              t.`period_start`,
              t.`period_end`,
              t.`description`, 
              t.`quantity`,
              t.`price`, 
              t.`amount`,
              t.`payment`,
              GetTaskPeriodCheck(t.`task`, tc.`period`) AS `task_period_check`
            FROM `task_period_charge` tc
              INNER JOIN `task` t ON(t.`task` = tc.`task`)
              INNER JOIN `service` s ON(s.`service` = t.`service`)
            WHERE t.`xid` = $this->XID 
              AND tc.`period` BETWEEN '$periodStart' AND '$periodEnd'
              $where
            ORDER BY tc.`period`, tc.`task`
        ";

        $this->query($sql);

        return $this->fetchAll();
    }

    // Budget

    public function budgetGet($year) {
        $sql = "
            SELECT
                b.budgetData
            FROM budget b
            WHERE b.`xid` = $this->XID
              AND b.`year` = $year
        ";
        $this->query($sql);

        return $this->fetchValue();
    }

    public function budgetSet($year, $data) {
        $sql = "
            REPLACE INTO `budget` SET
              `xid` = $this->XID,
              `year` = $year,
              `budgetData` = '$data'
        ";
        $this->query($sql);

        return $this->success();
    }

    // Budget plan

    public function budgetPlanList($year) {
        $sql = "
            SELECT
                `budget_plan`,
                `year`,
                `title`
            FROM `budget_plan`
            WHERE `xid` = $this->XID
              AND `year` = $year
            ORDER BY `budget_plan`
        ";
        $this->query($sql);

        return $this->fetchAll();
    }

    public function budgetPlan($budgetPlanId) {
        $sql = "
            SELECT
                `budget_plan`,
                `year`,
                `title`
            FROM `budget_plan`
            WHERE `xid` = $this->XID
              AND `budget_plan` = $budgetPlanId
        ";
        $this->query($sql);

        return $this->fetch();
    }

    public function budgetPlanData($budgetPlanId) {
        $sql = "
            SELECT
                `service`,
                `client`,
                `data`
            FROM `budget_plan_data`
            WHERE `budget_plan` = $budgetPlanId
            ORDER BY `service`, `client`
        ";
        $this->query($sql);

        return $this->fetchAll();
    }

    public function budgetPlanAdd($year, $title) {
        $sql = "
            INSERT INTO `budget_plan` SET
                `xid` = $this->XID,
                `year` = $year,
                `title` = '$title'
        ";
        $this->query($sql);

        return (int) $this->lastid();
    }

    public function budgetPlanUpdate($budgetPlanId, $title) {
        $sql = "
            UPDATE `budget_plan` SET
                `title` = '$title'
            WHERE `budget_plan` = $budgetPlanId  
        ";
        $this->query($sql);

        return $this->success();
    }

    public function budgetPlanDataClear($budgetPlanId) {
        $sql = "
            DELETE 
            FROM `budget_plan_data`
            WHERE `budget_plan` = $budgetPlanId  
        ";
        $this->query($sql);

        return $this->success();
    }

    public function budgetPlanDelete($budgetPlanId) {
        $sql = "
            DELETE 
            FROM `budget_plan`
            WHERE `budget_plan` = $budgetPlanId  
        ";
        $this->query($sql);

        return $this->success();
    }

    public function budgetPlanDataAdd($budgetPlanId, $service, $client, $data) {
        $sql = "
            INSERT INTO `budget_plan_data` SET
                `budget_plan` = $budgetPlanId,
                `service` = $service,
                `client` = $client,
                `data` = '$data'
        ";
        $this->query($sql);

        return $this->success();
    }
}
