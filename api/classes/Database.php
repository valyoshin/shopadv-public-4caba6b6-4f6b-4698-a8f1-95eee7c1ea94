<?php

//
// Class: Database
// MySQL database client api
// Copyright (c) 2012-2014 Alyoshin Vitaliy <master@ap-soft.com>
//

defined('IN_SITE') or die();

require_once('utils.php');

class Database {

    protected $connection_id = false;
    protected $result_id = null;
    private $last_sql = '';
    private $connect_error = false;
    // connect options
    public $Host = 'localhost';
    public $Port = 3306;
    public $DBUser = 'root';
    public $DBPassword = '';
    public $DBName = 'mysql';
    // encodings
    public $ClientEncoding = 'utf8';
    public $ServerEncoding = 'utf8';
    public $Collation = 'cp1251_general_ci';
    // data preparing config
    public $DataNull = 'NULL';
    public $DataDefault = 'DEFAULT';
    public $DataBoolTrue = "'Y'";
    public $DataBoolFalse = "'N'";


    function __construct($dbname, $dbhost, $dbuser, $dbpassword, $dbport = 3306) {
        $this->Host = $dbhost;
        $this->Port = $dbport;
        $this->DBName = $dbname;
        $this->DBUser = $dbuser;
        $this->DBPassword = $dbpassword;
    }

    function __destruct() {
        $this->disconnect();
    }

    // init

    public function setEncoding($clientEncoding, $serverEncoding, $collation) {
        $this->ClientEncoding = $clientEncoding;
        $this->ServerEncoding = $serverEncoding;
        $this->Collation = $collation;
    }

    // connection

    public function isConnected() {
        return !!$this->connection_id;
    }

    public function connect() {
        $this->connect_error = false;

        $this->connection_id = mysqli_connect($this->Host, $this->DBUser, $this->DBPassword, '', $this->Port);

        if (!$this->isConnected()) {
            $this->connect_error = 'Can not connect: ' . mysqli_connect_error();
            return false;
        }

        if (!mysqli_select_db($this->connection_id, $this->DBName)) {
            $this->connect_error = 'Can not select DB ' . $this->DBName . ': ' . mysqli_error($this->connection_id);
            $this->disconnect();
            return false;
        }

        mysqli_query($this->connection_id, "SET character_set_client='" . $this->ClientEncoding . "'");
        mysqli_query($this->connection_id, "SET character_set_results='" . $this->ServerEncoding . "'");
        mysqli_query($this->connection_id, "SET collation_connection='" . $this->Collation . "'");

        return true;
    }

    public function disconnect() {
        if ($this->isConnected()) {
            $this->free();
            mysqli_close($this->connection_id);
            $this->connection_id = false;
        }
    }

    // database

    public function getDatabase() {
        if (!$this->query('SELECT DATABASE()')) {
            return false;
        }
        return $this->fetchValue();
    }

    public function setDatabase($dbname) {
        $this->query('USE ' . $dbname);
        return $this->success();
    }

    // queries and fetching

    public function query($sql) {
        if ($this->isConnected() || $this->connect()) {
            $this->last_sql = $sql;
            $this->free();
            $result = mysqli_query($this->connection_id, $sql);

            if (is_bool($result)) {
                return $result;
            }

            $this->result_id = $result;

            return true;
        } else {
            return false;
        }
    }

    public function rowsCount() {
        return $this->result_id ? mysqli_num_rows($this->result_id) : null;
    }

    public function affectedRowsCount() {
        return mysqli_affected_rows($this->connection_id);
    }

    public function fetch($rt = MYSQLI_ASSOC) {
        return $this->result_id ? mysqli_fetch_array($this->result_id, $rt) : null;
    }

    public function fetchAll($rt = MYSQLI_ASSOC) {
        while ($row = $this->fetch($rt)) {
            $res[] = $row;
        }

        return isset($res) ? $res : false;
    }

    public function fetchAllRec($index = 0) {
        while ($row = $this->fetch(MYSQLI_NUM)) {
            $res[] = $row[$index];
        }

        return isset($res) ? $res : false;
    }

    public function fetchAllSet($index = 0) {
        while ($row = $this->fetch(MYSQLI_NUM)) {
            $res[$row[$index]] = 1;
        }

        return isset($res) ? $res : false;
    }

    public function fetchAllRecAssoc($index = 0) {
        if (is_int($index)) {
            $rt = MYSQLI_NUM;
        } else {
            $rt = MYSQLI_ASSOC;
        }
        while ($row = $this->fetch($rt)) {
            $res[$row[$index]] = $row;
        }

        return isset($res) ? $res : false;
    }

    public function fetchValue($index = 0) {
        if (!$row = $this->fetch(MYSQLI_NUM)) {
            return false;
        }

        return $row[$index];
    }

    public function free() {
        if ($this->result_id) {
            @mysqli_free_result($this->result_id);
            $this->result_id = null;
        }
    }

    public function next($res = false, $rt = MYSQLI_ASSOC) {
        $row = ($res) ? mysqli_fetch_array($res, $rt) : mysqli_fetch_array($this->result_id, $rt);
        if (!$row) {
            $this->free();
        }
        return $row;
    }

    public function lastid() {
        return mysqli_insert_id($this->connection_id);
    }

    // errors

    public function error() {
        if($this->isConnected()) {
            return mysqli_error($this->connection_id);
        }
        if($this->connect_error) {
            return $this->connect_error;
        }
        return mysqli_connect_error();
    }

    public function errno() {
        return $this->isConnected() ? mysqli_errno($this->connection_id) : mysqli_connect_errno();
    }

    public function fullError() {
        return $this->error() . "\n" . $this->getLastSQL() . "\n";
    }

    public function getLastSQL() {
        return $this->last_sql;
    }

    public function success() {
        return !$this->errno();
    }

    public function failure() {
        return !$this->success();
    }

    // preparing data

    public function asStr($text, $maxlen = 0) {
        return Utils::asStr($text, $maxlen);
    }

    public function asStrEsc($str) {
        return mysqli_real_escape_string($this->connection_id, $str);
    }

    public function asInt($val) {
        return Utils::asInt($val);
    }

    public function asFloat($val) {
        return Utils::asFloat($val);
    }

    public function asLine($val, $maxlen = 0) {
        return str_replace("\n", '', $this->asStr($val, $maxlen));
    }

    public function like($val) {
        return str_replace(array('%', '_', '*', '?'), array('\%', '\_', '%', '_'), $val);
    }

    public function valueOrNull($val) {
        return ($val) ? $val : $this->DataNull;
    }

    public function strOrNull($str) {
        return ($str) ? "'" . $str . "'" : $this->DataNull;
    }

    public function valueOrDefault($val) {
        return ($val) ? $val : $this->DataDefault;
    }

    public function strOrDefault($str) {
        return ($str) ? "'" . $str . "'" : $this->DataDefault;
    }

    public function valueOf($val) {
        if (is_object($val)) {
            return '';
        }
        if (is_array($val)) {
            return "'" . implode(',', $val) . "'";
        }
        if (is_null($val)) {
            return $this->DataNull;
        }
        if (is_string($val)) {
            return "'" . $val . "'";
        }
        if (is_bool($val)) {
            if ($val) {
                return $this->DataBoolTrue;
            } else {
                return $this->DataBoolFalse;
            }
        }
        return $val;
    }

    // transform Array like: ($field1 => $value1, $field2 => $value2, ..., $fieldN => $valueN)
    // to SQL-string like: $field1=$value1,$field2=$value2,...,$fieldN=$valueN
    // Values transform using "valueOf" function
    public function makeSetArr($data, $alias = '') {
        $r = array();
        if (!is_array($data)) {
            return $r;
        }
        if ($alias) {
            $alias .= '.';
        }
        foreach ($data as $f => $v) {
            $r[] = $alias . $f . '=' . $this->valueOf($v);
        }
        return $r;
    }

    // transform Array like: ($field1 => $value1, $field2 => $value2, ..., $fieldN => $valueN)
    // to SQL-string like: $value1,$value2,...,$valueN ordered and indexed like $fields
    // Values transform using "valueOf" function
    public function makeListArr($data, $fields) {
        $r = array();
        if (!is_array($data)) {
            return $r;
        }
        foreach ($fields as $f) {
            if (isset($data[$f])) {
                $r[] = $this->valueOf($data[$f]);
            } else {
                $r[] = $this->DataNull;
            }
        }
        return $r;
    }

    public function makeSet($data, $alias = '') {
        return implode(',', $this->makeSetArr($data, $alias));
    }

    public function makeList($data, $fields) {
        return implode(',', $this->makeListArr($data, $fields));
    }

    public function foundRows() {
        $res = mysqli_query($this->connection_id, 'SELECT FOUND_ROWS()');
        if (!$res) {
            return false;
        }
        $r = mysqli_fetch_array($res);
        return ($r) ? (int) $r[0] : false;
    }

    public function startTransaction()
    {
        $this->query('START TRANSACTION');
    }

    public function commit()
    {
        $this->query('COMMIT');
    }

    public function rollback()
    {
        $this->query('ROLLBACK');
    }
}
