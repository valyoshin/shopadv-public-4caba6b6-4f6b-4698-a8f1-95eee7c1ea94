<?php

//
// Class: Utils (static methods)
// Conversion and other usefull functions
// Copyright (c) 2012-2013 Alyoshin Vitaliy <master@ap-soft.com>
//

defined('IN_SITE') or die();

require_once('period.php');

// устанавливаем кодировку
mb_internal_encoding('UTF-8');

final class Utils {
    /* Charsets */

    static public function convertUTF8ToCP1251($str) {
        static $table = array(
            "\xD0\x81" => "\xA8", // Ё
            "\xD1\x91" => "\xB8", // ё
            // украинские символы
            "\xD0\x8E" => "\xA1", // Ў (У)
            "\xD1\x9E" => "\xA2", // ў (у)
            "\xD0\x84" => "\xAA", // Є (Э)
            "\xD0\x87" => "\xAF", // Ї (I..)
            "\xD0\x86" => "\xB2", // I (I)
            "\xD1\x96" => "\xB3", // i (i)
            "\xD1\x94" => "\xBA", // є (э)
            "\xD1\x97" => "\xBF", // ї (i..)
            // чувашские символы
            "\xD3\x90" => "\x8C", // ? (А)
            "\xD3\x96" => "\x8D", // ? (Е)
            "\xD2\xAA" => "\x8E", // ? (С)
            "\xD3\xB2" => "\x8F", // ? (У)
            "\xD3\x91" => "\x9C", // ? (а)
            "\xD3\x97" => "\x9D", // ? (е)
            "\xD2\xAB" => "\x9E", // ? (с)
            "\xD3\xB3" => "\x9F", // ? (у)
        );
        return preg_replace('#([\xD0-\xD1])([\x80-\xBF])#se', 'isset($table["$0"]) ? $table["$0"] :chr(ord("$2")+("$1" == "\xD0" ? 0x30 : 0x70))', $str);
    }

    static public function convertCP1251ToUTF8($str) {
        $in = array(
            chr(208), chr(192), chr(193), chr(194),
            chr(195), chr(196), chr(197), chr(168),
            chr(198), chr(199), chr(200), chr(201),
            chr(202), chr(203), chr(204), chr(205),
            chr(206), chr(207), chr(209), chr(210),
            chr(211), chr(212), chr(213), chr(214),
            chr(215), chr(216), chr(217), chr(218),
            chr(219), chr(220), chr(221), chr(222),
            chr(223), chr(224), chr(225), chr(226),
            chr(227), chr(228), chr(229), chr(184),
            chr(230), chr(231), chr(232), chr(233),
            chr(234), chr(235), chr(236), chr(237),
            chr(238), chr(239), chr(240), chr(241),
            chr(242), chr(243), chr(244), chr(245),
            chr(246), chr(247), chr(248), chr(249),
            chr(250), chr(251), chr(252), chr(253),
            chr(254), chr(255)
        );

        $out = array(
            chr(208) . chr(160), chr(208) . chr(144), chr(208) . chr(145),
            chr(208) . chr(146), chr(208) . chr(147), chr(208) . chr(148),
            chr(208) . chr(149), chr(208) . chr(129), chr(208) . chr(150),
            chr(208) . chr(151), chr(208) . chr(152), chr(208) . chr(153),
            chr(208) . chr(154), chr(208) . chr(155), chr(208) . chr(156),
            chr(208) . chr(157), chr(208) . chr(158), chr(208) . chr(159),
            chr(208) . chr(161), chr(208) . chr(162), chr(208) . chr(163),
            chr(208) . chr(164), chr(208) . chr(165), chr(208) . chr(166),
            chr(208) . chr(167), chr(208) . chr(168), chr(208) . chr(169),
            chr(208) . chr(170), chr(208) . chr(171), chr(208) . chr(172),
            chr(208) . chr(173), chr(208) . chr(174), chr(208) . chr(175),
            chr(208) . chr(176), chr(208) . chr(177), chr(208) . chr(178),
            chr(208) . chr(179), chr(208) . chr(180), chr(208) . chr(181),
            chr(209) . chr(145), chr(208) . chr(182), chr(208) . chr(183),
            chr(208) . chr(184), chr(208) . chr(185), chr(208) . chr(186),
            chr(208) . chr(187), chr(208) . chr(188), chr(208) . chr(189),
            chr(208) . chr(190), chr(208) . chr(191), chr(209) . chr(128),
            chr(209) . chr(129), chr(209) . chr(130), chr(209) . chr(131),
            chr(209) . chr(132), chr(209) . chr(133), chr(209) . chr(134),
            chr(209) . chr(135), chr(209) . chr(136), chr(209) . chr(137),
            chr(209) . chr(138), chr(209) . chr(139), chr(209) . chr(140),
            chr(209) . chr(141), chr(209) . chr(142), chr(209) . chr(143)
        );

        return str_replace($in, $out, $str);
    }

    /* File system and loging */

    static public function logFile($str, $filename = 'log') {
        $f = @fopen($filename, 'a');
        if (!$f) {
            return;
        }
        fwrite($f, $str . "\n");
        fclose($f);
    }

    static public function removePath($path) {
        if (is_dir($path)) {
            $files = glob($path . '*', GLOB_MARK);
            if ($files && is_array($files)) {
                foreach ($files as $file) {
                    if (substr($file, -1) === '/') {
                        self::removePath($file);
                    } else {
                        @unlink($file);
                    }
                }
            }
            try {
                rmdir($path);
            } catch (Exception $e) {

            }
        }
    }

    /* color conversions */

    static public function uintToHexColor($data) {
        $data = dechex($data);
        if (strlen($data) > 6) {
            $data = substr($data, 0, 6);
        }
        while (strlen($data) < 6) {
            $data = '0' . $data;
        }
        $b = substr($data, 0, 2);
        $g = substr($data, 2, 2);
        $r = substr($data, 4, 2);
        return $r . $g . $b;
    }

    /* input vars defence */

    // разрешенные латинские символы
    const REG_LATIN = 'a-zA-Z';
    // разрешенные кириллические символы
    const REG_CYR = 'А-Яа-яЁёЙйЄєІіЇї';
    // разрешенные цифры
    const REG_NUM = '0-9';
    // разрешенные знаки
    const REG_SYM = '\s_\-"!@#\/\$%&\*\(\)\+=\{\}\[\]:;,\.<>\?';
    // знаки для подстановок
    const REG_TEMPLATE = '\*\?';

    static public function asStrBase($text, $maxlen = 0, $strict = true) {
        // очищаем от ненужных символов начало и конец строки
        $text = trim($text);
        if ($maxlen && (mb_strlen($text) > $maxlen)) {
            $text = mb_substr($text, 0, $maxlen);
        }

        $text = preg_replace_callback('/&#(\d+);/m', function ($matches) {return '';}, $text); #decimal notation
        $text = preg_replace_callback('/&#x([a-f0-9]+);/mi', function ($matches) {return '';}, $text); #hex notation
        $text = html_entity_decode($text, ENT_QUOTES);

        // базовые замены
        if ($strict) {
            $text = str_replace(array("\n", "\r", "\t"), ' ', $text);
            $text = str_replace('~', '-', $text);
        }
        $text = str_replace(array("'", '`'), '"', $text);
        $text = str_replace('\\', '/', $text);

        $text = addslashes($text);

        return $text;
    }

    static public function asStr($text, $maxlen = 0) {
        $text = self::asStrBase($text, $maxlen);
        // оставляем в строке только разрешенные символы
        $text = preg_replace("/[^" . self::REG_LATIN . self::REG_CYR . self::REG_NUM . self::REG_SYM . "]/u", '', $text);
        return $text;
    }

    static public function asStrOnlyAlpha($text) {
        return preg_replace("/[^" . self::REG_LATIN . self::REG_CYR . self::REG_NUM . "]/u", '', $text);
    }

    static public function asPassword($text) {
        return self::asStrBase($text, 255);
    }

    static public function asHexStr($text, $maxlen = false) {
        $text = trim($text);
        if ($maxlen && (mb_strlen($text) > $maxlen)) {
            $text = mb_substr($text, 0, $maxlen);
        }
        return preg_replace("/[^0-9ABCDEF]/u", '', mb_strtoupper($text));
    }

    static public function periodTest($period) {
        // y,m,d - required, H,M,S - optional
        $r = Period::decode($period);
        if (!$r['y'] || ($r['y'] < 1900) || ($r['y'] > 2999)) {
            return false;
        }
        if (!$r['m'] || ($r['m'] < 1) || ($r['m'] > 12)) {
            return false;
        }
        if (!$r['d'] || ($r['d'] < 1) || ($r['d'] > 31)) {
            return false;
        }
        return true;
    }

    static public function asPeriodDate($period) {
        $period = self::asStrBase($period, 10);
        if (self::periodTest($period)) {
            return $period;
        } else {
            return null;
        }
    }

    static public function asPeriodStartDate($period) {
        $period = self::asStrBase($period, 10);
        if (mb_strlen($period) == 7) {
            $period .= '-01'; // correct for short period "YYYY-MM"
        }
        if (self::periodTest($period)) {
            $r = Period::decode($period);
            return date('Y-m-01', mktime(0, 0, 0, $r['m'], 1, $r['y']));
        } else {
            return null;
        }
    }

    static public function asPeriodDateTime($period) {
        $period = self::asStrBase($period, 19);
        if (self::periodTest($period)) {
            return $period;
        } else {
            return null;
        }
    }

    static public function asTime($period) {
        return self::asStrBase($period, 8);
    }

    static public function asInt($val) {
        if (is_string($val)) {
            $val = trim($val);
            $val = str_replace(array(" ", "\n", "\r", "\t"), '', $val);
        }
        return (int) $val;
    }

    static public function asId($val) {
        $val = self::asInt($val);
        return ($val) ? $val : null;
    }

    static public function asFloat($val, $def = 0.0) {
        if (is_string($val)) {
            $val = trim($val);
            $val = str_replace(array(" ", "\n", "\r", "\t"), '', $val);
            $val = str_replace(',', '.', $val);
        }
        if (!settype($val, 'float')) {
            $val = $def;
        }
        return $val;
    }

    static public function asIntSetArray($val, $sep = ',') {
        $V = explode($sep, $val);
        $R = array();
        foreach ($V as $v) {
            $v = self::asInt($v);
            if ($v) {
                $R[] = $v;
            }
        }
        return $R;
    }

    static public function asIntSetArrayRange($val, $min, $max, $sep = ',') {
        $V = explode($sep, $val);
        $R = array();
        foreach ($V as $v) {
            $v = self::asInt($v);
            if ($v && $v >= $min && $v <= $max) {
                $R[] = $v;
            }
        }
        return $R;
    }

    static public function asFloatSetArray($val, $sep = ',') {
        $V = explode($sep, $val);
        $R = array();
        foreach ($V as $v) {
            $R[] = self::asFloat($v);
        }
        return $R;
    }

    static public function asPeriodDateRange($val, $sep = ',') {
        $V = explode($sep, $val);
        for ($i = 0; $i < 2; $i++) {
            $R[$i] = isset($V[$i]) ? self::asPeriodDate($V[$i]) : null;
        }
        return $R;
    }

    static public function asIntSet($val, $sep = ',') {
        return implode($sep, self::asIntSetArray($val, $sep));
    }

    static public function asStrSetArray($val, $sep = ',', $maxlen = 0) {
        $V = explode($sep, $val);
        $R = array();
        foreach ($V as $v) {
            $v = self::asStr($v, $maxlen);
            if ($v) {
                $R[] = $v;
            }
        }
        return $R;
    }

    static public function asJSONArray($val) {
        $arr = json_decode($val, true);
        return is_array($arr) ? $arr : array();
    }

    static public function asStrSet($val, $sep = ',', $maxlen = 0) {
        return implode($sep, self::asStrSetArray($val, $sep, $maxlen));
    }

    static public function asSet($val, $valid, $sep = ',') {
        //$val = self::asStr($val);
        $V = explode($sep, $val);
        $R = array();
        foreach ($V as $v) {
            if (in_array($v, $valid)) {
                $R[] = $v;
            }
        }
        return implode($sep, $R);
    }

    static public function asEnum($val, $enum, $def) {
        //$val = self::asStr($val);
        return in_array($val, $enum) ? $val : $def;
    }

    /* variable "isset or ..." */

    static public function issetor(&$variable, $or = NULL) {
        return $variable === NULL ? $or : $variable;
    }

    /* hashing */

    static public function encode($str, $key) {
        return strtolower(md5(strtolower(md5($str)) . $key));
    }

    static public function uniqid() {
        return uniqid(time(), true);
    }

    static public function fileHash($filename) {
        return md5_file($filename);
    }

    static public function makePathTree($str, $len = 2, $cnt = 2) {
        $S = array();
        for ($i = 0; $i < $cnt; $i++) {
            $S[] = substr($str, $i * $len, $len);
        }
        return implode('/', $S);
    }

    static public function asFilename($fname) {
        $r = str_replace(array('?', '+', '!', '~', '`', "'", '"', '<', '>', '&', '^', '%', '@', '*', '/', '\\', ' '), '_', trim($fname));
        return str_replace(array("\t", "\n", "\r", "\x0B", "\0"), '', $r);
    }

}

?>
