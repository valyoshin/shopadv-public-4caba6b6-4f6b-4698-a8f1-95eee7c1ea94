<?php

function money2str($money, $hi, $lo, $lang, $rod = 'w')
{
    if ($money == '') {
        return '';
    }
    if ($money == 0.00) {
        return $lang['zero'] . $hi . '00' . $lo;
    }
    if ($money < 0) {
        $money = -$money;
    }
    $money0 = sprintf("%01.2f", $money);

    $m = explode('.', $money0);
    $money = $m[0];
    $kop = $m[1];

    $billions = $lang['billions'];
    $millions = $lang['millions'];
    $thousands = $lang['thousands'];
    $currency = array('', '', '');
    $digit[0] = $lang['digit_m'];
    $digit[1] = $lang['digit_f'];
    $hundreds = $lang['hundreds'];
    $decimals = $lang['decimals'];
    $from10to19 = $lang['from10to19'];
    $str = '';

    $A = array('billions', 'millions', 'thousands', 'currency');
    if ($rod == 'w') {
        $Sex = array(0, 0, 1, 1);
    } else {
        $Sex = array(0, 0, 1, 0);
    }
    $i = 1000000000.0;
    foreach ($A as $k => $v) {
        $ar = $$v;
        $sex = $Sex[$k];
        $dgt = $digit[$sex];
        $money = (int) ($money0 / $i);
        $money0 = $money0 - $i * $money;
        if ((($money % 100) > 10) && (($money % 100) < 20)) {
            $cas = 1;
        } else {
            if (($money % 10) == 1) {
                $cas = 0;
            } else {
                if ((($money % 10) > 4) || (($money % 10) == 0)) {
                    $cas = 1;
                } else {
                    $cas = 2;
                }
            }
        }
        $res = [];
        $index = (int) ($money / 100);
        if ($index < 10) {
            $res[] = $hundreds[$index];
        };
        $money = $money % 100;
        if ($money >= 20) {
            $index = (int) ($money / 10);
            if ($index < 10) {
                $res[] = $decimals[$index];
            };
            $money = $money % 10;
        }
        if ($money >= 10) {
            $index = $money - 10;
            if ($index < 10) {
                $res[] = $from10to19[$index];
            }
        } else {
            $res[] = $dgt[$money];
        }
        if (implode('', $res) != '') {
            $res[] = $ar[$cas];
        }
        $str = $str . implode(' ', $res) . ' ';
        $i = $i / 1000.0;
    }
    if ($str == '') {
        $str = $lang['zero'];
    }

    return $str . $hi . $kop . $lo;
}
