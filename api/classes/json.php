<?php

//
// Class: JSON
// JSON encoding with cyr.UTF-8
// Copyright (c) 2012-2013 Alyoshin Vitaliy <master@ap-soft.com>
// base on code of http://alexmuz.ru/php-json_encode/
//

defined('IN_SITE') or die();

final class JSON {

    static public function encode($value, $UTFReplace = false) {
        if (is_int($value)) {
            return (string) $value;
        }

        if (is_string($value)) {
            $value = str_replace(array('\\', '/', '"', "\r", "\n", "\b", "\f", "\t"), array('\\\\', '\/', '\"', '\r', '\n', '\b', '\f', '\t'), $value);
            if ($UTFReplace) {
                $convmap = array(0x80, 0xFFFF, 0, 0xFFFF);
                $result = "";
                for ($i = mb_strlen($value) - 1; $i >= 0; $i--) {
                    $mb_char = mb_substr($value, $i, 1);
                    $match = null;
                    if (mb_ereg("&#(\\d+);", mb_encode_numericentity($mb_char, $convmap, "UTF-8"), $match)) {
                        $result = sprintf("\\u%04x", $match[1]) . $result;
                    } else {
                        $result = $mb_char . $result;
                    }
                }
            } else {
                $result = $value;
            }
            return '"' . $result . '"';
        }

        if (is_float($value)) {
            return str_replace(",", ".", $value);
        }

        if (is_null($value)) {
            return 'null';
            //return '""';
        }

        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }

        if (is_array($value)) {
            $with_keys = false;
            $n = count($value);
            for ($i = 0, reset($value); $i < $n; $i++, next($value)) {
                if (key($value) !== $i) {
                    $with_keys = true;
                    break;
                }
            }
        } elseif (is_object($value)) {
            $with_keys = true;
        } else {
            return '';
        }

        $result = array();
        if ($with_keys) {
            foreach ($value as $key => $v) {
                $result[] = JSON::encode((string) $key) . ':' . JSON::encode($v);
            }
            return '{' . implode(',', $result) . '}';
        } else {
            foreach ($value as $key => $v) {
                $result[] = JSON::encode($v);
            }
            return '[' . implode(',', $result) . ']';
        }
    }

    static public function decode($json) {
        return json_decode($json, true);
    }
}
