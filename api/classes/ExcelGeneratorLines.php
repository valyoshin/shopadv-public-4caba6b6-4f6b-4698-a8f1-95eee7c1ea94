<?php

/**
 * ExcelGeneratorLines
 *
 */
require_once('phpexcel/PHPExcel.php');
require_once('ExcelGenerator.php');

class ExcelGeneratorLines extends ExcelGenerator {

    protected $Styles = array();

    public function __construct($tempDir) {
        parent::__construct($tempDir);
    }

    public function __destruct() {
        parent::__destruct();
    }

    public function addStyle($styleID, $styleArray) {
        $this->Styles[$styleID] = new PHPExcel_Style();
        $this->Styles[$styleID]->applyFromArray($styleArray);
        $this->_ex->addCellXf($this->Styles[$styleID]);
        return $this->Styles[$styleID];
    }

    public function getStyle($styleID) {
        if (isset($this->Styles[$styleID])) {
            return $this->Styles[$styleID]->toArray();
        }

        return null;
    }

    protected function writeCellValue($sheet, $col, $row, $data) {
        if (!is_array($data)) {
            parent::writeCellValue($sheet, $col, $row, $data);
        } else {
            $value = isset($data['value']) ? trim($data['value']) : '';
            parent::writeCellValue($sheet, $col, $row, $value);
            if (isset($data['url'])) {
                $sheet->getCellByColumnAndRow($col, $row)->getHyperlink()->setUrl($data['url']);
            }
            if (isset($data['style']) && isset($this->Styles[$data['style']])) {
                $sheet->getCellByColumnAndRow($col, $row)->setXfIndex($this->Styles[$data['style']]->getIndex());
            }
        }
    }

    public function writeRow($startCol, $row, $data) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $sheet = $this->_ex->getActiveSheet();
            $col = $startCol;
            foreach($data as $val) {
                $this->writeCellValue($sheet, $col, $row, $val);
                $col++;
            }
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('writeRow: ' . $this->_error);
        }
    }

    public function writeRows($startCol, $startRow, $data) {
        try {
            if (!$this->isOpened()) {
                throw new Exception(self::ERR_NOT_OPENED);
            }
            $row = $startRow;
            foreach($data as $dataRow) {
                $this->writeRow($startCol, $row, $dataRow);
                $row++;
            }
        } catch (Exception $e) {
            $this->_error = $e->getMessage();
            throw new Exception('writeRows: ' . $this->_error);
        }
    }


}
